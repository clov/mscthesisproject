package problems;

import java.io.IOException;

import problems.interfaces.Problem;

/**
 * f3 from the jDE paper (or f2 from CEC 2005)
 * @author Teemu Peltonen
 * @version Dec 16, 2014
 */
public class SumSumProblem extends Problem{
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public SumSumProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public SumSumProblem(int dimension) {
        super(dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100; this.bounds[i][1] = 100;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        double outersum = 0.0;
        double innersum;
        int D = this.getDimension();
        
        for (int i = 0; i < D; i++) {
            innersum = 0;
            for (int j = 0; j < i+1; j++)
                innersum += x[j] - this.getShift(j);
            outersum += innersum * innersum;
        }
        return outersum;
    }

}
