package problems;

import problems.interfaces.Problem;
import java.io.IOException;

/**
 * A problem defined by the Rosenbrock function (f6 from CEC2005,
 * or f3 from CEC2008).
 * @author Teemu Peltonen
 * @version Nov 28, 2014
 */
public class RosenbrockProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if the shiftfile couldn't be found
     *         or if there were problems reading its contents
     */
    public RosenbrockProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public RosenbrockProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100; this.bounds[i][1] = 100;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum = 0;
        double zi, zi1;
        double elem, elem1;
        for (int i = 0; i < D-1; i++) {
            zi = x[i] - this.getShift(i) + 1;
            zi1 = x[i+1] - this.getShift(i+1) + 1;
            elem = zi * zi - zi1;
            elem1 = (zi - 1);

            sum += 100.0 * elem * elem + elem1 * elem1;
        }
        return sum;
    }

}
