package problems;

import java.io.FileNotFoundException;

import algorithms.utils.ResultsUtils;
import problems.interfaces.Problem;

import static utils.MatrixUtils.reShape;
import static utils.MathUtils.subtract;
import static utils.MathUtils.norm2sqrd;;

/**
 * Lennard-Jones cluster problem
 * @author Teemu Peltonen
 * @version Feb 19, 2015
 */
public class LJclusterProblem extends Problem {	

    /**
     * @param dimension dimension of the problem
     */
    public LJclusterProblem(int dimension) {

        super(dimension);
        if (dimension % 3 != 0 || dimension < 6) {
            String msg = "The problem dimension must be 6 + 3n for some natural number n.";
            throw new IllegalArgumentException(msg);
        }
    }


    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];

        this.bounds[0][0] = 0; this.bounds[0][1] = 4; 
        this.bounds[1][0] = 0; this.bounds[1][1] = 4; 
        this.bounds[2][0] = 0; this.bounds[2][1] = 4;

        for (int i = 3; i < this.getDimension(); i++)
        {
            bounds[i][0] = -4 - 0.25*Math.floor((i-3)/3);
            bounds[i][1] =  4 + 0.25*Math.floor((i-3)/3);
        }
    }
    
    @Override
    public void saveConfig(double[] x, String filename)
            throws FileNotFoundException {
        ResultsUtils.saveXYZ(x, 'C', filename);
    }

    @Override
    public double f(double[] x) { 

        double v = 0;
        double rSqrd = Double.NaN;
        int n = x.length/3;
        double[][] X = reShape(x, n, 3);
        for(int i=0; i < n-1; i++)
            for(int j=i+1; j < n; j++)
            { 
                // N.B. use norm squared!
                rSqrd = norm2sqrd(subtract(X[i], X[j]));
                v += Math.pow(rSqrd, -6) -2*Math.pow(rSqrd, -3);
            }
        return v;
    }
    
}
