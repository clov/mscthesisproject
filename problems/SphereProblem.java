package problems;

import java.io.IOException;

import problems.interfaces.Problem;

/**
 * A problem defined by a sphere function (f1 from CEC2005).
 * @author Teemu Peltonen
 * @version Nov 27, 2014
 */
public class SphereProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public SphereProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public SphereProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100; this.bounds[i][1] = 100;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum = 0;
        double zi;
        for (int i = 0; i < D; i++) {
            zi = x[i] - this.getShift(i);
            sum += zi * zi;
        }
        return sum;
    }

}
