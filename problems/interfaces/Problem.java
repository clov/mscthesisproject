package problems.interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;

import utils.FileUtils;

/**
 * A class that represents a general problem.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public abstract class Problem {

    private final int dimension;
    /** A vector defining the shift for each variable */
    protected double[] shifts = null;
    /**
     * A dimension*2 matrix including the lower and upper bounds
     * for each variable
     */
    protected double[][] bounds = null;
    /** A problem-specific initial solution vector */
    protected double[] initialSolution = null;

    /**
     * Initialize a new problem with the given shift file.
     * @param dimension dimension of the problem
     * @param shiftFile file including the shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public Problem(int dimension, String shiftFile)
            throws IOException {
        this.dimension = dimension;
        this.setBounds();
        this.loadShifts(shiftFile);
    }
    
    /**
     * Initialize a new problem without shifts.
     * @param dimension dimension of the problem
     */
    public Problem(int dimension) {
        this.dimension = dimension;
        this.setBounds();
        this.setZeroShifts();
    }
    
    /**
     * @return the name of the problem
     */
    public String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * @return the dimension of the problem
     */
    public int getDimension() {
        return this.dimension;
    }
    
    /**
     * Return the shift for the i'th coordinate
     * @param i the coordinate number
     * @return the shift for the i'th coordinate
     */
    public double getShift(int i) {
        return this.shifts[i];
    }
    
    /**
     * @return the bound matrix of the problem
     */
    public double[][] getBounds() {
        return this.bounds;
    }
    
    /**
     * Set the bounding box for the problem
     */
    protected abstract void setBounds();
    
    /**
     * Generate a problem-specific initial solution vector
     */
    protected void generateInitialSolution() {}
    
    /**
     * @return the initial solution vector
     */
    public double[] getInitialSolution() {
        return this.initialSolution;
    }
    
    /**
     * Test whether the problem has a specific initial solution.
     * @return true if the problem has a specific initial
     *         solution, false otherwise
     */
    public boolean hasInitialSolution() {
        return this.getInitialSolution() != null;
    }
    
    private void loadShifts(String filename)
            throws IOException {
        
        int D = this.getDimension();
        double[] shiftss = FileUtils.loadRowVectorFromFile(filename);
        this.shifts = new double[D];
        
        if (D <= shiftss.length)
            for (int i = 0; i < D; i++)
                this.shifts[i] = shiftss[i];
        
        else {
            for (int i = 0; i < shiftss.length; i++)
                this.shifts[i] = shiftss[i];
            for (int i = shiftss.length-1; i < D; i++)
                this.shifts[i] = 0.0;
        }
    }
    
    /**
     * Set the shift vector to a zero vector.
     */
    protected void setZeroShifts() {
        
        int D = this.getDimension();
        this.shifts = new double[D];
        
        for (int i = 0; i < D; i++)
            this.shifts[i] = 0.0;
    }
    
    /**
     * Save the configuration into a file.
     * @param x configuration to save
     * @param filename name of the file
     * @throws FileNotFoundException if the file was not found
     */
    public void saveConfig(double[] x, String filename)
            throws FileNotFoundException {
        FileUtils.saveArrayAsColumn(x, filename);
    }
    
    /**
     * Evaluate the fitness function
     * @param x the parameter vector
     * @return the fitness of the parameter vector
     */
    public abstract double f(double[] x);

}
