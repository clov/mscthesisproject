package problems;

import problems.interfaces.Problem;

/**
 * Corana's parabola problem.
 * (f6 from the DE paper).
 * @author Teemu Peltonen
 * @version Jan 30, 2015
 */
public class CoranasParabolaProblem extends Problem{
    
    private static final double bound = 1000;
    private static final int dimension = 4;
    
    /**
     * @param dimension dimension of the problem
     */
    @SuppressWarnings("unused")
    public CoranasParabolaProblem(int dimension) {
        super(CoranasParabolaProblem.dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -bound;
            this.bounds[i][1] = bound;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        double[] d = {1, 1000, 10, 100};

        double xj;
        double zj;
        double dj;
        
        double sum = 0.0;
        for (int j = 0; j < D; j++) {
            xj = x[j];
            zj = Math.floor(Math.abs(xj/0.2)+0.49999) * 
                    Math.signum(xj) * 0.2;
            dj = d[j];
            if (Math.abs(xj-zj) < 0.05)
                sum += 0.15 * dj *
                       Math.pow(zj-0.05*Math.signum(zj), 2);
            else
                sum += dj * xj * xj;
        }
        return sum;
    }

}
