package problems;

import java.io.FileNotFoundException;

import problems.interfaces.Problem;
import problems.utils.PathUtils;
import utils.FileUtils;
import utils.random.RandUtils;

/**
 * The minimum energy path problem (approximately)
 * @author Teemu Peltonen
 * @version Oct 23, 2014
 */
public class MEPproblem extends Problem {

    private final int m;
    private final int midpointsNr;
    private final int pesDim;
    private final double[] startpoint;
    private final double[] endpoint;

    /**
     * @param dimension dimension of the problem
     */
    public MEPproblem(int dimension) {
        super(dimension);
        //TODO check that the dimension is not illegal
        //FIXME get m from dimension
        this.m = 5;
        this.midpointsNr = (int)Math.pow(2, this.m) - 1;
        this.pesDim = 2;
        this.startpoint = new double[]{-1, 0};
        this.endpoint = new double[]{1, 0};
        //this.generateInitialSolution();
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];

        //TODO make better bounds (if needed)
        for (int i = 0; i < this.m; i++) {
            this.bounds[i][0] = -2.0;
            this.bounds[i][1] = 2.0;
        }
        
        for (int i = this.m; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100.0;
            this.bounds[i][1] = 100.0;
        }
    }
    
    /**
     * Get the norms-vector-part from the total configuration
     * vector x.
     * @param x the total configuration vector
     * @return see above
     */
    private double[] getNorms(double[] x) {
        
        double[] norms = new double[this.m];
        for (int i = 0; i < this.m; i++)
            norms[i] = x[i];
        
        return norms;
    }
    
    /**
     * Get the rndVectors-part from the total configuration vector x.
     * @param x the total configuration vector
     * @return see above
     */
    private double[][] getRndVectors(double[] x) {
        
        double[][] rndVectors = 
                new double[midpointsNr][this.pesDim];
        for (int i = 0; i < this.midpointsNr; i++)
            for (int j = 0; j < this.pesDim; j++)
                rndVectors[i][j] = x[this.m+i*this.pesDim+j];
        
        return rndVectors;
    }
    
    //TODO test
    /**
     * Out of norms and rndVectors, generate the total configuration
     * vector
     * @param rndVectors see above
     * @param norms see above
     * @return the total configuration vector
     */
    private double[] getX(double[] norms, double[][] rndVectors) {
        
        int xlen = this.m + this.midpointsNr * this.pesDim;
        
        double[] x = new double[xlen];
        for (int i = 0; i < this.m; i++)
            x[i] = norms[i];
        
        for (int i = 0; i < this.midpointsNr; i++)
            for (int j = 0; j < this.pesDim; j++)
                x[this.m+i*this.pesDim+j] = rndVectors[i][j];
        
        return x;
    }
    
    /**
     * Generate a smooth path by gradually decreasing the norms
     * when the recursion gets deeper, to serve as an initial
     * solution.
     */
    @Override
    protected void generateInitialSolution() {
        
        double[][] rndVectors = 
                new double[this.midpointsNr][this.pesDim];
        for (int i = 0; i < this.midpointsNr; i++)
            for (int j = 0; j < this.pesDim; j++)
                rndVectors[i][j] = RandUtils.random();
        
        double[] norms = new double[this.m];
        for (int i = 0; i < this.m; i++) {
            /*
            norms[i] = Math.abs(RandUtils.gaussian(0.0,
                    3.0*Math.pow(2, -1.5*i)));
                    */
            norms[i] = RandUtils.random(0.0,
                    10.0*Math.pow(2, -1.5*i));
        }
        
        this.initialSolution = this.getX(norms, rndVectors);
    }
    
    /**
     * @return the initial solution vector
     */
    @Override
    public double[] getInitialSolution() {
        return this.initialSolution;
    }
    
    /**
     * Calculate the potential V at the given 2D-point.
     * @param point the point where to calculate the potential
     * @return the value of the potential at the given point
     */
    private double V(double[] point) {
        
        if (point.length != 2) {
            String msg = "This potential function must be called with a vector in R^2.";
            throw new IllegalArgumentException(msg);
        }
        
        double x = point[0];
        double y = point[1];
        
        double xSqrd = x * x;
        double ySqrd = y * y;
        double sum = 1 - xSqrd - ySqrd;

        return sum * sum + ySqrd / (xSqrd + ySqrd);
    }

    /**
     * Calculate the potential V2 at the given 2D-point.
     * @param point the point where to calculate the potential
     * @return the value of the potential at the given point
     */
    private double V2(double[] point) {
        
        if (point.length != 2) {
            String msg = "This potential function must be called with a vector in R^2.";
            throw new IllegalArgumentException(msg);
        }
        
        double x = point[0];
        double y = point[1];
        
        double xSqrd = x * x;
        double ySqrd = y * y;
        return xSqrd*xSqrd + 4 * xSqrd * ySqrd - 2 * xSqrd +
                2 * ySqrd;
    }
    
    @Override
    public double f(double[] x) {
        
        // how the vector x is formed:
        // rndVectors = [(x_1^1, ..., x_1^{3N}), ..., (x_{2^m-1}^1, ..., x_{2^m-1}^{3N})]
        // norms = [c_1, ..., c_m]
        // x = [c_1, ..., c_m, x_1^1, ..., x_1^{3N}, x_2^1, ..., x_2^{3N}, ..., x_{2^m-1}^1, ... x_{2^m-1}^{3}]
        
        double[] norms = this.getNorms(x);
        double[][] rndVectors = this.getRndVectors(x);
        
        //FIXME remove
        @SuppressWarnings("unused")
        double[] xnew = this.getX(norms, rndVectors);

        // the path
        double[][] points = PathUtils.generatePath(this.startpoint,
                this.endpoint, rndVectors, norms);
        
        // The object function is the path integral of the PES over
        // the path above. Now that the partition is equidistant
        // and if the partition is sufficiently dense, it can
        // equivalently (modulo function values) be written in the
        // discrete case as sum_{j=1}^K V(gamma(t_j)), where K is
        // the number of points, V is the potential and gamma
        // is the path.
        
        double sum = 0.0;
        for (double[] point : points)
            sum += V(point);
        
        return sum;
    }
    
    /**
     * Save the configuration x transformed into the corresponding
     * path points.
     * @param x the configuration to save
     * @param filename name of the file
     * @throws FileNotFoundException if the file was not found
     */
    @Override
    public void saveConfig(double[] x, String filename)
            throws FileNotFoundException {
        
        double[] norms = getNorms(x);
        double[][] rndVectors = getRndVectors(x);
        
        double[][] points = PathUtils.generatePath(this.startpoint,
                this.endpoint, rndVectors, norms);
        
        FileUtils.saveMatrix(points, filename);
    }
    
    /**
     * Testing the function f.
     * @param args not used
     */
    public static void main(String[] args) {

        Problem problem = new MEPproblem(0);
        
        double[] x = {1, 1,   0, 1, 2,   0, 1, 2,   0, 1, 2};
        problem.f(x);
    }

}
