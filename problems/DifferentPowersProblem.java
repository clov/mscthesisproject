package problems;

import problems.interfaces.Problem;

/**
 * f5 from CEC2013.
 * @author Teemu Peltonen
 * @version Oct 2, 2014
 */
public class DifferentPowersProblem extends Problem{
    
    /**
     * @param dimension dimension of the problem
     */
    public DifferentPowersProblem(int dimension) {
        super(dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100; this.bounds[i][1] = 100;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        double sum = 0;
        int D = this.getDimension();
        for (int i = 0; i < D; i++)
            sum += Math.pow(Math.abs(x[i]), 2+4*i/(D-1));
        return Math.sqrt(sum);
    }

}
