package problems;

import problems.interfaces.Problem;

/**
 * f8 from the jDE paper.
 * @author Teemu Peltonen
 * @version Dec 10, 2014
 */
public class XsinSumProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     */
    public XsinSumProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -500; this.bounds[i][1] = 500;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum = 0.0;
        double xi;
        
        for (int i = 0; i < D; i++) {
            xi = x[i];
            sum += xi * Math.sin(Math.sqrt(Math.abs(xi)));
        }
        return -sum;
    }

}
