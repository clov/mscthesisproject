package problems;

import problems.interfaces.Problem;

//FIXME probably bug somewhere (fixed now?)
/**
 * Step problem / modified third De Jong problem
 * (f3 from the DE paper).
 * @author Teemu Peltonen
 * @version Jan 30, 2015
 */
public class StepProblem extends Problem{
    
    private static double bound = 5.12;
    private static int dimension = 5;
    
    /**
     * @param dimension dimension of the problem
     */
    public StepProblem(@SuppressWarnings("unused") int dimension) {
        super(StepProblem.dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -bound;
            this.bounds[i][1] = bound;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        double xj;
        
        // test if x[j] is in the bounds for all j
        boolean belongs = true;
        for (int j = 0; j < D; j++) {
            xj = x[j];
            if (!(-bound < xj && xj < bound)) {
                belongs = false;
                break;
            }
        }
        
        if (belongs) {
            double sum = 0.0;
            for (int j = 0; j < D; j++) {
                xj = x[j];
                sum += Math.floor(xj);
            }
            return 30.0 + sum;
        }
        double prod = 1.0;
        for (int j = 0; j < D; j++) {
            xj = x[j];
            if (!(-bound < xj && xj < bound)
                    && xj < 0) {
                prod *= 30.0 * Math.signum(-xj-bound);
            }
        }
        return prod;
    }

}
