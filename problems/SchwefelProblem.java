package problems;

import java.io.IOException;

import problems.interfaces.Problem;

/**
 * Schwefel's problem (f2 from CEC2008).
 * @author Teemu Peltonen
 * @version Nov 28, 2014
 */
public class SchwefelProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public SchwefelProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public SchwefelProblem(int dimension) {
        super(dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -100; this.bounds[i][1] = 100;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        double max = 0;
        int D = this.getDimension();
        double abszi;
        for (int i = 0; i < D; i++) {
            abszi = Math.abs(x[i] - this.getShift(i));
            if (abszi > max)
                max = abszi;
        }
        return max;
    }

}
