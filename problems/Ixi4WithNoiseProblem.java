package problems;

import problems.interfaces.Problem;
import utils.random.RandUtils;

/**
 * f7 from the jDE paper.
 * @author Teemu Peltonen
 * @version Oct 14, 2014
 */
public class Ixi4WithNoiseProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     */
    public Ixi4WithNoiseProblem(int dimension) {
        super(dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -1.28; this.bounds[i][1] = 1.28;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        double sum = 0.0;
        
        for (int i = 0; i < D; i++)
            sum += (i+1) * Math.pow(x[i], 4);
        return sum + RandUtils.random();
    }

}
