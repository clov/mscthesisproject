package problems.utils;

import java.io.FileNotFoundException;
import java.util.Stack;

import utils.FileUtils;
import utils.MathUtils;
import utils.random.RandUtils;

/**
 * Utilities for generating paths.
 * @author Teemu Peltonen
 * @version Oct 23, 2014
 */
public class PathUtils {
    
    private static void generatePathRecursive(double[] A, double[] B,
            double[][] points, int m, int mmax,
            Stack<double[]> rndVectors, double[] norms,
            int midpointIdx, int d) {
        
        if (m == mmax + 1)
            return;
        
        double norm = norms[m-1];
        double[] rndVector = rndVectors.pop();
        
        double[] BminusA = new double[A.length];
        for (int i = 0; i < A.length; i++)
            BminusA[i] = B[i] - A[i];

        double[] displacement =
                MathUtils.generatePerpendicularVector(rndVector,
                        BminusA, norm);

        // midpoint = displacement + 0.5 * (A+B)
        double[] midpoint = new double[displacement.length];
        for (int i = 0; i < displacement.length; i++)
            midpoint[i] = displacement[i] + 0.5 * (A[i] + B[i]);
        
        points[midpointIdx] = midpoint;
        
        generatePathRecursive(A, midpoint, points, m+1, mmax,
                rndVectors, norms, midpointIdx-d, d/2);
        generatePathRecursive(midpoint, B, points, m+1, mmax,
                rndVectors, norms, midpointIdx+d, d/2);
    }
    
    /**
     * Generate a path recursively from the point A to B using
     * random vectors to displace the linear path at different points.
     * @param A the other endpoint of the path
     * @param B the other endpoint of the path
     * @param rndVectors vectors that are used to displace the linear
     *        path at different points
     * @param norms norms of the orthogonal displacement vectors
     * @return points of the generated path
     */
    public static double[][] generatePath(double[] A, double[] B,
            double[][] rndVectors, double[] norms) {
        
        int mmax = norms.length;
        
        Stack<double[]> rndVectorsStack = new Stack<double[]>();
        for (double[] rndVector : rndVectors)
            rndVectorsStack.add(rndVector);

        double[][] points = new double[rndVectors.length+2][A.length];
        points[0] = A;
        points[points.length-1] = B;
        int midpointIdx = (points.length-1) / 2;
        
        generatePathRecursive(A, B, points, 1, mmax,
                rndVectorsStack, norms, midpointIdx,
                midpointIdx/2);
        
        return points;
    }
    
    /**
     * Testing the random path generator.
     * @param args not used
     * @throws FileNotFoundException if the files used cannot be found
     */
    public static void main(String[] args)
            throws FileNotFoundException {
        
        double[] A = {-10.0, -10.0};
        double[] B = {10.0, 10.0};
        
        int mmax = 6;

        int dim = A.length;
        int midpointsNr = (int)Math.pow(2, mmax) - 1;
        
        double[][] rndVectors = 
                new double[midpointsNr][dim];
        for (int i = 0; i < midpointsNr; i++)
            for (int j = 0; j < dim; j++)
                rndVectors[i][j] = RandUtils.random();
        
        double[] norms = new double[mmax];
        for (int i = 0; i < mmax; i++) {
            norms[i] = Math.abs(RandUtils.gaussian(0.0,
                    30.0*Math.pow(2, -1.5*i)));
        }
        
        double[][] points = generatePath(A, B, rndVectors, norms);
        String dir = "C:/Users/clov/Dropbox/School/FYS/theses/gradu/mscthesisproject/";
        FileUtils.saveMatrix(points, dir+"develop_examples/randompath.txt");
    }

}
