package problems;

import problems.interfaces.Problem;
import java.io.IOException;

/**
 * A problem defined by the Rastrigin's function (f9 from CEC2005
 * or f4 from CEC2008).
 * @author Teemu Peltonen
 * @version Nov 27, 2014
 */
public class RastriginProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if the shiftfile couldn't be found
     *         or if there were problems reading its contents
     */
    public RastriginProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public RastriginProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -5.0; this.bounds[i][1] = 5.0; 
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum = 0.0;
        double zi;
        for (int i = 0; i < D; i++) {
            zi = x[i] - this.getShift(i);
            sum += (zi*zi - 10.0 * Math.cos(2*Math.PI*zi) + 10);
        }
        return sum;
    }

}
