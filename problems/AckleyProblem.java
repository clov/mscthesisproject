package problems;

import java.io.IOException;

import problems.interfaces.Problem;

/**
 * A problem defined by the Ackley function (f6 from CEC2008).
 * @author Teemu Peltonen
 * @version Nov 27, 2014
 */
public class AckleyProblem extends Problem {
    
    private static final double e = Math.exp(1);
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public AckleyProblem(int dimension, String shiftFile)
            throws IOException {
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public AckleyProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -32; this.bounds[i][1] = 32;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum1 = 0;
        double sum2 = 0;
        double zi;
        
        for (int i = 0; i < D; i++) {
            zi = x[i] - this.getShift(i);
            sum1 += zi * zi;
            sum2 += Math.cos(2 * Math.PI * zi);
        }
        return -20.0 * Math.exp(-0.2 * Math.sqrt(sum1/D))
               - Math.exp(sum2/D) + 20.0 + e;
    }

}
