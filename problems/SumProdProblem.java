package problems;

import problems.interfaces.Problem;

/**
 * f2 from the jDE paper.
 * @author Teemu Peltonen
 * @version Oct 14, 2014
 */
public class SumProdProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     */
    public SumProdProblem(int dimension) {
        super(dimension);
    }
    
    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -10; this.bounds[i][1] = 10;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        double absxi;
        double sum = 0.0;
        double prod = 1.0;
        
        for (int i = 0; i < D; i++) {
            absxi = Math.abs(x[i]);
            sum += absxi;
            prod *= absxi;
        }
        return sum + prod;
    }

}
