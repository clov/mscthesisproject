package problems;

import java.io.IOException;

import problems.interfaces.Problem;

/**
 * A problem defined by the Griewank function (f5 from CEC2008,
 * or f11 from the jDE paper)
 * @author Teemu Peltonen
 * @version Dec 11, 2014
 */
public class GriewankProblem extends Problem {
    
    /**
     * @param dimension dimension of the problem
     * @param shiftFile the filename of the file containing the
     *        shift vector
     * @throws IOException if some of the files couldn't be found
     *         or if there were problems reading their contents
     */
    public GriewankProblem(int dimension, String shiftFile)
            throws IOException{
        super(dimension, shiftFile);
    }
    
    /**
     * @param dimension dimension of the problem
     */
    public GriewankProblem(int dimension) {
        super(dimension);
    }

    @Override
    protected void setBounds() {
        
        this.bounds = new double[this.getDimension()][2];
        
        for (int i = 0; i < this.getDimension(); i++) {
            this.bounds[i][0] = -600; this.bounds[i][1] = 600;
        }
    }
    
    @Override
    public double f(double[] x) {
        
        int D = this.getDimension();
        
        double sum = 0;
        double prod = 1;
        double zi;
        for (int i = 0; i < D; i++) {
            zi = x[i] - this.getShift(i);
            sum += zi * zi;
            prod *= Math.cos(zi/Math.sqrt(i+1));
        }
        return sum / 4000.0 - prod + 1;
    }

}
