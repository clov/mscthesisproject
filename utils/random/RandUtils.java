package utils.random;

import java.util.Arrays;
import java.util.Random;

/**
 * Utilities for random number generation.
 * @author Teemu Peltonen
 * @version Feb 18, 2015 
 */
public class RandUtils {

    private static final Random random;

    // this is called when some method is called for the first time
    static {
        random = new Random();
    }
    
    /**
     * Generate a uniformly distributed random double in the
     * interval [0, 1[.
     * @return a random double in the interval [0, 1[.
     */
    public static double random() {
        return random.nextDouble();
    }

    /**
     * Generate a uniformly distributed random double in the
     * interval [a, b[.
     * @param a inclusively the lower limit of the random number
     * @param b exlusively the upper limit of the random number
     * @return a random double in the interval [a, b[
     */
    public static double random(double a, double b) {
        return a + random.nextDouble() * (b - a);
    }
    
    /**
     * Generate a uniformly distributed random integer in the
     * interval [0, n[.
     * @param n endpoint of the interval
     * @return a random integer in the interval [0, n[
     */
    public static int randomInt(int n) {
        return random.nextInt(n);
    }

    /**
     * Generate a random number following the standard Gaussian
     * distribution
     * @return a random number following the standard Gaussian
     *         distribution
     */
    public static double gaussian() {
        return random.nextGaussian();
    }
    
    /**
     * Generate a random number following the Gaussian distribution
     * with the given mean and standard deviation
     * @param mean mean of the Gaussian distribution
     * @param stdDev standard deviation of the Gaussian distribution
     * @return a random number following the Gaussian distribution
     */
    public static double gaussian(double mean, double stdDev) {
        return gaussian() * stdDev + mean;
    }

    /**
     * Generate a random number following the Cauchy distribution
     * @return a random number following the Cauchy distribution
     */
    public static double cauchy() {
        return Math.tan(Math.PI * (random() - 0.5));
    }
    
    /**
     * Generate a random number following the Cauchy distribution
     * with the given location and scale parameters
     * @param location the location parameter
     * @param scale the scale parameter
     * @return random number following the Cauchy distribution
     */
    public static double cauchy(double location, double scale) {
        return cauchy() * scale + location;
    }
    
    /**
     * Randomly permutate the elements of a given array.
     * @param array the array whose elements are to be permutated
     */
    public static void randomPermutation(int[] array) {
        
        int rand, temp;
        for (int i = 0; i < array.length; i++) {
            rand = randomInt(i+1);
            temp = array[i];
            array[i] = array[rand];
            array[rand] = temp;
        }
    }

    /**
     * Small tests
     * @param args not used
     */
    public static void main(String[] args) {
        int[] array = {10, 11, 12, 13, 14};
        randomPermutation(array);
        System.out.println(Arrays.toString(array));
    }

}
