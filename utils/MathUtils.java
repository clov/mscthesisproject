package utils;

import java.util.Arrays;

/**
 * Mathematical utilities.
 * @author Teemu Peltonen
 * @version Apr 1, 2015
 */
public class MathUtils {

    /**
     * Subtract 2 R^n vectors element-wise.
     * @param vec1 the first vector
     * @param vec2 the second vector
     * @return the subtraction vec1 - vec2
     */
    public static double[] subtract(double[] vec1, double[] vec2) {

        int n = vec1.length;
        
        if (n != vec2.length) {
            String msg = "The dimensions of the vectors must agree.";
            throw new IllegalArgumentException(msg);
        }
        
        double[] vecSum = new double[n];
        for (int i = 0; i < n; i++)
            vecSum[i] = vec1[i] - vec2[i];
        
        return vecSum;
    }
    
    /**
     * Calculate the Euclidean dot product between the vectors
     * u and v.
     * @param u see above
     * @param v see avove
     * @return the Euclidean dot product
     */
    public static double dot(double[] u, double[] v) {
        
        if (u.length != v.length) {
            String msg = "u and v must have the same dimensions.";
            throw new IllegalArgumentException(msg);
        }
        
        double sum = 0.0;
        for (int i = 0; i < u.length; i++)
            sum += u[i] * v[i];
        return sum;
    }

    /**
     * Calculates the squared Euclidean norm of a vector in R^n.
     * @param vector a vector in R^n whose norm squared is to be
     *               computed
     * @return Euclidean norm squared of the given vector
     */
    public static double norm2sqrd(double[] vector) {
        return dot(vector, vector);
    }

    /**
     * Calculates the Euclidean norm of a vector in R^n.
     * @param vector a vector in R^n whose norm is to be computed
     * @return Euclidean norm of the given vector
     */
    public static double norm2(double[] vector) {
        return Math.sqrt(norm2sqrd(vector));
    }
    
    /**
     * Make the given vector of given length.
     * @param vector the vector to use
     * @param r the length of the following vector
     */
    public static void toLength(double[] vector, double r) {
        double norm = norm2(vector);
        for (int i = 0; i < vector.length; i++)
            vector[i] = vector[i] / norm * r;
    }
    
    /**
     * Finds the index i such that array[i] is the minimum element
     * of the array.
     * @param array the given array whose minimum is to be found
     * @return the minimizing index
     * @example
     * <pre name="test">
     *  indexMin(new double[]{-3.0, -1.0, 2.0}) === 0;
     *  indexMin(new double[]{-1.0, -3.0, 2.0}) === 1;
     *  indexMin(new double[]{-1.0, 2.0, -3.0}) === 2;
     *  indexMin(new double[]{-1.0, -1.0, -1.0}) === 0;
     * </pre>
     */
    public static int indexMin(double[] array) {
        int minIndex = 0;
        double minValue = Double.MAX_VALUE;
        double element;
        for (int i = 0; i < array.length; i++) {
            element = array[i];
            if (element < minValue) {
                minIndex = i;
                minValue = element;
            }
        }
        return minIndex;
    }
    
    /**
     * Round everything between -1 and 1 to 0.
     * @param x number to round
     * @return 0 if -1 <= x <= 1, x otherwise
     */
    public static double fix(double x) {
        if (-1 <= x && x <= 1) return 0.0;
        return x;
    }
    
    /**
     * Test whether two numbers are close to each other within some
     * absolute tolerance.
     * @param a a number
     * @param b another number
     * @param tol the absolute tolerance parameter
     * @return true if the numbers are close to each other, false
     *         if not
     */
    public static boolean areClose(double a, double b, double tol) {
        return Math.abs(a-b) < tol;
    }
    
    /**
     * Test whether two numbers are close to each other within the
     * absolute tolerance 1E-9.
     * @param a a number
     * @param b another number
     * @return true if the numbers are close to each other, false
     *         if not
     */
    public static boolean areClose(double a, double b) {
        return areClose(a, b, 1E-9);
    }
    
    /**
     * Test whether the two arrays are close to each other
     * element-wise within some tolerance
     * @param u an array
     * @param v another array
     * @param tol the absolute tolerance parameter
     * @return true if the vectors are close to each other, false
     *         otherwise (also if the dimensions do not agree)
     */
    public static boolean areClose(double[] u, double[] v, double tol) {
        
        if (u.length != v.length) return false;
        
        for (int i = 0; i < u.length; i++)
            if (!areClose(u[i], v[i], tol)) return false;
        
        return true;
    }

    /**
     * Test whether the two arrays are close to each other
     * element-wise within the absolute tolerance 1E-9
     * @param u an array
     * @param v another array
     * @return true if the vectors are close to each other, false
     *         otherwise (also if the dimensions do not agree)
     */
    public static boolean areClose(double[] u, double[] v) {
        return areClose(u, v, 1E-9);
    }
    
    /**
     * Test whether two vectors are linearly dependent within some
     * absolute tolerance.
     * @param u a vector
     * @param v another vector
     * @param tol the absolute tolerance parameter
     * @return true if the vectors are close to each other within
     *         the given absolute tolerance, false otherwise
     * @example
     * <pre name="test">
     *  double[] u = {1.0, 1.0, 1.0};
     *  double tol = 0.2;
     *  areLD(u, new double[]{1.0, 1.0, 1.0}, tol) === true;
     *  areLD(u, new double[]{1.1, 0.9, 1.0}, tol) === true;
     *  areLD(u, new double[]{1.1, 0.9, -1.0}, tol) === false;
     *  areLD(u, new double[]{-1.0, -1.0, -1.0}, tol) === true;
     *  areLD(u, new double[]{-1.1, -0.9, -1.0}, tol) === true;
     *  areLD(u, new double[]{-1.1, -0.9, 1.0}, tol) === false;
     * </pre>
     */
    public static boolean areLD(double[] u, double[] v, double tol) {

        if (u.length != v.length) {
            String msg = "The vectors have to have the same dimensions.";
            throw new IllegalArgumentException(msg);
        }
        
        double[] minusU = new double[u.length];
        for (int i = 0; i < u.length; i++)
            minusU[i] = -u[i];
        
        if (areClose(u, v, tol) || areClose(minusU, v, tol))
            return true;
        
        return false;
    }
    
    /**
     * Test whther two vectors are linearly dependent within the
     * absolute tolerance 1E-9.
     * @param u a vector
     * @param v another vector
     * @return true if the vectors are close to each other within
     *         the absolute tolerance 1E-9, false otherwise
     */
    public static boolean areLD(double[] u, double[] v) {
        return areLD(u, v, 1E-9);
    }
    
    /**
     * Make v orthogonal to u by using the idea from the Gram-Schmidt
     * method.
     * @param v see above
     * @param u see above
     * @return orthogonalized v
     */
    public static double[] orthogonalize(double[] v, double[] u) {
        
        if (areClose(norm2sqrd(u), 0) ||
            areClose(norm2sqrd(v), 0)) {
            String msg = "u and v must not be zero vectors.";
            throw new IllegalArgumentException(msg);
        }
        
        double[] orthogonalized = new double[v.length];
        double dot = dot(v, u);
        double uNormSqrd = norm2sqrd(u);
        
        // orthogonalized = v - v.u / u.u * u
        for (int i = 0; i < v.length; i++)
            orthogonalized[i] = v[i] - dot / uNormSqrd * u[i];

        return orthogonalized;
    }
    
    /**
     * From the vector v, generate a new vector with given norm
     * that is perpendicular to u.
     * @param v see above
     * @param u see avove
     * @param norm norm of the new vector
     * @return see above
     */
    public static double[] generatePerpendicularVector(double[] v,
            double[] u, double norm) {
        
        double[] vnew = orthogonalize(v, u);
        double vnewNorm = norm2(vnew);
        for (int i = 0; i < v.length; i++)
            vnew[i] = vnew[i] / vnewNorm * norm;
        
        return vnew;
    }
    
    /**
     * Calculate the mean of the elements of x.
     * @param x array containing the elements
     * @return the mean of the elements of x
     */
    public static double mean(double[] x) {

        double sum = 0.0;
        for (double xi : x)
            sum += xi;
        return sum / x.length;
    }
    
    /**
     * Calculate the mean of the elements of x.
     * @param x array containing the elements
     * @return the mean of the elements of x
     */
    public static double mean(int[] x) {

        double sum = 0.0;
        for (double xi : x)
            sum += xi;
        return sum / x.length;
    }

    /**
     * Calculate the mean and the standard deviation of the
     * elements of x.
     * @param x array containing the elements
     * @return the mean and the standard deviation
     * @example
     * <pre name="test">
     *  #TOLERANCE=1E-6;
     *  double[] x = {2, 4, 4, 4, 5, 5, 7, 9};
     *  double result[] = meanAndStdDev(x);
     *  result[0] ~~~ 5.0;
     *  result[1] ~~~ 2.13809;
     * </pre>
     */
    public static double[] meanAndStdDev(double[] x) {
        
        double mean = mean(x);
        int N = x.length;
        
        double sum = 0.0;
        double d;
        for (double xi : x) {
            d = xi - mean;
            sum += d * d;
        }
            
        double[] result = new double[2];
        result[0] = mean;
        result[1] = Math.sqrt(sum / (N-1));
        return result;
    }
   
    /**
     * Calculate the mean and the standard error of the mean of the
     * elements of x.
     * @param x array containing the elements
     * @return the mean and the standard error of the mean
     * @example
     * <pre name="test">
     *  #TOLERANCE=1E-6;
     *  double[] x = {2, 4, 4, 4, 5, 5, 7, 9};
     *  double result[] = meanAndStdErr(x);
     *  result[0] ~~~ 5.0;
     *  result[1] ~~~ 0.755929;
     * </pre>
     */
    public static double[] meanAndStdErr(double[] x) {

        double[] result = meanAndStdDev(x);
        result[1] /= Math.sqrt(x.length);
        return result;
    }
    
    /**
     * Calculate the mean, the standard deviation and the standard
     * error of the mean of the elements of x.
     * @param x array containing the elements
     * @return the mean, the standard deviation and the standard
     *         error of the mean
     * @example
     * <pre name="test">
     *  #TOLERANCE=1E-6;
     *  double[] x = {2, 4, 4, 4, 5, 5, 7, 9};
     *  double result[] = meanAndStdDevAndErr(x);
     *  result[0] ~~~ 5.0;
     *  result[1] ~~~ 2.13809;
     *  result[2] ~~~ 0.755929;
     * </pre>
     */
    public static double[] meanAndStdDevAndErr(double[] x) {
        
        double[] meanAndStdDev = meanAndStdDev(x);
        double[] result = new double[3];
        result[0] = meanAndStdDev[0];
        double stddev = meanAndStdDev[1];
        result[1] = stddev;
        result[2] = stddev / Math.sqrt(x.length);
        
        return result;
    }
    
    /**
     * Shift the contents of an array by i indices and treat the
     * edges by periodic boundary conditions.
     * @param array array to shift
     * @param n how many indices to shift (shif to right if
     *        positive, left if negative)
     * @return a new array that is the old array shifted
     * @example
     * <pre name="test">
     *  #import java.util.Arrays;
     *  int[] array = {0, 1, 2, 3};
     *  Arrays.toString(shiftArray(array, 0)) === "[0, 1, 2, 3]";
     *  Arrays.toString(shiftArray(array, 1)) === "[3, 0, 1, 2]";
     *  Arrays.toString(shiftArray(array, -1)) === "[1, 2, 3, 0]";
     *  Arrays.toString(shiftArray(array, 2)) === "[2, 3, 0, 1]";
     *  Arrays.toString(shiftArray(array, -2)) === "[2, 3, 0, 1]";
     *  Arrays.toString(shiftArray(array, 3)) === "[1, 2, 3, 0]";
     *  Arrays.toString(shiftArray(array, -3)) === "[3, 0, 1, 2]";
     *  Arrays.toString(shiftArray(array, 4)) === "[0, 1, 2, 3]";
     *  Arrays.toString(shiftArray(array, -4)) === "[0, 1, 2, 3]";
     *  Arrays.toString(shiftArray(array, 8)) === "[0, 1, 2, 3]";
     *  Arrays.toString(shiftArray(array, -8)) === "[0, 1, 2, 3]";
     *  // same as shifting 1
     *  Arrays.toString(shiftArray(array, 9)) === "[3, 0, 1, 2]";
     *  // same as shifting -1
     *  Arrays.toString(shiftArray(array, -9)) === "[1, 2, 3, 0]";
     * </pre>
     */
    public static int[] shiftArray(int[] array, int n) {
        
        int len = array.length;
        // shifting to the left is the same as shifting to the
        // right by a proper amount
        while (n < 0)
            n += len;
        int[] newArray = new int[len];
        for (int i = 0; i < len; i++) {
            newArray[(i+n) % len] = array[i];
        }
        return newArray;
    }

    /**
     * Small tests
     * @param args not used
     */
    public static void main(String[] args) {
        
        int[] array = {0, 1, 2, 3};
        shiftArray(array, -1);
    }

}
