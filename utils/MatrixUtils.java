package utils;

import java.util.Arrays;

import utils.MathUtils;

/**
 * Utility functions for matrix manipulations.
 * @author Teemu Peltonen
 * @version May 28, 2014
 */
public class MatrixUtils {

    /**
     * Convert a two-dimensional array to String.
     * @param mat the two-dimensional array to be converted
     * @return a matrix-looking String object of the given
     *         array
     */
    public static String toString(double[][] mat) {

        StringBuilder str = new StringBuilder("[\n");
        for (double[] row : mat) {
            str.append(Arrays.toString(row));
            str.append("\n");
        }
        str.append("]");
        return str.toString();
    }

    
    /**
     * Transpose a two-dimensional matrix
     * @param mat matrix to be transposed
     * @return the transpose
     */
    public static double[][] transpose(double[][] mat) {
        int n = mat.length;
        int m = mat[0].length;
        double[][] matT = new double[m][n];

        for (int i=0; i < n; i++) 
            for (int j=0; j < m; j++) 
                matT[j][i] = mat[i][j];
        return matT; 
    }

    /**
     * Reshape a vector into an n*m matrix.
     * @param vec vector to be reshaped
     * @param n number of the rows
     * @param m number of the columns
     * @return a reshaped matrix
     */
    public static double[][] reShape(double[] vec, int n, int m) {
        //TODO what to do when the dimensions don't match?
        int N = vec.length;
        double[][] mat = new double[n][m];
        int i = 0;
        for (int j=0; j < N; j++) {
            mat[i][j%m] = vec[j];
            if ((j+1) % m == 0)
                i++;
        }
        return mat;
    }
    
    /**
     * Test whether the two matrices are close to each other
     * element-wise within some tolerance
     * @param a an array
     * @param b another array
     * @param tol the absolute tolerance parameter
     * @return true if the arrays are close to each other, false
     *         otherwise (also if the dimensions do not agree)
     */
    public static boolean areClose(double[][] a, double[][] b, double tol) {
        
        if (a.length != b.length) return false;
        
        for (int i = 0; i < a.length; i++)
            if (!MathUtils.areClose(a[i], b[i], tol))
                return false;
        
        return true;
    }
    
    /**
     * Test whether the two matrices are close to each other
     * element-wise within the tolerance 1E-9.
     * @param a an array
     * @param b another array
     * @return true if the arrays are close to each other, false
     *         otherwise (also if the dimensions do not agree)
     */
    public static boolean areClose(double[][] a, double[][] b) {
        return areClose(a, b, 1E-9);
    }

    /**
     * Small tests
     * @param args not used
     */
    public static void main(String[] args) {
        double[][] mat = {{1, 2, 3, 4},
                          {-1, -2, -3, -4}};
        System.out.println(toString(mat));
        double[][] matT = transpose(mat);
        System.out.println(toString(matT));

        double[] vec = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(Arrays.toString(vec));
        mat = reShape(vec, 3, 3);
        System.out.println(toString(mat));


    }

}
