package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * General utilities for playing with files.
 * @author Teemu Peltonen
 * @version Nov 27, 2014
 */
public class FileUtils {

    /**
     * Open a file for reading.
     * @param filename name of the file
     * @return the opened BufferedReader object
     * @throws FileNotFoundException if the file couldn't be opened 
     */
    public static BufferedReader openFileForReading(String filename)
            throws FileNotFoundException {
        return new BufferedReader(new FileReader(filename));
    }
    
    /**
     * Open a file for writing.
     * @param filename name of the file
     * @param append whether to append in the end of the file or not
     * @return the opened PrintStream object
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static PrintStream openFileForWriting(String filename,
            boolean append) throws FileNotFoundException {
        return new PrintStream(new FileOutputStream(filename,
                append));
    }
    
    /**
     * Open a file for writing.
     * @param filename name of the file
     * @return the opened PrintStream object
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static PrintStream openFileForWriting(String filename)
            throws FileNotFoundException {
        return openFileForWriting(filename, false);
    }
    
    /**
     * Load a row vector from a file which includes the components
     * in single line separated by spaces.
     * @param filename name of the file
     * @return the loaded row vector
     * @throws IOException if the file couldn't be opened or if there
     *         were problems reading the contents.
     */
    public static double[] loadRowVectorFromFile(String filename)
            throws IOException {

        BufferedReader f = openFileForReading(filename);
        String[] s = f.readLine().split(" ");
        f.close();
        
        double[] vector = new double[s.length]; 

        for (int i = 0; i < s.length; i++) {
            vector[i] = Double.parseDouble(s[i]);
        }
        return vector;
    }
    
    /**
     * Save the contents of an array into a file where each element
     * is on its own line.
     * @param array array whose contents are to be saved
     * @param filename name of the file where to save
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static void saveArrayAsColumn(double[] array, String filename)
            throws FileNotFoundException {
        
        PrintStream fo = openFileForWriting(filename);
        
        for (double element : array)
            fo.println(element);
        fo.close();
    }
    
    /**
     * Save the contents of an array into a file as a row vector.
     * @param array array to be saved
     * @param filename name of the file
     * @param append whether to append in the end of the file or not
     * @throws FileNotFoundException if the file cannot be found
     */
    public static void saveArrayAsRow(int[] array, String filename,
            boolean append) throws FileNotFoundException {
        
        PrintStream fo = openFileForWriting(filename, append);
        
        for (int element : array)
            fo.printf("%d ", element);
        fo.println();
        fo.close();
    }
    
    /**
     * Save the pair of numbers into a single line in a file.
     * @param a the first number
     * @param b the second number
     * @param filename name of the file
     * @param append whether to append in the end of the file or not
     * @throws FileNotFoundException if the file cannot be found
     */
    public static void save(int a, double b, String filename,
            boolean append) throws FileNotFoundException {
        
        PrintStream fo = openFileForWriting(filename, append);
        
        fo.printf("%d %e\n", a, b);
        fo.close();
    }
    
    /**
     * Save the group of numbers into a single line in a file.
     * @param a the first number
     * @param b the second number
     * @param c the third number
     * @param filename name of the file
     * @param append whether to append in the end of the file or not
     * @throws FileNotFoundException if the file cannot be found
     */
    public static void save(int a, double b, double c,
            String filename, boolean append)
                    throws FileNotFoundException {
        
        PrintStream fo = openFileForWriting(filename, append);
        
        fo.printf("%d %f %f\n", a, b, c);
        fo.close();
    }
    
    /**
     * Save the contents of a matrix into a file where each row
     * is in its own line and the columns are separated by spaces.
     * @param matrix the matrix whose contents are to be saved
     * @param filename name of the file where to save
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static void saveMatrix(double[][] matrix, String filename)
            throws FileNotFoundException {
        
        PrintStream fo = openFileForWriting(filename);
        
        StringBuilder rowString;
        for (double[] row : matrix) {
            rowString = new StringBuilder();
            for (double element : row) {
                rowString.append(element);
                rowString.append(" ");
            }    
            rowString.deleteCharAt(rowString.length()-1);
            fo.println(rowString);
        }
        fo.close();
    }
    
}
