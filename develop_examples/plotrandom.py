import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("random.txt")

bins = 500
rangee = (-5, 5)
ylim = (0.0, 0.7)

plt.subplot(1, 2, 1)
plt.hist(data[:, 0], bins=bins, range=rangee, normed=True)
plt.xlim(rangee)
plt.ylim(ylim)

plt.subplot(1, 2, 2)
plt.hist(data[:, 1], bins=bins, range=rangee, normed=True)
plt.xlim(rangee)
plt.ylim(ylim)

plt.show()
