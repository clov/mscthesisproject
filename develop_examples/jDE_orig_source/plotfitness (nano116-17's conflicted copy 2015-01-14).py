import numpy as np
import matplotlib.pyplot as plt
import sys

evals, fitnesses = [], []

with open('f1jan.out', 'r') as f:

    while True:
        line = f.readline().strip()
        if line == '':
            break
        splitted_line = line.split(' ')
        evals.append(float(splitted_line[0]))
        fitnesses.append(float(splitted_line[-1]))

plt.plot(evals, fitnesses, 'k-')
plt.gca().set_yscale('log')
if len(sys.argv) > 1:
    plt.savefig(sys.argv[1])
plt.show()
