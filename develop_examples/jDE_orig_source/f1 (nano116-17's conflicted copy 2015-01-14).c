
#include "de.h"
#include "math.h"

double evaluate2(int n, double X[], long *nfeval)
{
    int i;
    double f = 0.0;
    double zi;
    
    (*nfeval)++;
    
    /* funtion f1 */
    for (i=0; i < n; i++) {
      zi = X[i];
      f += zi * zi;
    }
    
    return f;
}

double evaluate(int n, double X[], long *nfeval)
{
    double sum = 0.0;
    int i;
    double zi;

    (*nfeval)++;
    
    for (i = 0; i < n; i++) {
        zi = X[i];
        sum += zi*zi - 10.0*cos(2*M_PI*zi) + 10.0;
    }

    return sum;
}
