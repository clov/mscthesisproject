package develop_examples;

import java.util.Vector;
import java.util.Arrays;

/**
 * A playground.
 * @author Teemu Peltonen 
 * @version Feb 11, 2015
 */
public class JavaTest {
	
	/**
	 * @param args not used
	 */
	public static void main(String[] args) {

	    double khi = 0.8;
        double theta0 = 0.001;

        for (double dummy = 1; dummy < 1.1; dummy += 1) {
            for (double T0 = 0.1; T0 < 2.7; T0 += 0.2) {

                System.out.println(khi);
                System.out.println(T0);
                System.out.println(theta0);
                System.out.println();
            }
        }
  
	}

}