import numpy as np
import matplotlib.pyplot as plt
import os, sys

datafilename = sys.argv[1]
name, _ = os.path.splitext(datafilename)

custom_ylim = len(sys.argv) == 4
if custom_ylim:
    ylim = (float(sys.argv[2]), float(sys.argv[3]))

data = np.loadtxt(datafilename)
evals = data[:, 0]
fitnesses = data[:, 1]
generations = data[:, 2]
groupsizes = data[:, 3]

plt.plot(evals, fitnesses, 'k-')
plt.gca().set_yscale('log')
plt.xlim((evals[0], evals[-1]))
if custom_ylim:
    plt.ylim(ylim)
plt.savefig('figs/{0}.png'.format(name))
plt.show()

plt.clf()
plt.plot(generations, groupsizes, 'k.')
plt.ylim((0, 300))
plt.show()
