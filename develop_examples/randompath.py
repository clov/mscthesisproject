import numpy as np
import matplotlib.pyplot as plt

def are_linearly_dependent(u, v, rtol=1E-5, atol=1E-8):
    """Return true if the vectors u and v are linearly dependent,
    within a given tolerance, false otherwise."""
    
    unorm = np.linalg.norm(u)
    vnorm = np.linalg.norm(v)
    
    if (unorm == 0 or vnorm == 0):
        raise ValueError('u and v must not be zero vectors')

    uhat = u / unorm
    vhat = v / vnorm
    
    return np.allclose(uhat, vhat, rtol, atol) or\
           np.allclose(uhat, -vhat, rtol, atol)
    

def orthogonalize(v, u):
    """Make v orthogonal to u by using the idea from the
    Gram-Schmidt method."""
    
    if np.isclose(np.linalg.norm(u), 0) or\
       np.isclose(np.linalg.norm(v), 0):
        raise ValueError('u and v must not be zero vectors')
    
    u = np.array(u)
    v = np.array(v)
    
    return v - np.dot(v, u) / np.dot(u, u) * u
    

def generate_perpendicular_vector(v, u, norm):
    """From the vector v, generate a vector with given norm that is
    perpendicular to u."""
    
    if np.isclose(np.linalg.norm(u), 0):
        raise ValueError('u must not be a zero vector')
    if norm <= 0:
        raise ValueError('norm must be strictly positive')
    if are_linearly_dependent(u, v):
        raise ValueError('u and v must be linearly independent')
    
    u = np.array(u)
    v = orthogonalize(v, u)
    return v / np.linalg.norm(v) * norm
    

def _generate_path_recursive(A, B, points, m, mmax,
            rnd_vectors, norms, midpoint_idx, d):

    if m == mmax + 1:
        return
    
    norm = norms[m-1]
    rnd_vector = rnd_vectors.pop()

    displacement = generate_perpendicular_vector(rnd_vector, B-A,
                                                 norm)
    midpoint = displacement + 0.5*(A+B)
    points[midpoint_idx] = midpoint
    
    _generate_path_recursive(A, midpoint,
            points, m+1, mmax, rnd_vectors, norms,
            midpoint_idx-d, d/2)
    _generate_path_recursive(midpoint, B,
            points, m+1, mmax, rnd_vectors, norms,
            midpoint_idx+d, d/2)


def generate_path(A, B, rnd_vectors, norms):
    
    mmax = len(norms)
    rnd_vectors = list(rnd_vectors)
    
    points = [np.NaN] * (len(rnd_vectors)+2)
    points[0] = A
    points[-1] = B
    midpoint_idx = (len(points)-1) / 2

    _generate_path_recursive(A, B,
            points, 1, mmax, rnd_vectors, norms,
            midpoint_idx, midpoint_idx/2)

    return points


def single_point_example():
    
    A = np.array([-10., -1.])
    B = np.array([10., 10.])
    midpoint = 0.5 * (A+B)
    xs = [A[0], B[0], midpoint[0]]
    ys = [A[1], B[1], midpoint[1]]

    plt.plot(xs, ys, 'r.', markersize=10)

    rnd_vectors = [random_perpendicular_vector_middle(A, B, i/4.) for i in range(1, 20)]

    xs = [rnd_vector[0] for rnd_vector in rnd_vectors]
    ys = [rnd_vector[1] for rnd_vector in rnd_vectors]

    plt.plot(xs, ys, 'k.')
    plt.gca().set_aspect('equal')
    lim = (-15, 15)
    plt.xlim(lim)
    plt.ylim(lim)
    plt.show()


def path_example():

    A = np.array([-10., -10.])
    B = np.array([10., 10.])
    
    mmax = 6
    # use rnd_vectors and norms to represent a path!
    rnd_vectors = [np.random.rand(len(A)) for i in range(2**mmax-1)]
    # decrease the standard deviation when m increases
    norms = [np.abs(np.random.normal(0.0, 30.0 * 2**(-1.5*m))) for m in range(0, mmax)]
    #norms = [np.random.random() * 30.0* 2**(-1.5*m) for m in range(mmax)]
    points = generate_path(A, B, rnd_vectors, norms)
    #points = generate_path_from_displacements(A, B, displacements)
    
    xs = [point[0] for point in points]
    ys = [point[1] for point in points]

    plt.plot(xs, ys, 'r.')
    plt.plot(xs, ys, 'k-')
    lim = (-60, 60)
    plt.xlim(lim)
    plt.ylim(lim)
    plt.gca().set_aspect('equal')
    plt.show()


if __name__ == '__main__':
    #single_point_example()
    path_example()
    





