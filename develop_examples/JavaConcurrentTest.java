package develop_examples;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Learning parallel programming.
 * @author Teemu Peltonen
 * @version May 28, 2014
 */
public class JavaConcurrentTest implements Callable<Integer> {
    
    private int S = 0;
    private String word;
    private static Object lock = new Object();
    private InnerClass innerClass = new InnerClass();
    
    /**
     * @param word a String whose length is to be calculated
     */
    public JavaConcurrentTest(String word) {
        this.word = word;
    }
    
    @Override
    public Integer call() throws InterruptedException {
        /*
        synchronized(lock) {
            for (int i = 0; i < this.word.length(); i++) {
                Thread.sleep(100);
                System.out.print(this.word.charAt(i));
            }
        }
        */
        S++;
        innerClass.call();
        return new Integer(this.word.length());
    }
    
    /**
     * @author Teemu Peltonen
     * @version Nov 14, 2014
     */
    public class InnerClass {
        
        /**
         * 
         */
        public void call() {
            System.out.println("S = " + S);
        }
    }

    /**
     * Small tests
     * @param args not used
     * @throws Exception if failed
     */
    public static void main(String[] args) throws Exception {
        
        String[] words = {"kissa", "bcd"};
        
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        CompletionService<Integer> pool = new ExecutorCompletionService<Integer>(threadPool);
        //Set<Future<Integer>> futures = new HashSet<Future<Integer>>();
        
        for (String word : words) {
            Callable<Integer> callable = new JavaConcurrentTest(word);
            //Future<Integer> future = pool.submit(callable);
            //futures.add(future);
            pool.submit(callable);
        }
        int sum = 0;
        for (int i = 0; i < words.length; i++) {
            Future<Integer> result = pool.take();
            sum += result.get().intValue();
        }
        
        System.out.println();
        System.out.println(sum);
        threadPool.shutdownNow();

    }

}
