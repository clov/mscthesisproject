import numpy as np
import matplotlib.pyplot as plt

xs = np.linspace(0, 1, 10)
plt.plot(xs, xs**2, 'k-')
plt.show()