import numpy as np
import matplotlib.pyplot as plt

P = 15

class Number:
    """Represents a number in the interval [0,1]."""
    
    def __init__(self, value):
        if type(value) == float:
            self.value_base10 = value
            self.value_base2list = self.base10_to_base2list(value)
        else:
            self.value_base2list = value
            self.value_base10 = self.base2list_to_base10(value)

    def base2list_to_base10(self, value_base2list):
        sum = 0.
        for i in xrange(0, len(value_base2list)):
            bit = value_base2list[i]
            sum += bit * 2**(-i-1)
        return sum

    def base10_to_base2list(self, value_base10):
        x = value_base10
        f = 0.5
        base2list = np.empty(P, dtype=int)
        for q in range(P):
            if x >= f:
                base2list[q] = 1
                x = x - f
            else:
                base2list[q] = 0
            f = f / 2

        return base2list

    def get_value_base10(self):
        return self.value_base10

    def set_value_base10(self, x):
        self.value_base10 = x

    def get_fitness(self, f):
        return f(self.value_base10)

    def mutate(self, pm):
        x = self.value_base2list
        for q in range(P):
            r = np.random.random()
            if r < pm:
                # flip bit
                x[q] = 1 - x[q]

    def __str__(self):
        return str(self.value_base2list)

    def __getitem__(self, i):
        return self.value_base2list[i]
    

class Numbers:
    """Represents a population of numbers."""
    
    def __init__(self, numbers):
        self.numbers = numbers

    def cross(self, i1, i2):
        parent1 = self.numbers[i1]
        parent2 = self.numbers[i2]
        child1 = np.ones(P, dtype=int) * -1
        child2 = np.ones(P, dtype=int) * -1
        s = np.random.randint(0, P)
        for q in range(s):
            child1[q] = parent1[q]
            child2[q] = parent2[q]
        for q in range(s, P):
            child1[q] = parent2[q]
            child2[q] = parent1[q]
        
        return Number(child1), Number(child2)

    def get_best(self, f):
        best_idx = -1
        best_fitness = np.inf
        for i in range(len(self.numbers)):
            fitness = self.numbers[i].get_fitness(f)
            if fitness < best_fitness:
                best_idx = i
                best_fitness = fitness

        return best_idx, best_fitness

    def plot_population(self, f, i, t):
        plt.clf()
        x = np.linspace(0, 1, 200)
        plt.plot(x, f(x))
        positions = [number.get_value_base10() for number in
                     self.numbers]
        fitnesses = [number.get_fitness(f) for number in
                     self.numbers]
        plt.plot(positions, fitnesses, 'k.')
        plt.title('step {0}'.format(t))
        plt.savefig('figs/step0000{0}.png'.format(i))
    
    def __getitem__(self, i):
        return self.numbers[i]

    def __setitem__(self, i, item):
        self.numbers[i] = item


def f(x):
    return 10*np.abs(x-0.5)-np.cos(100*(x-0.5))+1

def minimize_locally(number, f, P):
    h = 0.00001
    delta = 0.05
    x = number.get_value_base10()
    while np.abs(delta) > 1. / 2**P:
        deriv = f(x+h) - f(x)
        if delta * deriv >= 0:
            delta = -0.5 * delta
        x += delta

    number.set_value_base10(x)


def minimize(f, M, nR, pm, best_fitnesses, plot_every):
    population = [Number(np.random.random()) for x in range(M)]
    population = Numbers(population)
    i = 0
    for t in xrange(nR):
        # choose 2 separate random parents for crossover
        parent1_idx = np.random.randint(0, M)
        parent2_idx = np.random.randint(0, M)
        while parent2_idx == parent1_idx:
            parent2_idx = np.random.randint(0, M)

        child1, child2 = population.cross(parent1_idx, parent2_idx)
        # mutate the childs
        child1.mutate(pm)
        child2.mutate(pm)

        # perform local optimization
        minimize_locally(child1, f, 20)
        minimize_locally(child2, f, 20)

        # replace parents if the children's fitnesses are better
        parent1 = population[parent1_idx]
        parent2 = population[parent2_idx]
        if child1.get_fitness(f) < parent1.get_fitness(f):
            population[parent1_idx] = child1
        if child2.get_fitness(f) < parent2.get_fitness(f):
            population[parent2_idx] = child2

        best_fitnesses.append(population.get_best(f)[1])
        if np.mod(t, plot_every) == 0:
            population.plot_population(f, i, t)
            i += 1

    best_idx, best_fitness = population.get_best(f)
    return population[best_idx].get_value_base10(), best_fitness


if __name__ == '__main__':
    best_fitnesses = []
    steps = 1000
    print minimize(f, 20, steps, 0.1, best_fitnesses, steps/10)
    plt.clf()
    plt.loglog(best_fitnesses)
    plt.show()
