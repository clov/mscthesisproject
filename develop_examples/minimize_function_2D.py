import numpy as np
import matplotlib.pyplot as plt

P = 15

class Number:
    """Represents a number in the interval [0,1]."""
    
    def __init__(self, value):
        if type(value) == float:
            self.value_base10 = value
            self.value_base2list = self.base10_to_base2list(value)
        else:
            self.value_base2list = value
            self.value_base10 = self.base2list_to_base10(value)

    def base2list_to_base10(self, value_base2list):
        sum = 0.
        for i in xrange(0, len(value_base2list)):
            bit = value_base2list[i]
            sum += bit * 2**(-i-1)
        return sum

    def base10_to_base2list(self, value_base10):
        x = value_base10
        f = 0.5
        base2list = np.empty(P, dtype=int)
        for q in range(P):
            if x >= f:
                base2list[q] = 1
                x = x - f
            else:
                base2list[q] = 0
            f = f / 2

        return base2list

    def get_value_base10(self):
        return self.value_base10

    def set_value_base10(self, x):
        self.value_base10 = x

    def get_fitness(self, f):
        return f(self.value_base10)

    def mutate(self, pm):
        x = self.value_base2list
        for q in range(P):
            r = np.random.random()
            if r < pm:
                # flip bit
                x[q] = 1 - x[q]

    def __str__(self):
        return str(self.value_base2list)

    def __getitem__(self, i):
        return self.value_base2list[i]


class Vector:

    def __init__(self, (x, y)):
        """Initializes the vector with the components x and y, where
        x and y both have to be Number instances."""
        self.x = x
        self.y = y

    def get_fitness(self, f):
        x_value = self.x.get_value_base10()
        y_value = self.y.get_value_base10()
        return f((x_value, y_value))

    def mutate(self, pm):
        self.x.mutate(pm)
        self.y.mutate(pm)

    def get_component(self, comp):
        if comp == 'x':
            return self.x
        if comp == 'y':
            return self.y

    def get_component_value_base10(self, comp):
        return self.get_component(comp).get_value_base10()

    def get_vector_value_base10(self):
        return (self.x.get_value_base10(), self.y.get_value_base10())
        

class Vectors:
    """Represents a population of vectors."""
    
    def __init__(self, vectors):
        """vectors: array-like object of Vector instances"""
        self.vectors = vectors 

    def _cross_component(self, parent1, parent2, comp):
        child1_comp = np.ones(P, dtype=int) * -1
        child2_comp = np.ones(P, dtype=int) * -1
        s = np.random.randint(0, P)
        for q in range(s):
            child1_comp[q] = parent1.get_component(comp)[q]
            child2_comp[q] = parent2.get_component(comp)[q]
        for q in range(s, P):
            child1_comp[q] = parent2.get_component(comp)[q]
            child2_comp[q] = parent1.get_component(comp)[q]

        return Number(child1_comp), Number(child2_comp)


    def cross(self, i1, i2):
        parent1 = self[i1]
        parent2 = self[i2]

        child1x, child2x = self._cross_component(parent1, parent2, 'x')
        child1y, child2y = self._cross_component(parent1, parent2, 'y')

        return Vector((child1x, child1y)), Vector((child2x, child2y))


    def get_best(self, f):
        """Note: here the fitness function is actually a cost function
        (lower is better)"""
        best_idx = -1
        best_fitness = np.inf
        for i in range(len(self.vectors)):
            fitness = self.vectors[i].get_fitness(f)
            if fitness < best_fitness:
                best_idx = i
                best_fitness = fitness

        return best_idx, best_fitness

    def plot_population(self, f, i, t):
        plt.clf()
        x = np.linspace(0., 1., 200)
        y = np.linspace(0., 1., 200)
        X, Y = np.meshgrid(x, y)
        plt.contour(X, Y, f((X, Y)))
        #positions = [vector.get_vector_base10() for vector in
        #             self.vectors]
        vectors_x = [vector.get_component_value_base10('x')
                     for vector in self.vectors]
        vectors_y = [vector.get_component_value_base10('y')
                     for vector in self.vectors]
        #fitnesses = [vector.get_fitness(f) for vector in
        #             self.vectors]
        plt.plot(vectors_x, vectors_y, 'k.')
        plt.title('step {0}'.format(t))
        plt.savefig('figs/step0000{0}.png'.format(i))
    
    def __getitem__(self, i):
        return self.vectors[i]

    def __setitem__(self, i, item):
        self.vectors[i] = item


def f((x, y)):
    return 4*np.abs((2*x-1)**2+(2*y-1)**2)\
            +np.sin(15*((2*x-1)**2+(2*y-1)**2))

def ackley((x, y)):
    return 20.+np.exp(1)\
     -20.*np.exp(-0.1*np.sqrt(((10.*(x-0.5))**2+(10.*(y-0.5))**2))/2)\
     -np.exp((np.cos(10.*2.*np.pi*(x-0.5))+np.cos(10.*2.*np.pi*(y-0.5)))/2)
            
#FIXME transform into 2D
def minimize_locally(number, f, P):
    h = 0.00001
    delta = 0.05
    x = number.get_value_base10()
    while np.abs(delta) > 1. / 2**P:
        deriv = f(x+h) - f(x)
        if delta * deriv >= 0:
            delta = -0.5 * delta
        x += delta

    number.set_value_base10(x)


def minimize(f, M, nR, pm, best_fitnesses, plot_every):
    population = []
    for t in range(M):
        x = Number(np.random.random())
        y = Number(np.random.random())
        population.append(Vector((x, y)))
    population = Vectors(population)

    i = 0
    for t in xrange(nR):
        # choose 2 separate random parents for crossover
        parent1_idx = np.random.randint(0, M)
        parent2_idx = np.random.randint(0, M)
        while parent2_idx == parent1_idx:
            parent2_idx = np.random.randint(0, M)

        child1, child2 = population.cross(parent1_idx, parent2_idx)
        # mutate the childs
        child1.mutate(pm)
        child2.mutate(pm)

        # perform local optimization
        #minimize_locally(child1, f, 20)
        #minimize_locally(child2, f, 20)

        # replace parents if the children's fitnesses are better
        parent1 = population[parent1_idx]
        parent2 = population[parent2_idx]
        if child1.get_fitness(f) < parent1.get_fitness(f):
            population[parent1_idx] = child1
        if child2.get_fitness(f) < parent2.get_fitness(f):
            population[parent2_idx] = child2

        best_fitnesses.append(population.get_best(f)[1])
        if np.mod(t, plot_every) == 0:
            population.plot_population(f, i, t)
            i += 1

    best_idx, best_fitness = population.get_best(f)
    return population[best_idx].get_vector_value_base10(), best_fitness


if __name__ == '__main__':

    best_fitnesses = []
    steps = 10000
    print minimize(ackley, 20, steps, 0.1, best_fitnesses, steps/10)
    plt.clf()
    plt.loglog(best_fitnesses)
    plt.show()

