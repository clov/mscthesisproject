import numpy as np
import matplotlib.pyplot as plt

def V(x, y):

    xSqrd = x * x
    ySqrd = y * y
    sum = 1 - xSqrd - ySqrd

    return sum * sum + ySqrd / (xSqrd + ySqrd)

def V2(x, y):
    
    xSqrd = x * x;
    ySqrd = y * y;
    return xSqrd*xSqrd + 4 * xSqrd * ySqrd - 2 * xSqrd +\
            2 * ySqrd

delta = 0.025
x = np.arange(-1.5, 1.5, delta)
y = np.arange(-1.5, 1.5, delta)
X, Y = np.meshgrid(x, y)
Z = V(X, Y)

plt.contourf(X, Y, Z, levels=np.arange(0.0, 2.0, 0.05))

#fname = '../runs/MEPproblem_67D/DE/NP100_F0.5_CR0.9/xBestRun.txt'
fname = '../runs/MEPproblem_67D/JDE/NP100/xBestRun.txt'

data = np.loadtxt(fname)
xs = data[:, 0]
ys = data[:, 1]

plt.plot(xs, ys, 'r.')
plt.plot(xs, ys, 'k-')

"""
lim = (-60, 60)
plt.xlim(lim)
plt.ylim(lim)
"""
plt.gca().set_aspect('equal')

plt.savefig('MEP.png')
plt.show()
