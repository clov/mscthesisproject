import numpy as np
from randompath import are_linearly_dependent,\
        generate_perpendicular_vector

class TestRandomPath:

    def test_are_linearly_dependent(self):

        f = are_linearly_dependent
        assert     f((1., 0.), (1., 0.))
        assert     f((1., 0.), (3.5, 0.))
        assert not f((1., 0.), (3.5, 0.01))
        assert     f((-2.2, 0.), (4.3, 0.))
        assert     f((0., 2.1), (0., -5.6))
        assert not f((0., 2.1), (0.01, -5.6))
        assert     f((1., 1.), (2., 2.))
        assert not f((1., 1.), (2., 2.01))
        assert     f((-1., 1.), (1., -1.))
        assert     f((1., 2., -1.), (3., 6., -3.))


    def test_generate_perpendicular_vector(self):

        f = generate_perpendicular_vector

        u = (1., 0.)

        shouldbe = (0., 1.)
        v = f((0., 1.), u, 1.)
        assert np.allclose(v, shouldbe)
        v = f((3.2, 1.3), u, 1.)
        assert np.allclose(v, shouldbe)
        v = f((-4.1, 1.1), u, 1.)
        assert np.allclose(v, shouldbe)

        shouldbe = (0., -1.)
        v = f((0., -1.), u, 1.)
        assert np.allclose(v, shouldbe)
        v = f((3.1, -0.1), u, 1.)
        assert np.allclose(v, shouldbe)
        v = f((1.2, -5.5), u, 1.)
        assert np.allclose(v, shouldbe)

        v = f((0., 1.), u, 2.5)
        assert np.allclose(v, (0., 2.5))

        u = (1., 1.)

        v = f((1., 2.), u, np.sqrt(2))
        assert np.allclose(v, (-1., 1.))
        v = f((1., -2.), u, np.sqrt(2))
        assert np.allclose(v, (1., -1))


