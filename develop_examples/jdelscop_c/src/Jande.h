
#ifndef Jande
#define Jande

const int nFun = 19;  // number of functions  
const int nRun = 25;  // run 25 times each function

//FIXME modified
//const int nDim = 5;   // number of different dimensions
//const int DIM[nDim] = { 50, 100, 200, 500, 1000 };  // actual dimensions
const int nDim = 1;   // number of different dimensions
const int DIM[nDim] = { 3 };  // actual dimensions
//const int nDim = 1;   // number of different dimensions
//const int DIM[nDim] = { 1000 } ;  // actual dimensions

const int maxNP = 5000; // max. size of NP
const int maxD  = 1100; // max. size of D (+ some for self-adaptive control parameters)

// LSCOP: Range
//const double Range[nFun] = {100, 100, 100, 5, 600, 32, 10, 65.536, 100, 15, 100, 100, 100, 5, 10, 100, 10, 100, 100, 5, 10};
const double Range[nFun] = {100, 100, 100, 5, 600, 32, 10, 65.536, 100, 15, 100, 100, 100, 5, 10, 100, 100, 5, 10};

const double fOpt[nFun] = { -450, -450, 390, -330, -180, -140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
#endif // Jande
