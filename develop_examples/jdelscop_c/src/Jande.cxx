/*
 * LSCOP, februar 2010
 * Janez Brest
 */
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <string>
#include <cstdlib>
#include <cmath>
#include "Jande.h"
using namespace std;

extern "C" {
    #include <sys/time.h>
    #include "ura.h"
    #include "funsoft.h"
    extern tFitness func(int func_number , int dim , double* x);
}

// GLOBAL -- memory
tFitness REZ[nDim][nFun][nRun];   // results: Dimension, Function, Run 
double pop[maxNP][maxD];          // population
double newPop[maxNP][maxD];       // new population

double SEK[nDim][nFun][nRun];     // times: Dimension, Function, Run 
struct timeval time_start;



// [0, 1)
double mRandom() {
  return rand()/(RAND_MAX + 1.0);
}

// [0, val) intValue
unsigned int mRandomInt(int upper) {
  return (int)(upper * mRandom());
}


void assignd(const int size, double dest[], const double source[]) { // dest := source
   for (int i = 0; i < size; i++)
      dest[i] = source[i];
}

// prototypes
int popReduce(double pop[maxNP][maxD], const int extDim, const int NP, tFitness cost[maxNP]);
int popReduceS2(double pop[maxNP][maxD], const int extDim, const int NP, tFitness cost[maxNP]);
void jDE(const int i, double pop[maxNP][maxD], const int D, const int NP, const int extDim, double tmp[maxD], double best[maxD], tFitness cost[maxNP], string strategy, int it, int MaxFES); 
void writeResults(tFitness REZ[nDim][nFun][nRun], int izpisiDim);
void writeTimes(double SEK[nDim][nFun][nRun], int izpisiDim);


int main(int argc, char**argv) {

   int dim;
   int i, it;
   int iDim;
   int iFunk;
   int iRun;
   int NumReduce;       // 0.. no reduction, 1, ... max. num. of reductions
   double best[maxD];
   tFitness b, c;   
   tFitness cost[maxNP];
   double nov[maxD];
   double min, max;
   int j, k;
   int imin;
   int izbran; 
   string strategy;

   const tFitness approxLevel = 1e-14;  // to approximate all the average errors below 1e-14 to 0.0;

   for (iDim = 0; iDim < nDim; iDim++) { // DIMENSION
      dim = DIM[iDim];  // get a dimension
      NumReduce = 3;    // 7.2.2010 pmax := NumReduce+1 ==> pmax = 3+1 = 4 
      int extDim = dim + 30; // extended dim to store control paramaters
      const int MaxFES = dim * 5000;     // maximum number of fitness evaluations is 5000*D
      for (iFunk = 0; iFunk < nFun; iFunk++) {  // FUNCTION
         cout << "D" << dim << " F" << 1+iFunk << endl;
         for (iRun = 0; iRun < nRun; iRun++) {  // RUN
            int NP = 100;                       // NPinit

            time_start = ura_init();

            // Obtain the lower and upper bound of the search space.
            // This boundary is the same for all dimensions.
            max = Range[iFunk];
            min = -max;

            // All functions are subject to minimization, hence this is the worst
            // objective value possible.
            // ERROR: b = numeric_limits<double>::max();
            b = numeric_limits<tFitness>::max();

            for (k = 0; k < NP; k++) {
               for (j = 0; j<dim; j++) {
                  pop[k][j] = (min + (mRandom() * (max - min)));
               }
               for (j = dim; j<extDim; j += 2) {
                  pop[k][j] = 0.5;   // self-ad. F init.
                  pop[k][j+1] = 0.9; // self-ad. CR init.
               }
               cost[k] = numeric_limits<tFitness>::max(); // minimization
            }
            imin = 0;

            for (it = 0; it < MaxFES; it++) {  // iterations; 
               i = it % NP;

               if (mRandom() < 0.1 && it > MaxFES/2)
                  strategy = "jDEbest";
               else if (i < NP/2)
                  strategy = "jDEbin";
               else
                  strategy = "jDEexp";

               jDE(i, pop, dim, NP, extDim, nov, best, cost, strategy, it, MaxFES); // calculate i-th offspring
               izbran = i;
               
               // border check;
               for (j = 0; j<dim; j++) {
                  if (nov[j] < min) nov[j]=min;
                  if (nov[j] > max) nov[j]=max;
               }

               c = func(1+iFunk, dim, nov) - fOpt[iFunk]; // function call - fOptimum

               if (c <= cost[izbran]) {                   // offspring has better or equal fitness value
                  cost[izbran] = c; 
                  assignd(extDim, newPop[izbran], nov);   // newPop[izbran] := nov

                  if (c <= b) {
                     //cout << it << " : " << b << " --> " << c << " imin: " << imin << endl;
                     imin = izbran;
                     b = c;
                     assignd(extDim, best, nov);  // best := nov
                  }   
               }
               else {
                  assignd(extDim, newPop[izbran], pop[izbran]);  // newPop[izbran] := pop[izbran]
               }

               if (NumReduce > 0 && it % (MaxFES / (NumReduce+1)) == (MaxFES/(NumReduce+1) - 1) && NP > 10 && it < MaxFES-1) {
                  // *** reduction type APIN2008, CEC2008
                  //imin = imin/2;
                  //NP = popReduce(newPop, extDim, NP, cost);

                  // *** new reduction ***
                  if(imin < NP/2) 
                     imin = imin/2;
                  else
                     imin = (imin-NP/2)/2;
                  
                  NP = popReduceS2(newPop, extDim, NP, cost);
                  //cout << "REDUCTION: it: " << it << "new NP: " << NP << endl;
               }

               if (i == NP-1) {
                  for (k=0; k < NP; k++) {
                     assignd(extDim, pop[k], newPop[k]);  // pop[k] := newPop[k]
                      
                  }
               }

            } // for it
            SEK[iDim][iFunk][iRun] = ura_razlika(time_start, ura_init()); 
            cout << "D" << dim << " F" << 1+iFunk << " iRun: " << iRun << " final fitness: " << b << " --> " << ((b <= approxLevel)? 0.0 : b) << " in iteration " << it << " time[s]: " << SEK[iDim][iFunk][iRun] << endl;
            REZ[iDim][iFunk][iRun] = (b <= approxLevel)? 0.0 : b; // using approx. level for output

         } // iRun
      } // iFunk
      writeResults(REZ,iDim+1);  
      writeTimes(SEK,iDim+1);  
   } // iDim
}  // END MAIN

int popReduce(double pop[maxNP][maxD], const int extDim, const int NP, tFitness cost[maxNP]) {
     int i;
     for (i=0; i< NP/2; i++) {
        if (cost[i] > cost[NP/2+i]) {
           assignd(extDim, pop[i], pop[NP/2+i]);
           cost[i] = cost[NP/2+i];
        }
     }
     return (NP+1)/2;
  }

int popReduceS2(double pop[maxNP][maxD], const int extDim, const int NP, tFitness cost[maxNP]) {
     int i;
     for (i=0; i< NP/4; i++) {
        if (cost[i] > cost[NP/4+i]) {
           assignd(extDim, pop[i], pop[NP/4+i]);
           cost[i] = cost[NP/4+i];
        }
     }
     for (i=0; i< NP/4; i++) {
        if (cost[NP/2+i] > cost[3*NP/4+i]) {
           assignd(extDim, pop[NP/2+i], pop[3*NP/4+i]);
           cost[NP/2+i] = cost[3*NP/4+i];
        }
     }

     for (i=0; i< NP/4; i++) {  // copy
        assignd(extDim, pop[NP/4+i], pop[NP/2+i]);
        cost[NP/4+i] = cost[NP/2+i];
     }

     return (NP+1)/2;
}

void chooseRs(int LB, int UB, int NP, int i, int &r1, int &r2, int& r3) {
   int size = UB - LB; // 
   do { 
       r1 = LB + mRandomInt(size);
   }while(r1==i);
   do { 
       r2 = LB + mRandomInt(size);
       //r2 = mRandomInt(NP);
   }while(r2==i || r2==r1);
   do { 
       r3 = LB + mRandomInt(size);
       //r3 = mRandomInt(NP);
   }while(r3==i || r3==r1 || r3==r2);

}

void jDE(const int i, double pop[maxNP][maxD], const int D, const int NP, const int extDim, double tmp[maxD], double best[maxD], tFitness cost[maxNP], string strategy, int it, int MaxFES) {
     int r1, r2, r3;
     double tau1 = 0.1;
     double tau2 = 0.1;
     double F, CR;
     int L, n;

     assignd(extDim, tmp, pop[i]);    // tmp := pop[i]

     chooseRs(0,NP,NP,i,r1,r2,r3);     // normal
 
     int odmik = 0;

     // Each strategy has its own F and CR parameters
     if (strategy == "jDEbin") odmik = 0;
     if (strategy == "jDEexp") odmik = 2;
     if (strategy == "jDEbest") odmik = 4;
     n = mRandomInt(D);
       
     // *** LOWER and UPPER values for strategies *** 
     double F_l = 0.1+sqrt(1.0/NP); // for small NP   
     double CR_l;
     double CR_u = 0.95;
     if (strategy == "jDEbin") { CR_l = 0.00; CR_u=1.0; }
     else if (strategy == "jDEexp" ) { CR_l = 0.3; CR_u=1.0; F_l = 0.5; }
     else { F_l = 0.4; CR_l = 0.7; }  // jDEbest

     if (mRandom()<tau1) {
        tmp[D+odmik] = F = F_l + mRandom() * (1 - F_l);
     }
     else {
        F = tmp[D+odmik];
     }
     if (mRandom()<tau2) {
        tmp[D+1+odmik] = CR = CR_l + mRandom() * (CR_u-CR_l);
     }
     else {
        CR = tmp[D+1+odmik];
     }

     if (strategy == "jDEbin") {
        if (mRandom() < 0.75 && cost[r2] > cost[r3])  // COST, sign change 
           F = -F;

        for (L=0; L<D; L++) /* perform D binomial trials */
        {
           if ((mRandom() < CR) || L == (D-1)) /* change at least one parameter */
           { 
              tmp[n] = pop[r1][n] + F*(pop[r2][n]-pop[r3][n]);
           }
           n = (n+1)%D;
        }
     }
     /*-------DE/rand/1/exp-------------------------------------------------------------------*/
     else if (strategy == "jDEexp") /* strategy DE1 in the techreport */
     {
        if (mRandom() < 0.75 && cost[r2] > cost[r3])  // COST, sign change 
           F = -F;

        L = 0;
        //F = 0.5; CR=0.9; 
        do
        {                       
           tmp[n] = pop[r1][n] + F*(pop[r2][n]-pop[r3][n]);
           n = (n+1)%D;
           L++;
        }while((mRandom() < CR) && (L < D));
     }
     else if ("jDEbest" == strategy) {         // 
        for (L=0; L<D; L++) /* perform D binomial trials */
        {
           if ((mRandom() < CR) || L == (D-1)) /* change at least one parameter */
           {  
             tmp[n] = F*(best[n]) + F*(pop[r2][n]-pop[r3][n]);
           }
           n = (n+1)%D;
        }
     }
     else {         // 
        cerr << "ERROR! No strategy: " << strategy << endl;
	exit(1);
     }

  } // end jDE   


void sort(tFitness SQ[nRun]) {
   tFitness tmp;
   for (int i=0; i < nRun; i++) 
      for (int j=0; j < nRun-1; j++) 
         if (SQ[j] > SQ[j+1]) {
            tmp = SQ[j];
            SQ[j] = SQ[j+1];
            SQ[j+1] = tmp;
         }
}

string wrInt(int ii) {
  ostringstream oss; 
  oss << ii;
  string a = oss.str(); 
  return a;
}
string wrTF(tFitness t) {
  ostringstream oss; 
  oss.precision(2); 
  oss << scientific << t;
  string a = oss.str(); 
  return a;
}

// tables and cvs
void writeResults(tFitness REZ[nDim][nFun][nRun], int izpisDim) {
   int iDim;
   int iFunk;
   int iRun;
   int i;

   tFitness mean[nFun];
   tFitness std[nFun];
   tFitness data[nFun][5]; // best, median, worst, avg, std.dev
   tFitness SQ[nRun];

   for (iDim = 0; iDim < izpisDim; iDim++) { // DIMENSION
      int dim = DIM[iDim];  // get a dimension
  
      string rez = "";
      rez += " \\begin{table*}[htb] \n";
      rez += " \\begin{small} \n";
      rez += " \\begin{center} \n";
      rez += " \\caption{Experimental Results with dimension $D=" + wrInt(dim) + "$.} \\label{table:D" +wrInt(dim)+ "} \n";
      rez += " \\begin{tabular}{cccccccc} \n";
      for (iFunk=0; iFunk < nFun; iFunk++) {
        // 
        mean[iFunk] = 0;
        std[iFunk] = 0;
        for (iRun = 0; iRun < nRun; iRun++) {
           SQ[iRun] = REZ[iDim][iFunk][iRun]; 
           mean[iFunk] += SQ[iRun];
        }
        sort(SQ);
        mean[iFunk] /= nRun;
        double xi;
        for (iRun = 0; iRun < nRun; iRun++) {   //STD
           xi = REZ[iDim][iFunk][iRun];
           std[iFunk] +=  (mean[iFunk] - xi)*(mean[iFunk] - xi); 
        }
        std[iFunk] = sqrt(std[iFunk]/(nRun-1));   // 
      
        // all results put into data 
        data[iFunk][0] = SQ[0];
        data[iFunk][1] = SQ[nRun/2];
        data[iFunk][2] = SQ[nRun-1];
        data[iFunk][3] = mean[iFunk];
        data[iFunk][4] = std[iFunk];
     }

     string hlineBA = " \\noalign{\\smallskip}\\hline\\noalign{\\smallskip} \n";
     string hlineB =  " \\noalign{\\smallskip}\\hline \n";
     string hlineA =  " \\hline\\noalign{\\smallskip} \n";
     string clineBA = " \\noalign{\\smallskip}\\cline{1-6}\\noalign{\\smallskip} \n";
     string clineB =  " \\noalign{\\smallskip}\\cline{1-6} \n";
     string clineA =  " \\cline{1-6}\\noalign{\\smallskip} \n";

     // Scienfic output x.xxe+xx  (1.23e45)
     rez += hlineA;
     rez += "       & $F_{1}$ & $F_{2}$ & $F_{3}$ & $F_{4}$ & $F_{5}$ & $F_{6}$ & $F_{7}$ \\\\ \n"; 
     rez += hlineBA;
     rez += " Best   ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     rez += " Median ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     rez += " Worst  ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean   ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std    ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += hlineBA;

     rez += "       & $F_{8}$ & $F_{9}$ & $F_{10}$ & $F_{11}$ & $F_{12}$ & $F_{13}$ & $F_{14}$ \\\\ \n"; 
     rez += hlineBA;
     rez += " Best   ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     rez += " Median ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     rez += " Worst  ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean   ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std    ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += hlineBA;

     //rez += clineA;
     rez += "       & $F_{15}$ & $F_{16}*$ & $F_{17}*$ & $F_{18}*$ & $F_{19}*$  \\\\ \n"; 
     rez += clineBA;
     rez += " Best   ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     rez += " Median ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     rez += " Worst  ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean   ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std    ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += clineB;
     rez += " \\end{tabular} \n";
     rez += " \\end{center} \n";
     rez += " \\end{small} \n";
     rez += " \\end{table*} \n";

     string fileName = "tabelaD" + wrInt(dim) + ".tex";
     cout << "Pisem v datoteko: " << fileName << endl;
     ofstream outFile(fileName.c_str(), ios::out);
     // write to file
     outFile << rez << endl;
     outFile.close();

     // data.cvs
     fileName = "dataD" + wrInt(dim) + ".cvs";
     cout << "Pisem v datoteko: " << fileName << endl;
     ofstream cvsFile(fileName.c_str(), ios::out);
     cvsFile << "Fun,D" << wrInt(dim) << "," << endl; 
     for (i = 0; i < nFun; i++) {
        cvsFile << "F" << wrInt(i+1) << (i+1>=16?"*":"") << "," << wrTF(data[i][3]) << "," << endl; 
     }
     cvsFile << "," << endl;
     cvsFile.close();


  }  // DIM
}

// TIMES
void writeTimes(double SEK[nDim][nFun][nRun], int izpisDim) {
   int iDim;
   int iFunk;
   int iRun;
   int i;

   tFitness mean[nFun];
   tFitness std[nFun];
   tFitness data[nFun][5]; // best, median, worst, avg, std.dev
   tFitness SQ[nRun];

   for (iDim = 0; iDim < izpisDim; iDim++) { // DIMENSION
      int dim = DIM[iDim];                   // get a dimension
  
      string rez = "";
      rez += " \\begin{table*}[htb] \n";
      rez += " \\begin{small} \n";
      rez += " \\begin{center} \n";
      rez += " \\caption{Execution times with dimension $D=" + wrInt(dim) + "$.} \\label{table:casiD" +wrInt(dim)+ "} \n";
      rez += " \\begin{tabular}{cccccccc} \n";
       
      for (iFunk=0; iFunk < nFun; iFunk++) {

        // for each function 
        mean[iFunk] = 0;
        std[iFunk] = 0;
        for (iRun = 0; iRun < nRun; iRun++) {
           SQ[iRun] = SEK[iDim][iFunk][iRun]; 
           mean[iFunk] += SQ[iRun];
        }
        sort(SQ);
        mean[iFunk] /= nRun;
        double xi;
        for (iRun = 0; iRun < nRun; iRun++) {   //STD
           xi = SEK[iDim][iFunk][iRun];
           std[iFunk] +=  (mean[iFunk] - xi)*(mean[iFunk] - xi); 
        }
        std[iFunk] = sqrt(std[iFunk]/(nRun-1));   // 
      
        // all results put into data 
        data[iFunk][0] = SQ[0];
        data[iFunk][1] = SQ[nRun/2];
        data[iFunk][2] = SQ[nRun-1];
        data[iFunk][3] = mean[iFunk];
        data[iFunk][4] = std[iFunk];
     }

     string hlineBA = " \\noalign{\\smallskip}\\hline\\noalign{\\smallskip} \n";
     string hlineB =  " \\noalign{\\smallskip}\\hline \n";
     string hlineA =  " \\hline\\noalign{\\smallskip} \n";
     string clineBA = " \\noalign{\\smallskip}\\cline{1-6}\\noalign{\\smallskip} \n";
     string clineB =  " \\noalign{\\smallskip}\\cline{1-6} \n";
     string clineA =  " \\cline{1-6}\\noalign{\\smallskip} \n";

     // Scienfic format x.xxe+xx  (1.23e45)
     rez += hlineA;
     rez += "       & $F_{1}$ & $F_{2}$ & $F_{3}$ & $F_{4}$ & $F_{5}$ & $F_{6}$ & $F_{7}$ \\\\ \n"; 
     rez += hlineBA;
     //rez += " Best   ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     //rez += " Median ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     //rez += " Worst  ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std  ";  for (i = 0; i < 7; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += hlineBA;

     rez += "       & $F_{8}$ & $F_{9}$ & $F_{10}$ & $F_{11}$ & $F_{12}$ & $F_{13}$ & $F_{14}$ \\\\ \n"; 
     rez += hlineBA;
     //rez += " Best   ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     //rez += " Median ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     //rez += " Worst  ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean   ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std    ";  for (i = 7; i < 14; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += hlineBA;
     rez += "       & $F_{15}$ & $F_{16}*$ & $F_{17}*$ & $F_{18}*$ & $F_{19}*$  \\\\ \n"; 
     rez += clineBA;
     //rez += " Best   ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][0]);  rez += " \\\\ \n"; 
     //rez += " Median ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][1]);  rez += " \\\\ \n"; 
     //rez += " Worst  ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][2]);  rez += " \\\\ \n"; 
     rez += " Mean   ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][3]);  rez += " \\\\ \n"; 
     rez += " Std    ";  for (i = 14; i < 19; i++)  rez += " & " + wrTF(data[i][4]);  rez += " \\\\ \n"; 
     rez += clineB;
     rez += " \\end{tabular} \n";
     rez += " \\end{center} \n";
     rez += " \\end{small} \n";
     rez += " \\end{table*} \n";

     string fileName = "casiD" + wrInt(dim) + ".tex";
     cout << "Pisem v datoteko: " << fileName << endl;
     ofstream outFile(fileName.c_str(), ios::out);
     // write to file
     outFile << rez << endl;
     outFile.close();
  }  // DIM
}



/*
  public static final double calcDev(final double[][] pop, final int D, final int NP) {
     int i,j;
     double minmax = Double.POSITIVE_INFINITY;
     double maxmax = 0.0;
     double min, max;
     for(j=0; j < D; j++) {
        min = max = pop[0][j];
        for (i=1; i<NP; i++) {
           if (min > pop[i][j]) min = pop[i][j];
           if (max < pop[i][j]) max = pop[i][j];
        }
        if (minmax > (max-min))  minmax = max-min;
        if (maxmax < (max-min))  maxmax = max-min;
     }
     //System.out.println("minmax:" + minmax + "  maxmax:" + maxmax);
     return minmax;
  }
*/

