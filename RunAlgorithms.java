import java.io.File;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import algorithms.interfaces.Algorithm;
import algorithms.interfaces.GenerationLoggers;
import algorithms.interfaces.IndividualLoggers;
import algorithms.utils.Best;
import algorithms.utils.MeanResult;
import algorithms.utils.MeanResults;
import algorithms.utils.ResultsUtils;
import algorithms.utils.SaveBestFitness;

import algorithms.Dummy;
import algorithms.S;
import algorithms.SA;
import algorithms.SAcoleman;
import algorithms.MDE_pBX;
import algorithms.MDE_pBX_orig;
import algorithms.CCPSO2;
import algorithms.JDE;
import algorithms.DE;

import algorithms.CCPSO2.SaveGroupSize;

import problems.interfaces.Problem;

import problems.AckleyProblem;
import problems.CoranasParabolaProblem;
import problems.GriewankProblem;
import problems.Ixi4WithNoiseProblem;
import problems.LJclusterProblem;
import problems.MEPproblem;
import problems.RosenbrockProblem;
import problems.SphereProblem;
import problems.DifferentPowersProblem;
import problems.SchwefelProblem;
import problems.RastriginProblem;
import problems.StepProblem;
import problems.SumProdProblem;
import problems.SumSumProblem;
import problems.XsinSumProblem;

import utils.FileUtils;
import utils.MathUtils;
import utils.random.RandUtils;

/**
 * @author modified by Teemu Peltonen
 * @version Apr 1, 2015
 */
public class RunAlgorithms {

    private static Object printLock = new Object();
    private static Object bestRunLock = new Object();

    // parameters set of the best run of an algorithm
    // (not in fact parameters, but the configuration)
    private static double[] xBestRun = null;

    //Fitness value of the best run of an algorithm
    private static double fxBestRun = Double.MAX_VALUE;

    /**
     * Run a single algorithm for one time with the ID repNr.
     * @param a the algorithm to run
     * @param repNr ID of the current run
     * @param folder working directory for the run
     * @param maxEvaluations maximum number of function evaluations
     * @param log whether to log the repetition
     * @return the final fitness value
     */
    private static double runAlgorithmRepetition(Problem problem,
            Algorithm a, int repNr, String folder,
            int maxEvaluations, boolean log)
    throws Exception {	

        // run the optimization algorithm
        long t0 = System.currentTimeMillis();

        // budget
        Vector<Best> bests = a.execute(problem, maxEvaluations,
                folder, repNr);

        synchronized(bestRunLock) {

            if (xBestRun == null)
            {	
                xBestRun = a.getFinalBest();
                // the best fitness value is at the last step
                fxBestRun = bests.get(bests.size()-1).getfBest();
            }
            else if(fxBestRun > bests.get(bests.size()-1).getfBest())
            {
                xBestRun = a.getFinalBest();
                fxBestRun = bests.get(bests.size()-1).getfBest();
            }
        }

        synchronized(printLock) {
            System.out.printf("%s, %dD, %s, %s, repetition %d: final fitness %e, elapsed time %d ms\n",
                    problem.getName(), problem.getDimension(),
                    a.getName(), a.getParametersAsPrintableString(),
                    repNr, bests.get(bests.size()-1).getfBest(),
                    System.currentTimeMillis()-t0);
        }

        if (log) {
            // save results
            String fileName = folder+"bestfitness_rep"+repNr+".txt";
            ResultsUtils.saveBests(bests, fileName);
        }

        return bests.get(bests.size()-1).getfBest();
    }

    private static class AlgorithmRepetitionThread
    implements Callable<Double> {

        private Problem problem;
        private Algorithm algorithm;
        private int repNr;
        private String dir;
        private int maxEvaluations;
        private boolean logSingleRuns;

        public AlgorithmRepetitionThread(Problem problem,
                Algorithm algorithm, int repNr, String dir,
                int maxEvaluations, boolean logSingleRuns) {

            this.problem = problem;
            this.algorithm = algorithm;
            this.repNr = repNr;
            this.dir = dir;
            this.maxEvaluations = maxEvaluations;
            this.logSingleRuns = logSingleRuns;
        }

        @Override
        public Double call() throws Exception {

            // create a copy for each repetition
            Algorithm a = this.algorithm.copy();

            double result = runAlgorithmRepetition(this.problem,
                    a, this.repNr, this.dir,
                    this.maxEvaluations, this.logSingleRuns);
            return new Double(result);
        }		
    };


    /**
     * Run the given algorithms, each repetitionNumber times and
     * calculate statistics of the fitnesses.
     * @param problem optimization problem to solve
     * @param algorithms algorithms to run
     * @param repetitionNumber number of repetitions per algorithm
     * @param maxEvaluations maximum number of function evaluations
     *        per repetition
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object containing the mean, standard
     *         deviation and standard error information of all the
     *         algorithms
     * @throws Exception if the calculation was not successfull
     */
    public static MeanResults runAlgorithms(final Problem problem,
            final Vector<Algorithm> algorithms,
            final int repetitionNumber, final int maxEvaluations,
            String dir, boolean logSingleRuns, boolean multithread)
    throws Exception {	

        int nrProc = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPool = Executors.newFixedThreadPool(nrProc);
        CompletionService<Double> pool = new ExecutorCompletionService<Double>(threadPool);

        //String parameters = new String();

        long t0 = System.currentTimeMillis();

        String folder;
        Algorithm a;
        MeanResults meanResults = new MeanResults();

        for (int i = 0; i < algorithms.size(); i++) {

            a = algorithms.get(i);

            xBestRun = null; // global
            fxBestRun = Double.NaN; // global

            folder = a.getPath(dir, problem);

            new File(folder).mkdirs();

            double[] fitnesses = new double[repetitionNumber];
            if (multithread) {

                for (int j = 0; j < repetitionNumber; j++) {
                    AlgorithmRepetitionThread thread = 
                        new AlgorithmRepetitionThread(problem,
                                algorithms.get(i), j, folder,
                                maxEvaluations, logSingleRuns);
                    pool.submit(thread);
                }

                for (int j = 0; j < repetitionNumber; j++)
                {
                    Future<Double> result = pool.take();
                    fitnesses[j] = result.get().doubleValue();
                }

            } else {

                for (int j = 0; j < repetitionNumber; j++) {

                    Algorithm algorithmCopy = algorithms.get(i).copy();

                    fitnesses[j] = runAlgorithmRepetition(problem,
                            algorithmCopy, j, folder,
                            maxEvaluations, logSingleRuns);
                }

            }

            double[] result = MathUtils.meanAndStdDevAndErr(fitnesses);
            double mean = result[0];
            double stddev = result[1];
            double stderr = result[2];
            MeanResult meanResult = new MeanResult(a, problem, mean,
                    stddev, stderr);
            meanResults.add(meanResult);
            System.out.printf("%s, %dD, %s, mean fitness %e (%e)\n\n",
                    problem.getName(), problem.getDimension(),
                    a.getName(), mean, stddev);

            meanResult.save(folder+"meanFinalFitness.txt", false);
            problem.saveConfig(xBestRun,
                    folder+"xBestRun.txt");

        }

        threadPool.shutdownNow();

        //System.out.println("Fitness of the best run: "+fxBestRun);
        /*
        long time = System.currentTimeMillis();
        System.out.println("Total elapsed time: " + (time-t0) + " ms");
        System.out.println();
         */

        return meanResults;

    }

    /**
     * Run a given algorithm with parameter sweeps. The algorithms-
     * vector should include a single algorithm with different
     * parameter settings.
     * @param algorithms a vector containing instances of a single
     *        algorithm with different parameter settings
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runParametersSweep(Vector<Algorithm>
    algorithms, Problem problem, int repetitionNumber,
    int maxEvaluations, String dir, boolean logSingleRuns,
    boolean multithread)
    throws Exception {

        Algorithm a = algorithms.get(0);

        String folder = dir + problem.getName() + "_"
        + problem.getDimension() + "D/" + a.getName() + "/";
        new File(folder).mkdirs();

        MeanResults meanResults = runAlgorithms(problem, algorithms,
                repetitionNumber, maxEvaluations, dir, logSingleRuns,
                multithread);
        meanResults.save(folder+"sweep.txt", true);

        return meanResults;
    }

    /**
     * Run the DE algorithm with parameter sweeps.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runDEparametersSweep(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        double CR = 0.9;

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;

        for (double F = 0.05; F < 0.96; F += 0.1) {
            for (int popsize = 4; popsize < 110; popsize += 10) {

                params = new Vector<Number>();
                params.add(new Integer(popsize));
                params.add(new Double(F));
                params.add(new Double(CR));
                Algorithm a = new DE(params);
                algorithms.add(a);
            }
        }

        return runParametersSweep(algorithms, problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
    }

    /**
     * Run the SA algorithm with parameter sweeps.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runSAparametersSweep(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;

        double r = 0.002;
        int lk = 100;

        for (double khi = 0.8; khi < 1.0; khi += 0.02) {
            for (double T0 = 0.1; T0 < 2.7; T0 += 0.2) {

                params = new Vector<Number>();
                params.add(new Double(khi));
                params.add(new Double(T0));
                params.add(new Double(r));
                params.add(new Integer(lk));

                algorithms.add(new SA(params));
            }
        }

        return runParametersSweep(algorithms, problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
    }

    /**
     * Run the SAcoleman algorithm with parameter sweeps.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runSAcolemanParametersSweep(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;

        double khi = 0.8;
        double theta0 = 0.001;

        for (double dummy = 1; dummy < 1.1; dummy += 1) {
            for (double T0 = 0.1; T0 < 2.7; T0 += 0.2) {

                params = new Vector<Number>();
                params.add(new Double(khi));
                params.add(new Double(T0));
                params.add(new Double(theta0));

                algorithms.add(new SAcoleman(params));
            }
        }

        return runParametersSweep(algorithms, problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
    }

    /**
     * Run the CCPSO2 algorithm with parameter sweeps.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runCCPSO2parametersSweep(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;

        for (int popsize = 1; popsize < 100; popsize += 10) {
            for (double p = 0.0; p < 1.1; p += 0.1) {

                params = new Vector<Number>();
                params.add(new Integer(popsize));
                params.add(new Double(p));

                algorithms.add(new CCPSO2(params));
            }
        }

        return runParametersSweep(algorithms, problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
    }

    /**
     * Run the JDE algorithm with parameter sweeps.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions of the algorithm
     * @param maxEvaluations maximum number of function evaluations
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having the statistics of the
     *         runs
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runJDEparametersSweep(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;

        double initF = 0.5;
        double initCR = 0.9;

        for (int popsize = 4; popsize < 200; popsize += 10) {

            params = new Vector<Number>();
            params.add(new Integer(popsize));
            params.add(new Double(initF));
            params.add(new Double(initCR));
            algorithms.add(new JDE(params));
        }

        return runParametersSweep(algorithms, problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
    }

    /**
     * Run algorithms with fixed parameters.
     * @param problem optimization problem to solve
     * @param repetitionNumber number of repetitions per algorithm
     * @param maxEvaluations maximum number of fitness function
     *        evaluations per algorithm
     * @param dir directory for the runs
     * @param logSingleRuns whether to log all the single runs or not
     * @param multithread whether to enable multithreading or not
     * @return a MeanResults object having statistics information
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runAlgorithmsFixedParams(Problem problem,
            int repetitionNumber, int maxEvaluations, String dir,
            boolean logSingleRuns, boolean multithread)
    throws Exception {

        Vector<Algorithm> algorithms = new Vector<Algorithm>();
        Vector<Number> params;
        GenerationLoggers genLoggers;
        IndividualLoggers indLoggers;
        String path;

        /*
        params = new Vector<Number>();
        params.add(new Integer(100));
        params.add(new Double(0.15));
        algorithms.add(new MDE_pBX_orig(params));
         */
        params = new Vector<Number>();
        params.add(new Integer(30)); // population size
        params.add(new Double(0.5)); // p
        CCPSO2 aCCPSO2 = new CCPSO2(params);
        path = aCCPSO2.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path, 1));
        genLoggers.add(aCCPSO2.new SaveGroupSize(path, 1));
        aCCPSO2.setLoggers(genLoggers);
        algorithms.add(aCCPSO2);
        /*
        params = new Vector<Number>();
        params.add(new Double(0.8)); // khi
        params.add(new Double(1.3)); // T0
        params.add(new Double(0.001)); // theta0
        SAcoleman aSA = new SAcoleman(params);
        path = aSA.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path, 1));
        //genLoggers.add(aSA.new SaveTemp(path, 1));
        //genLoggers.add(aSA.new SaveStepSize(path, 1));
        //genLoggers.add(aSA.new SaveAcceptanceRatio(path, 1));
        aSA.setLoggers(genLoggers);
        algorithms.add(aSA);
         */
        /*
        params = new Vector<Number>();
        params.add(new Double(0.88)); // khi
        params.add(new Double(0.9)); // T0
        params.add(new Double(0.002)); // r
        params.add(new Integer(100)); // lk
        SA aSA = new SA(params);
        path = aSA.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path,
	    5*problem.getDimension()));
        //genLoggers.add(aSA.new SaveTemp(path, 1));
        aSA.setLoggers(genLoggers);
        algorithms.add(aSA);
         */
        /*
        params = new Vector<Number>();
        params.add(new Integer(150));
        params.add(new Double(0.4));
        S aS = new S(params);
        path = aS.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path, 1));
        aS.setLoggers(genLoggers);
        algorithms.add(aS);
         */
        /*
        algorithms.add(new Dummy());
         */
        /*
        params = new Vector<Number>();
        params.add(new Integer(20)); // population size
        params.add(new Double(0.5)); // initial F's
        params.add(new Double(0.9)); // initial CR's
        JDE aJDE = new JDE(params);
        path = aJDE.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path, 1));
        aJDE.setLoggers(genLoggers);
        indLoggers = new IndividualLoggers();
        indLoggers.add(aJDE.new SaveFCR(path));
        aJDE.setLoggers(indLoggers);
        algorithms.add(aJDE);
         */
        /*
        params = new Vector<Number>();
        params.add(new Integer(35)); // population size
        params.add(new Double(0.4)); // F
        params.add(new Double(0.9)); // CR
        DE algorithm = new DE(params);
        path = algorithm.getPath(dir, problem);
        genLoggers = new GenerationLoggers();
        genLoggers.add(new SaveBestFitness(path, 1));
        algorithm.setLoggers(genLoggers);
        algorithms.add(algorithm);
         */
        /*
        params = new Vector<Number>();
        params.add(new Integer(100)); // population size
        params.add(new Double(0.5)); // F
        params.add(new Double(0.9)); // CR
        algorithms.add(new DE(params));
         */

        return runAlgorithms(problem, algorithms, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);

    }

    /**
     * Run algorithms on a single problem.
     * @param dir directory for the runs
     * @param problem the problem to use
     * @param repetitionNumber number of algorithm repetitions
     * @param maxEvaluations maximum number of function evaluations
     * @param logSingleRuns log single runs or not
     * @param multithread enable multithreading or not
     * @return a MeanResults object having statistics information
     * @throws Exception if the calculation was not successful
     */
    public static MeanResults runSingleProblem(final String dir,
            final Problem problem, final int repetitionNumber,
            final int maxEvaluations, final boolean logSingleRuns,
            final boolean multithread)
    throws Exception {

        MeanResults meanResults;

        meanResults = runAlgorithmsFixedParams(problem,
                repetitionNumber, maxEvaluations, dir,
                logSingleRuns, multithread);
        /*
        meanResults = runDEparametersSweep(problem, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);
         */
        /*
        meanResults = runSAparametersSweep(problem, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);
         */
        /*
        meanResults = runSAcolemanParametersSweep(problem, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);
         */
        /*
        meanResults = runCCPSO2parametersSweep(problem, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);
         */
        /*
        meanResults = runJDEparametersSweep(problem, repetitionNumber,
                maxEvaluations, dir, logSingleRuns, multithread);
         */

        return meanResults;

    }

    /**
     * @param args not used
     * @throws Exception if the calculation was not successful
     */
    public static void main(String[] args) throws Exception {

        //final String dir = "runs/SAvsEAs/sweep/";
        //final String dir = "runs/SAvsEAs/SA/";
        //final String dir = "runs/CEC2008_scalability/CCPSO2groupsize12/";
        //final String dir = "runs/LJclusterProblem_scalability/CCPSO2groupsize60/";
        final String dir = "runs/test/CCPSO2/modified/"; 
        //FIXME

        // number of repetitions per algorithm
        //FIXME
        final int repetitionNumber = 16;
        // should be false in the current version
        final boolean logSingleRuns = false;
        final boolean multithread = true;
        int maxEvaluations;

        //FIXME
        //int[] problemDimensions = {6, 30, 60, 120, 180, 300, 600};
        int[] problemDimensions = {30, 60};
        int problemDimension;
        Vector<Problem> problems = new Vector<Problem>();
        for (int j = 0; j < problemDimensions.length; j++) {

            problemDimension = problemDimensions[j];
            //FIXME
            //problems.add(new LJclusterProblem(problemDimension));
            /*
            problems.add(new SphereProblem(problemDimension,
                    "problems/files/cec2008/sphere_shift_func_data.txt"));
            problems.add(new SchwefelProblem(problemDimension,
                    "problems/files/cec2008/schwefel_shift_func_data.txt"));
            problems.add(new RosenbrockProblem(problemDimension,
                    "problems/files/cec2008/rosenbrock_shift_func_data.txt"));
             */
            problems.add(new RastriginProblem(problemDimension,
            "problems/files/cec2008/rastrigin_shift_func_data.txt"));
            problems.add(new GriewankProblem(problemDimension,
            "problems/files/cec2008/griewank_shift_func_data.txt"));
            problems.add(new AckleyProblem(problemDimension,
            "problems/files/cec2008/ackley_shift_func_data.txt"));
        }

        for (Problem problem : problems) {

            maxEvaluations = 5000 * problem.getDimension();

            MeanResults meanResults = runSingleProblem(dir, problem,
                    repetitionNumber, maxEvaluations, logSingleRuns,
                    multithread);

            meanResults.save(dir+problem.getName()+"ScalabilityAnalysis.txt", true);

            MeanResult meanResult;
            System.out.println("Overall results:");
            for (int i = 0; i < meanResults.size(); i++) {
                meanResult = meanResults.get(i);
                System.out.printf("%s, %dD, %s, %s: mean fitness %e (%e)\n",
                        problem.getName(), problem.getDimension(),
                        meanResult.getAlgorithm().getName(),
                        meanResult.getAlgorithm().getParametersAsPrintableString(),
                        meanResult.getMean(), meanResult.getStdDev());
            }
            System.out.println();
        }

    }

}
