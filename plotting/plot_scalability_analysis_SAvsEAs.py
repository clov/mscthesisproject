from plot_scalability_analysis import plot_single_algorithm
from plot_internal_variable import plot_best_fitnesses
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

known_minima_filename = '../problems/files/LJclusterProblem_known_minima.txt'
known_minima_data = np.loadtxt(known_minima_filename)
known_minima_Natoms = known_minima_data[:, 0]
known_minima_dimensions = list(known_minima_Natoms * 3)
known_minima_energies = known_minima_data[:, 1]

subtract_known_minima = True 
plot_known_minima = False 
log = False
figsize = (3.5, 3.0)

font = {'family' : 'serif',
        'serif' : 'Palatino',
        'size' : 12}
matplotlib.rc('font', **font)
matplotlib.rc('text', usetex=True)

algorithms = ('SA', 'SAcoleman', 'DE', 'JDE', 'CCPSO2')
datafilenames =\
    ('../runs/SAvsEAs/SA/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/SAvsEAs/SAcoleman/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/SAvsEAs/DE/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/SAvsEAs/JDE/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/SAvsEAs/CCPSO2/LJclusterProblemScalabilityAnalysis.txt')
colors = ('k', 'b', 'r', 'g', 'm')
dotstyles = ['.'] * len(colors)
linestyles = ('-', '--', '-.', ':', '-')
texts = ('SA1', 'SA2', 'DE', 'jDE', 'CCPSO2')
x = 400.
points = ((x, 0.99), (x, 0.87), (x, 0.71), (x, 0.35), (x, 0.07))

plt.figure(figsize=figsize)
for i in range(len(algorithms)):

    algorithm = algorithms[i]
    dotstyle = dotstyles[i]
    linestyle = linestyles[i]
    datafilename = datafilenames[i]
    color = colors[i]
    text = texts[i]
    point = points[i]
    plot_single_algorithm(datafilename, algorithm, algorithm,
            color+dotstyle, color+linestyle, True, False,
            subtract_known_minima, known_minima_dimensions,
            known_minima_energies, True)

    plt.text(point[0], point[1], text, color=color)


if plot_known_minima:
    plt.plot(known_minima_dimensions, known_minima_energies,
            'r-', label='Known minima')

plt.xlim((0, 610))
plt.ylim((0, 1.1))
#plt.xlabel('Number of atoms')
plt.xlabel('Problem dimension')
if subtract_known_minima:
    ylabel = 'Average relative error'
    #ylabel = 'Average error in cohesion energy'
    #ylabel = 'Average error'
else:
    ylabel = 'Average best fitness'
plt.ylabel(ylabel)
#plt.legend(loc='upper left')
#plt.title('Lennard-Jones cluster problem')
if log:
    plt.gca().set_yscale('log')
figname = '../runs/SAvsEAs/LJclusterProblemScalabilityAnalysis.pdf'
plt.tight_layout()
plt.savefig(figname)
plt.show()
plt.clf()


# best fitness evolution plots

dim = 600
dim_idx = known_minima_dimensions.index(dim)
known_minimum_energy = known_minima_energies[dim_idx]
datafolders =\
    ('../runs/SAvsEAs/SA/LJclusterProblem_{0}D/SA/khi0.8800_T00.9000_r0.0020_lk100/',
    '../runs/SAvsEAs/SAcoleman/LJclusterProblem_{0}D/SAcoleman/khi0.800_T01.300_theta00.001/',
    '../runs/SAvsEAs/DE/LJclusterProblem_{0}D/DE/NP35_F0.4_CR0.9/',
    '../runs/SAvsEAs/JDE/LJclusterProblem_{0}D/JDE/NP20/',
     '../runs/SAvsEAs/CCPSO2/LJclusterProblem_{0}D/CCPSO2/NP30_p0.5/')
colors = ('k', 'b', 'r', 'g', 'm')
styles = ['-'] * len(colors)
x = 2.0E6
points = ((1.3E6, -130), (x, -130), (x, -260), (x, -440),
          (x, -850))
#order = (2, 3, 4, 0, 1)
order = (0, 1, 2, 3, 4)
figsize = (3.8, 3.1)

plt.figure(figsize=figsize)
for i in order[:]:
    algorithm = algorithms[i]
    datafolder = datafolders[i].format(dim)
    color = colors[i]
    style = styles[i]
    text = texts[i]
    point = points[i]

    plot_best_fitnesses(datafolder, sameFig=True,
            style=color+style, show=False, log=False, clf=False,
            save=False)

    plt.text(point[0], point[1], text, color=color)

    
plt.xlabel('Function evaluations')
plt.ylabel('Energy of the best individual')
plt.ylim((known_minimum_energy, 0))
#plt.title('Lennard-Jones cluster problem, 200 atoms')
plt.tight_layout()
plt.savefig('../runs/SAvsEAs/bestfitnesses.pdf')
plt.show()





