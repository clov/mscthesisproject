from plot_scalability_analysis import plot_single_algorithm
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

known_minima_filename = '../problems/files/LJclusterProblem_known_minima.txt'
known_minima_data = np.loadtxt(known_minima_filename)
known_minima_Natoms = known_minima_data[:, 0]
known_minima_dimensions = list(known_minima_Natoms * 3)
known_minima_energies = known_minima_data[:, 1]

subtract_known_minima = True 
plot_known_minima = False 
log = False
figsize = (5., 4.)
ext = '.pdf'

font = {'family' : 'serif',
        'serif' : 'Palatino',
        'size' : 12}
matplotlib.rc('font', **font)
matplotlib.rc('text', usetex=True)


# CCPSO2 constant group size -versions
algorithms0 = ('CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2',
              'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2',
              'CCPSO2', 'CCPSO2', 'CCPSO2')
labels0 = ('CCPSO2 biggest sets',
           'CCPSO2 group size 1', 'CCPSO2 group size 2',
           'CCPSO2 group size 3', 'CCPSO2 group size 4',
           'CCPSO2 group size 5', 'CCPSO2 group size 6',
           'CCPSO2 group size 10', 'CCPSO2 group size 12',
           'CCPSO2 group size 15', 'CCPSO2 group size 20',
           'CCPSO2 group size 30', 'CCPSO2 group size 60')
texts0 = ('adaptive', '1', '2', '3', '4', '5', '6', '10', '12', '15',
          '20', '30', '60')
"""
x = 610
textpoints0 = ((x, 250), (600, 1250), (x, 1200), (x, 550), (x, 180),
        (500, 80), (550, 100), (600, 120), (x, 230), (x, 650),
        (x, 900), (x, 1000), (x, 1120))
"""
x = 615
textpoints0 = ((480., 0.24), (590., 1.02), (x, 0.97), (x, 0.45),
        (x, 0.15), (500., 0.07), (540., 0.075), (590., 0.08),
        (525., 0.173), (x, 0.52), (x, 0.72), (x, 0.82), (x, 0.9))
datafilenames0 =\
    ('../runs/LJclusterProblem_scalability/CCPSO2biggestSets/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2alongAxes/scalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize2/scalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize3/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize4/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize5/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize6/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize10/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize12/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize15/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize20/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize30/LJclusterProblemScalabilityAnalysis.txt',
     '../runs/LJclusterProblem_scalability/CCPSO2groupsize60/LJclusterProblemScalabilityAnalysis.txt')
figname0 =\
'../runs/LJclusterProblem_scalability/LJclusterProblemScalabilityAnalysis_CCPSO2constantGroupSizes'+ext

# more comparison
algorithms1 = ('CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2',
               'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2')
labels1 = ('CCPSO2', 'CCPSO2 smaller sets', 'CCPSO2 smaller sets 2',
           'CCPSO2 smaller sets 3', 'CCPSO2 smaller sets 4',
           'CCPSO2 smaller sets 5', 'CCPSO2 biggest sets',
           'CCPSO2 sets with 1', 'CCPSO2 no permutations',
           'CCPSO2 modified CGPSO')
texts1 = None
textpoints1 = None
datafilenames1 =\
    ('../runs/LJclusterProblem_scalability/CCPSO2vsJDE/scalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2smallerSets/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2smallerSets2/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2smallerSets3/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2smallerSets4/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2smallerSets5/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2biggestSets/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2setsWith1/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2noPermutations/LJclusterProblemScalabilityAnalysis.txt',
    '../runs/LJclusterProblem_scalability/CCPSO2modifiedCGPSO/LJclusterProblemScalabilityAnalysis.txt')
figname1 =\
'../runs/LJclusterProblem_scalability/LJclusterProblemScalabilityAnalysis_moreComparison'+ext

colors = ('k', 'b', 'm', 'r', 'c', 'y', 'g',
                 'k', 'b', 'm', 'r', 'c', 'y')
errorbar_fmts = ['.'] * len(colors)
plot_fmts = ('-', '--', '-.', ':', '-', '--', '-.',
        ':', '-', '--', '-.', ':', '-')

algorithms_all = (algorithms0, algorithms1)
labels_all = (labels0, labels1)
texts_all = (texts0, texts1)
textpoints_all = (textpoints0, textpoints1)
datafilenames_all = (datafilenames0, datafilenames1)
fignames_all = (figname0, figname1)

for i in range(len(algorithms_all)):

    plt.figure(figsize=figsize)

    algorithms = algorithms_all[i]
    labels = labels_all[i]
    texts = texts_all[i]
    textpoints = textpoints_all[i]
    datafilenames = datafilenames_all[i]
    figname = fignames_all[i]

    for algorithm_idx in range(len(algorithms)):
        algorithm = algorithms[algorithm_idx]
        label = labels[algorithm_idx]
        color = colors[algorithm_idx]
        if texts is not None and textpoints is not None:
            try:
                text = texts[algorithm_idx]
                np
                textpoint = textpoints[algorithm_idx]
                plt.text(textpoint[0], textpoint[1], text,
                        color=color)
            except IndexError:
                pass

        datafilename = datafilenames[algorithm_idx]
        errorbar_fmt = errorbar_fmts[algorithm_idx]
        plot_fmt = plot_fmts[algorithm_idx]

        plot_single_algorithm(datafilename, algorithm, label,
                color+errorbar_fmt, color+plot_fmt, True, False, 
                subtract_known_minima,
                known_minima_dimensions, known_minima_energies,
                True)

        
    if plot_known_minima:
        plt.plot(known_minima_dimensions, known_minima_energies,
                'r-', label='Known minima')

    plt.xlim((0, 650))
    #ylow, yhigh = plt.gca().get_ylim()
    #plt.ylim((0, 1400))
    plt.ylim((0., 1.1))
    plt.xlabel('Problem dimension')
    if subtract_known_minima:
        ylabel = 'Average relative error'
    else:
        ylabel = 'Average best fitness'
    plt.ylabel(ylabel)
    #plt.legend(loc='upper left')
    #plt.title('LJclusterProblem')
    if log:
        plt.gca().set_yscale('log')
    plt.tight_layout()
    plt.savefig(figname)
    plt.show()
    plt.clf()





