import numpy as np
import matplotlib.pyplot as plt

def plot_single_algorithm(datafilename, algorithm, label,
        errorbar_fmt, plot_fmt, plot_errorbar=True,
        divide_by_dims=False, subtract_known_minima=False,
        known_minima_dimensions=None, known_minima_energies=None,
        plot_relative_error=False):

    dims = []
    means = []
    errors = []
    stddevs = []
    stderrs = []

    with open(datafilename, 'r') as f:
        for line in f:

            parts = line.split(' ')
            alg = parts[2]
            if alg == algorithm:

                dim = int(parts[1])
                dims.append(dim)
                N = dim / 3

                if subtract_known_minima:
                    dim_idx = known_minima_dimensions.index(dim)
                    known_minimum_energy =\
                        known_minima_energies[dim_idx]

                mean = float(parts[-3])
                if divide_by_dims:
                    mean /= dim
                means.append(mean)

                stderr = float(parts[-1])
                
                if subtract_known_minima:
                    if divide_by_dims:
                        err = mean - known_minimum_energy / dim
                        stderr /= dim
                    elif plot_relative_error:
                        err = np.abs((mean - known_minimum_energy)\
                                / known_minimum_energy)
                        stderr /= np.abs(known_minimum_energy)
                    else:
                        err = mean - known_minimum_energy

                    errors.append(err)

                #stddevs.append(float(parts[-2]))
                stderrs.append(stderr)

    if subtract_known_minima:
        ys = errors
    else:
        ys = means

    dims = np.array(dims)
    ys = np.array(ys)
    
    if plot_errorbar:
        plt.errorbar(dims, ys, yerr=stderrs, fmt=errorbar_fmt)
    plt.plot(dims, ys, plot_fmt, label=label)

