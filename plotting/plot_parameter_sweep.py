import numpy as np
import matplotlib.pyplot as plt
import os

#formats = ['k|', 'kx', 'k*', 'kp', 'ks', 'k.', 'ko', 'kv', 'k>', 'k^'][::-1]
formats = ['b.', 'g.', 'r.', 'c.', 'm.', 'y.', 'k.',
           'b.', 'g.', 'r.', 'c.', 'm.', 'y.']
lineformats = ['b-', 'g--', 'r-.', 'c:', 'm-', 'y--', 'k-.',
        'b:', 'g-', 'r--', 'c-.', 'm:', 'y-']
    
def plot_parameter_sweep(filename, var1idx, var1label, var2idx,
        var2label, n=None, **kwargs):

    data = np.loadtxt(filename, dtype=str)
    hasVar1 = var1idx is not None and var1label is not None

    if hasVar1:
        var1s = data[:, var1idx].astype(float)
        nVar1s = len(var1s)
    else:
        nVar1s = 1
    var2s = data[:, var2idx].astype(float)
    means = data[:, -3].astype(float)
    stderrs = data[:, -1].astype(float)

    if n is None:
        n = len(var2s)

    for j in range(0, nVar1s, n):
        if hasVar1:
            label = '{0} = {1}'.format(var1label, var1s[j])
        var2s_single = var2s[j:j+n]
        means_single = means[j:j+n]
        stderrs_single = stderrs[j:j+n]
        format = formats.pop()
        lineformat = lineformats.pop()
        plt.errorbar(var2s_single, means_single, stderrs_single,
                     fmt=format)
        if hasVar1:
            plt.plot(var2s_single, means_single, lineformat, label=label)
        else:
            plt.plot(var2s_single, means_single, lineformat)

    for name, value in kwargs.items():
        if name == 'xlim':
            plt.xlim(value)
        if name == 'ylim':
            plt.ylim(value)

    if hasVar1:
        plt.legend(loc='upper right')
    plt.xlabel(var2label) 
    plt.ylabel('Average final best energy')
    filename_woext, _ = os.path.splitext(filename)
    plt.savefig(filename_woext+'.png')
    plt.show()


def plot_DE_parameter_sweep(filename, n, **kwargs):
    plot_parameter_sweep(filename, 6, 'F', 4, 'NP', n, **kwargs)

def plot_JDE_parameter_sweep(filename, **kwargs):
    plot_parameter_sweep(filename, None, None, 4, 'NP', **kwargs)

def plot_SA_parameter_sweep(filename, n, **kwargs):
    plot_parameter_sweep(filename, 4, 'khi', 6, 'T0', n, **kwargs)
    
def plot_SAcoleman_parameter_sweep(filename, **kwargs):
    plot_parameter_sweep(filename, None, None, 6, 'T0', **kwargs)

def plot_CCPSO2_parameter_sweep(filename, n, **kwargs):
    plot_parameter_sweep(filename, 4, 'NP', 6, 'p', n, **kwargs)

if __name__ == '__main__':
    
    n = 12
    filename =\
    "../runs/SAvsEAs/sweep/LJclusterProblem_120D/CCPSO2/sweepBiggestSetsWo1.txt"
    xlim = (-0.1, 1.7)
    ylim = (-185.3, -160)
    plot_CCPSO2_parameter_sweep(filename, n, xlim=xlim, ylim=ylim)
