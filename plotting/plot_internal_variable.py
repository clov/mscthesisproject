import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os, sys

def get_files_in_folder(folder, name_start, name_ext):
    
    all_filenames = os.listdir(folder)
    filenames = []
    for filename in all_filenames:
        
        filename_head, ext = os.path.splitext(filename)

        name = filename_head.split('_')[0]
        if name == name_start and ext == name_ext:
            filenames.append(filename)
    
    return filenames


def plot_internal_variable(x, y, xlabel, ylabel, yscale=1.0,
        log=False, style='k-', **kwargs):

    y = yscale * np.array(y)
    plt.plot(x, y, style)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    if log:
        plt.gca().set_yscale('log')

    xlim = ((x[0], x[-1]))
    ylim = plt.gca().get_ylim()

    for name, value in kwargs.items():
        if name == 'xlim':
            xlim = value
        elif name == 'ylim':
            ylim = value
            
    plt.xlim(xlim)
    plt.ylim(ylim)
    
    
def plot_best_fitnesses(folder, sameFig, yscale=1.0, style='k-',
        ext='.pdf', xlabel='Evaluations', ylabel='Best fitness value',
        show=False, log=True, clf=True,
        save=True, **kwargs):
    
    for filename in get_files_in_folder(folder, 'bestfitness',
                                        '.txt'):

        filename_head, _ = os.path.splitext(filename)

        data = np.loadtxt(folder+filename)
        x = data[:, 0]
        y = data[:, 1]

        plot_internal_variable(x, y, xlabel, ylabel, yscale=yscale,
            log=log, style=style, **kwargs)
        plt.tight_layout()
        
        if not sameFig:
            if save:
                plt.savefig(folder+'figs/'+filename_head+ext)
            if show:
                plt.show()
            plt.clf()

    if sameFig:
        filename = filename.split('_')[0] + ext
        plt.ticklabel_format(style='sci', axis='x',
                scilimits=(0, 0))
        if save:
            plt.savefig(folder+'figs/'+filename)
        if show:
            plt.show()
        if clf:
            plt.clf()
    

def plot_group_sizes(folder, ext='.pdf', show=False, **kwargs):

    xlabel = 'Generation'
    ylabel = 'Group size'
    
    for filename in get_files_in_folder(folder, 'groupsize', '.txt'):

        filename_head, _ = os.path.splitext(filename)

        data = np.loadtxt(folder+filename)
        x = data[:, 0]
        y = data[:, 1]
        x = range(len(y))

        plot_internal_variable(x, y, xlabel, ylabel, style='k.',
                **kwargs)
        plt.tight_layout()
        plt.savefig(folder+'figs/'+filename_head+ext)
        if show:
            plt.show()
        plt.clf()


def plot_F(folder, filename, ext='.pdf', **kwargs):
    
    xlabel = 'Generation'
    ylabel = 'F'
    figname = 'F'
    
    data = np.loadtxt(folder+filename)
    x = data[:, 0]
    y = data[:, 1]
    
    filename_head, _ = os.path.splitext(filename)
    _, rep = filename_head.rsplit('_')

    plot_internal_variable(x, y, xlabel, ylabel, style='k+',
                           **kwargs)
    plt.savefig(folder+'figs/'+figname+'_'+rep+ext)
    plt.clf()


def plot_CR(folder, filename, ext='.pdf', **kwargs):
    
    xlabel = 'Generation'
    ylabel = 'CR'
    figname = 'CR'
    
    data = np.loadtxt(folder+filename)
    x = data[:, 0]
    y = data[:, 2]

    filename_head, _ = os.path.splitext(filename)
    _, rep = filename_head.rsplit('_')

    plot_internal_variable(x, y, xlabel, ylabel, style='k+',
                           **kwargs)
    plt.savefig(folder+'figs/'+figname+'_'+rep+ext)
    plt.clf();


def plot_F_CR(folder, ext='.pdf', **kwargs):
    
    for filename in get_files_in_folder(folder, 'FCR', '.txt'):
        plot_F(folder, filename, ext, **kwargs)
        plot_CR(folder, filename, ext, **kwargs)


def plot_temp(folder, ext='.pdf', show=False,
        **kwargs):

    xlabel = 'Generation'
    ylabel = 'Temperature'
    
    for filename in get_files_in_folder(folder, 'temp', '.txt'):

        filename_head, _ = os.path.splitext(filename)

        data = np.loadtxt(folder+filename)
        x = data[:, 0]
        y = data[:, 1]
        x = range(len(y))

        plot_internal_variable(x, y, xlabel, ylabel, style='k-',
                **kwargs)
        plt.savefig(folder+'figs/'+filename_head+ext)
        if show:
            plt.show()
        plt.clf()


def plot_stepsize(folder, ext='.pdf', show=False, **kwargs):

    xlabel = 'Generation'
    ylabel = 'Step size'
    
    for filename in get_files_in_folder(folder, 'stepsize', '.txt'):

        filename_head, _ = os.path.splitext(filename)

        data = np.loadtxt(folder+filename)
        x = data[:, 0]
        y = data[:, 1]
        x = range(len(y))

        plot_internal_variable(x, y, xlabel, ylabel, style='k-',
                **kwargs)
        plt.savefig(folder+'figs/'+filename_head+ext)
        if show:
            plt.show()
        plt.clf()


def plot_acceptanceratio(folder, ext='.pdf', show=False, **kwargs):

    xlabel = 'Generation'
    ylabel = 'Acceptance ratio'
    
    for filename in get_files_in_folder(folder, 'acceptanceratio',
            '.txt'):

        filename_head, _ = os.path.splitext(filename)

        data = np.loadtxt(folder+filename)
        x = data[:, 0]
        y = data[:, 1]
        x = range(len(y))

        plot_internal_variable(x, y, xlabel, ylabel, style='k-',
                **kwargs)
        plt.savefig(folder+'figs/'+filename_head+ext)
        if show:
            plt.show()
        plt.clf()


if __name__ == '__main__':

    font = {'family' : 'serif',
            'serif' : 'Palatino',
            'size' : 12}
    matplotlib.rc('font', **font)
    matplotlib.rc('text', usetex=True)
    

    #for problem in ('Sphere', 'Schwefel', 'Rosenbrock', 'Rastrigin',
    #        'Griewank', 'Ackley'):
    #for problem in ('LJcluster',):
    for problem in ('Rastrigin', 'Griewank', 'Ackley'):
        #for dim in (600, 300, 180, 120, 60, 30, 6):
        #for dim in (600,):
        for dim in (30, 60):
        
            #folder = '../runs/CEC2008_scalability/CCPSO2biggestSets/{0}Problem_{1}D/CCPSO2/NP30_p0.5/'.format(problem, dim)
            #folder = '../runs/LJclusterProblem_scalability/CCPSO2biggestSets/{0}Problem_{1}D/CCPSO2/NP30_p0.5/'.format(problem, dim)
            folder = '../runs/test/CCPSO2/modified/{0}Problem_{1}D/CCPSO2/NP30_p0.5/'.format(problem, dim)
            figfolder = folder + 'figs/'
            if not os.path.exists(figfolder):
                os.mkdir(figfolder)

            #plt.figure(figsize=(4.5, 3.5))
            plot_best_fitnesses(folder, sameFig=True,
                    ext='.png', show=False, log=True)
            """
                    ylim=(-1229.184776, 0),
                    xlabel='Function evaluations',
                    ylabel='Energy of the best individual')
            """

            #plt.figure(figsize=(3.5, 3))
            #plot_group_sizes(folder, ext='.png', show=False,
            #        ylim=(-10, dim+10))

            #plot_F_CR(folder, xlim=(0, 500), ylim=(0,1))
            #plot_temp(folder, ext='.png', log=True, show=False)
            #plot_stepsize(folder, ext='.png', log=True, show=False)
            #plot_acceptanceratio(folder, ext='.png', show=False);
