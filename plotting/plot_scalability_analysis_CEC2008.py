from plot_scalability_analysis import plot_single_algorithm
import numpy as np
import matplotlib.pyplot as plt

subtract_known_minima = True 
plot_known_minima = False 
log = True
figsize = (9, 7)

problems = ('Sphere', 'Schwefel', 'Rosenbrock',
            'Rastrigin', 'Griewank', 'Ackley')
if log:
    locs = ('upper left', 'lower right', 'upper left',
            'lower left', 'upper left', 'upper left')
    ylims = None
else:
    locs = ['upper left'] * 6
    ylims = ((0., 1E-20), (-5, 180), (0., 1E3),
             (-10, 700), (0., 1E-1), (0., 1E-8))

# CCPSO2 constant group size -versions
algorithms0 = ('CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2',
               'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2', 'CCPSO2',
                'CCPSO2', 'CCPSO2', 'CCPSO2')
labels0 = ('adaptive', '1', '2', '3', '4', '5', '6', '10', '12',
          '15', '20', '30', '60')
datafilenames0 =\
    ('../runs/CEC2008_scalability/CCPSO2biggestSets/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize1/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize2/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize3/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize4/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize5/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize6/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize10/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize12/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize15/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize20/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize30/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2groupsize60/{0}ProblemScalabilityAnalysis.txt')
figname0 =\
'../runs/CEC2008_scalability/{0}ProblemScalabilityAnalysis_CCPSO2constantGroupSizes{1}.png'

# more comparison
algorithms1 = ('CCPSO2', 'CCPSO2')
labels1 = ('CCPSO2', 'CCPSO2 biggest sets')
datafilenames1 =\
    ('../runs/CEC2008_scalability/CCPSO2vsJDEvsS/{0}ProblemScalabilityAnalysis.txt',
     '../runs/CEC2008_scalability/CCPSO2biggestSets/{0}ProblemScalabilityAnalysis.txt')
figname1 =\
'../runs/CEC2008_scalability/{0}ProblemScalabilityAnalysis_moreComparison{1}.png'


errorbar_fmts = ('k.', 'b.', 'm.', 'r.', 'c.', 'y.', 'g.',
                 'k.', 'b.', 'm.', 'r.', 'c.', 'y.', 'g.',
                 'k.', 'b.')
plot_fmts = ('k-', 'b--', 'm-.', 'r:', 'c-', 'y--', 'g-.',
        'k:', 'b-', 'm--', 'r-.', 'c:', 'y-', 'g--',
        'k-.', 'b:')

algorithms_all = (algorithms0, algorithms1)
labels_all = (labels0, labels1)
datafilenames_all = (datafilenames0, datafilenames1)
fignames_all = (figname0, figname1)

for problem_idx in range(len(problems)):
    problem = problems[problem_idx]
    
    for i in range(len(algorithms_all)):

        plt.figure(figsize=figsize)

        algorithms = algorithms_all[i]
        labels = labels_all[i]
        datafilenames = datafilenames_all[i]
        figname = fignames_all[i]

        for algorithm_idx in range(len(algorithms)):

            algorithm = algorithms[algorithm_idx]
            label = labels[algorithm_idx]
            datafilename = datafilenames[algorithm_idx].format(problem)
            errorbar_fmt = errorbar_fmts[algorithm_idx]
            plot_fmt = plot_fmts[algorithm_idx]

            plot_errorbar = True
            if log:
                plot_errorbar = False
            plot_single_algorithm(datafilename, algorithm, label,
                    errorbar_fmt, plot_fmt, plot_errorbar)
            
        plt.xlim((0, 610))
        plt.xlabel('Problem dimension')
        if ylims is not None:
            plt.ylim(ylims[problem_idx])
        if subtract_known_minima:
            ylabel = 'Average error'
        else:
            ylabel = 'Average best fitness'
        plt.ylabel(ylabel)

        if log:
            plt.gca().set_yscale('log')
            figname = figname.format(problem, '_log')
        else:
            figname = figname.format(problem, '')

        box = plt.gca().get_position()
        plt.gca().set_position([box.x0, box.y0, box.width*0.8,
            box.height])
        plt.legend(loc='upper left', bbox_to_anchor=(1.05, 1.),
                borderaxespad=0.)
        #plt.legend(loc=locs[problem_idx])

        plt.title(problem)
        plt.savefig(figname)
        plt.clf()





