package algorithms;

import static algorithms.utils.AlgorithmUtils.crossOverBin;
import static algorithms.utils.AlgorithmUtils.generateRandomSolution;
import static algorithms.utils.AlgorithmUtils.saturateToro;
import static utils.MathUtils.fix;
import static utils.MathUtils.indexMin;

import java.io.FileNotFoundException;
import java.util.Vector;

import problems.interfaces.Problem;

import utils.random.RandUtils;
import algorithms.interfaces.Algorithm;
import algorithms.utils.Best;

/**
 * MDE_pBX original version with a bug
 * @author modified by Teemu Peltonen
 * @version Dec 5, 2014
 */
public class MDE_pBX_orig extends Algorithm
{
    
	private double fBest;
	
    /**
     * @param params parameters for the algorithm
     */
    public MDE_pBX_orig(Vector<Number> params) {
        super(params);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public MDE_pBX_orig(Algorithm algorithm) {
        super(algorithm);
    }

	@Override
	public Vector<Best> execute(Problem problem, int maxEvaluations,
	        String folder, int repetitionNr)
	                throws FileNotFoundException
	{
	    repNr = repetitionNr;
	    
	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
		
		double CRm = 0.6;
		double Fm = 0.5;
		double N = 1.5;
		
		int groupSize = (int)(populationSize*q);
		
		Vector<Best> bests = new Vector<Best>();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();
		
		int G = 1;
		int Gmax = maxEvaluations%populationSize;
		if(Gmax == 0)
			Gmax = maxEvaluations/populationSize;
		else
			Gmax = maxEvaluations/populationSize + 1;
		
		double[][] population = new double[populationSize][problemDimension];
		double[] fitnesses = new double[populationSize];
		
		double[] best = new double[problemDimension];
		fBest = Double.NaN;
		
		int i = 0;
		
		// evaluate initial population
		for (int j = 0; j < populationSize; j++)
		{
			double[] tmp;
			if (j == 0 && initialSolution != null)
				tmp = initialSolution;
			else
				tmp = generateRandomSolution(bounds, problemDimension);
			
			for (int n = 0; n < problemDimension; n++)
				population[j][n] = tmp[n];
			fitnesses[j] = problem.f(population[j]);
			
			if (j == 0 || fitnesses[j] < fBest)
			{
				fBest = fitnesses[j];
				for (int n = 0; n < problemDimension; n++)
					best[n] = population[j][n];
					//bests.add(new Best(i, fBest));
			}
			if(i%problemDimension==0)
				bests.add(new Best(i, fBest));
			i++;
		}

		// temp variables
		double[] currPt = new double[problemDimension];
		double[] newPt = new double[problemDimension];
		double[] crossPt = new double[problemDimension];
		double[] temp;
		double currFit = Double.NaN;
		double crossFit = Double.NaN;
		
		// iterate
		while (i < maxEvaluations)
		{
			double FmMeanPow = 0;
			double CrMeanPow = 0;
			int counter = 0;
			for (int j = 0; j < populationSize && i < maxEvaluations; j++)
			{
				if(i%problemDimension==0)
					bests.add(new Best(i, fBest));
				for (int n = 0; n < problemDimension; n++)
					currPt[n] = population[j][n];
				currFit = fitnesses[j];
				
				//MUTATION
				temp = new double[populationSize];
				for(int n=0; n<populationSize; n++)
					temp[n] = n; //vector of indexes
				int c = 1; 
				int r = RandUtils.randomInt(populationSize-c); 
				int I = (int)temp[r]; int indMin = I;
				temp = removeElementAtIndex(r,temp); c++;
				double[] XgrBest = population[I];
				double grMin = fitnesses[I];  
				for(int n=0; n<groupSize-1; n++) 
				{
					r = RandUtils.randomInt(populationSize-c);
					I = (int)temp[r];
					temp = removeElementAtIndex(r,temp); c++;
					if(fitnesses[I] < grMin)
					{
						grMin = fitnesses[I];
						XgrBest = population[I]; 
						indMin = I;
					}
					
				}
				
				/*int r1 = randInteger(populationSize-1);
				int r2 = randInteger(populationSize-1);
				boolean exit = (r1 != j) & (r1 != indMin) & (r1 != r2) & (r2 != j) & (r2 != indMin);
				while(!exit)
				{
					r1 = randInteger(populationSize-1);
					r2 = randInteger(populationSize-1);
					exit = (r1 != j) & (r1 != I) & (r1 != r2) & (r2 != j) & (r2 != indMin);
				}*/
				temp = new double[populationSize];
				for(int n=0; n<populationSize; n++)
					temp[n] = n; //vector of indexes
				temp = removeElementAtIndex(j,temp);
				temp = removeElementAtIndex(indMin,temp);
				r = RandUtils.randomInt(populationSize-3);
				double[] Xr1 = population[(int)temp[r]];
				temp = removeElementAtIndex(r,temp);				
				r = RandUtils.randomInt(populationSize-4);
				double[] Xr2 = population[(int)temp[r]];
				double F = generateF(Fm);
				
				newPt = currentToGrBest( XgrBest, currPt, Xr1, Xr2,  F);
						
				// CROSSOVER
				int P = (int)Math.ceil( (populationSize/2)*(1 - G/Gmax) );
				int Pindex = RandUtils.randomInt(P-1);
				temp = new double[populationSize];
				for(int n=0; n<populationSize;n++)
					temp[n] = fitnesses[n];                 
				double[] pBest = null;
				for(int n=0; n<=Pindex;n++)
				{
					int min = indexMin(temp);
					pBest = population[min];
					temp = removeElementAtIndex(min, temp);
				}	
				double CR =  generateCR(CRm);
				//FIXME is the order correct?
				//crossPt = crossOverBin(pBest, newPt, CR);			
				crossPt = crossOverBin(newPt, pBest, CR);			
				
				//saturation
				crossPt = saturateToro(crossPt, bounds);
				
				crossFit = problem.f(crossPt);
				i++;

				// best update
				if (crossFit < fBest)
				{
					fBest = crossFit;
					for (int n = 0; n < problemDimension; n++)
						best[n] = crossPt[n];
					//bests.add(new Best(i, fBest));
				}
				

				// replacement
				if (crossFit < currFit)
				{
					for (int n = 0; n < problemDimension; n++)
						population[j][n] = crossPt[n];
					fitnesses[j] = crossFit;
					//Mean_pow evaluation (Fsuccesfful and CRsuccessful sets)
					counter++;
					//FIXME bug here: wrong definition of the power mean
					FmMeanPow += Math.pow( Math.pow(F, N)/counter , 1/N);
					CrMeanPow += Math.pow( Math.pow(CR, N)/counter , 1/N);
					
				}
				else // di troppo?
				{
					for (int n = 0; n < problemDimension; n++)
						population[j][n] = currPt[n];
					fitnesses[j] = currFit;
				}
			}
			G++;
			//Fm update
			double Wf = 0.9 + 0.2*RandUtils.random();
			Fm = Wf*Fm +(1 - Wf)*FmMeanPow;
			//Cr update
			double Wc = 0.9 + 0.1*RandUtils.random();
			CRm = Wc*CRm +(1 - Wc)*CrMeanPow;
			
			this.callGenerationLoggers();
		}

		finalBest = best;
		
		bests.add(new Best(i, fBest));
		
		return bests;
	}	
	
	private double[] currentToGrBest(double[] X_gr_best, double[] X_target, double[] X_r1, double[] X_r2, double F)
	{	
		int dim = X_target.length;
		double[] v = new double[dim];
		for(int i=0; i<dim; i++)
			v[i] = X_target[i] + F*(X_gr_best[i] - X_target[i] + X_r1[i] - X_r2[i]);
		
		return v;
	}
	
	private double[] removeElementAtIndex(int ind, double[] array)
	{
		double[] newArray = new double[array.length - 1];
		for(int n=0; n<newArray.length; n++)
		{
		if(n<ind)
			newArray[n] = array[n];
		else
			newArray[n] = array[n+1];
		}
		return newArray;
	}
	
	private double generateCR(double mean)
	{	
		double cr = Double.NaN;
//		boolean generate = true;
//		while(generate)
//		{
//			cr = gaussian(mean , 0.1);
//			generate = cr<0 || cr>1;
//		}	
		cr = RandUtils.gaussian(mean,0.1);
		if (cr > 1)
			cr = cr - fix(cr);
		else if (cr < 0)
			cr=1-Math.abs(cr - fix(cr));
		return cr;
	}
	
	private double generateF(double Fm)
	{
		double F = Double.NaN;
//		boolean generate = true;
//		while(generate)
//		{
//			F = cauchy(Fm , 0.1);
//			generate = F<=0 || F>1;
//		}
		F = RandUtils.cauchy(Fm , 0.1);
		double Fsat = (F - Double.MIN_VALUE)/(1 -Double.MIN_VALUE);
		if (Fsat > 1)
			F = Fsat - fix(Fsat);
		else if (Fsat < 0)
			F=1-Math.abs(Fsat-fix(Fsat));
		return F;
	}

    @Override
    public String getParametersAsFilenameString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP"+populationSize+"_q"+q;
    }

    @Override
    public String getParametersAsSaveableString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP "+populationSize+" q "+q;
    }

    @Override
    public String getParametersAsPrintableString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP "+populationSize+", q "+q;
    }

    @Override
    public Algorithm copy() {
        return new MDE_pBX_orig(this);
    }

	@Override
	public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		
		return vars;
	}

}