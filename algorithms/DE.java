package algorithms;

import java.util.Locale;
import java.util.Vector;

import problems.interfaces.Problem;

import algorithms.interfaces.Algorithm;
import algorithms.interfaces.Population;
import algorithms.utils.AlgorithmUtils;
import algorithms.utils.Best;

/**
 * Differential evolution [1].
 * 
 * [1] Storn, R. & Price, K., Differential Evolution --- A Simple
 * and Efficient Heuristic for global Optimization over Continuous
 * Spaces, Journal of Global Optimization, Kluwer Academic
 * Publishers, 1997, 11, 341-359
 * 
 * @author Teemu Peltonen
 * @version Dec 3, 2014
 */
public class DE extends Algorithm {
    
	Population population;
	
    /**
     * @param parameters parameters for the algorithm such that
     *        parameters[0] is the population size
     */
    public DE(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public DE(Algorithm algorithm) {
        super(algorithm);
    }
    
    @Override
    public Vector<Best> execute(Problem problem, int maxEvaluations,
            String folder, int repetitionNr) throws Exception {
        
        this.repNr = repetitionNr;
        
        final int populationSize = this.getParameter(0).intValue();
        final double F = this.getParameter(1).doubleValue();
        final double CR = this.getParameter(2).doubleValue();
        
        final int problemDimension = problem.getDimension();
        final double[][] bounds = problem.getBounds();
        
        this.population = new Population(populationSize,
                                        problemDimension);

        this.evaluations = 0;
        
        Vector<Best> bests = new Vector<Best>();
        
        // generate the initial population
        if (problem.hasInitialSolution())
            population.generateSemiRandomPopulationAndCalcFitnesses(bounds,
                                                        problem);
        else
            population.generateRandomPopulationAndCalcFitnesses(bounds,
                                                        problem);
        this.evaluations += populationSize;
        
        bests.add(new Best(this.evaluations,
                population.getBestFitness()));
        
        // temp variables
        double[] x;
        double[] v;
        double[] u;
        
        // iterate until the stopping criterion is met
        while (this.evaluations < maxEvaluations) {
            
            // for each individual
            for (int i = 0; i < populationSize; i++) {
                
                // mutation
                v = AlgorithmUtils.getDErand1mutation(i, population, F);
                
                // saturation (seem's to be unimportant)
                //v = AlgorithmUtils.saturateAbsorbingWalls(v, bounds);
                //v = AlgorithmUtils.saturateToro(v, bounds);
                
                // crossover
                x = population.getIndividual(i);
                u = AlgorithmUtils.crossOverBin(x, v, CR);
               
                // selection and best update (to this end, the
                // fitness of x hasn't been changed)
                population.replaceIfBetterAndUpdateBest(i, u,
                                                      problem);
                // only the u fitness evaluation is done above
                this.evaluations++;
            }
            this.generation++;
            
            bests.add(new Best(this.evaluations,
                    population.getBestFitness()));
            this.callGenerationLoggers();
        }
        this.setFinalBest(population.getBestSolution());
        this.setFinalFbest(population.getBestFitness());
        return bests;
    }

    @Override
    public String getParametersAsFilenameString() {

        final int populationSize = this.getParameter(0).intValue();
        final double F = this.getParameter(1).doubleValue();
        final double CR = this.getParameter(2).doubleValue();
        
        return "NP"+populationSize+"_F"+F+"_CR"+CR;
    }

    @Override
    public String getParametersAsSaveableString() {

        final int populationSize = this.getParameter(0).intValue();
        final String F = String.format(Locale.ENGLISH, "%.2f", 
                this.getParameter(1).doubleValue());
        final String CR = String.format(Locale.ENGLISH, "%.2f",
                this.getParameter(2).doubleValue());
        
        return "NP "+populationSize+" F "+F+" CR "+CR;
    }

    @Override
    public String getParametersAsPrintableString() {

        final int populationSize = this.getParameter(0).intValue();
        final String F = String.format(Locale.ENGLISH, "%.2f", 
                this.getParameter(1).doubleValue());
        final String CR = String.format(Locale.ENGLISH, "%.2f",
                this.getParameter(2).doubleValue());
        return "NP "+populationSize+", F "+F+", CR "+CR;
    }

    @Override
    public Algorithm copy() {
        return new DE(this);
    }

	@Override
	public Vector<Number> getLoggableVariables() {
		
		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.population.getBestFitness()));
		
		return vars;
	}
    
    
}
