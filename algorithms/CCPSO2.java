package algorithms;

import static algorithms.utils.AlgorithmUtils.generateRandomSolution;
import static algorithms.utils.AlgorithmUtils.saturateToro;
import static algorithms.utils.AlgorithmUtils.saturateMirror;

import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Vector;

import problems.interfaces.Problem;

import utils.FileUtils;
import utils.random.RandUtils;
import algorithms.interfaces.Algorithm;
import algorithms.interfaces.GenerationLogger;
import algorithms.utils.AlgorithmUtils;
import algorithms.utils.Best;

/**
 * The CCPSO2 (Cooperatively Coevolving Particle Swarm Optimization)
 * algorithm by Li et al. [1].
 * 
 * [1] Li, X. et al. Cooperatively Coevolving Particle Swarms for
 * Large Scale Optimization. IEEE Transactions on Evolutionary
 * Computation, 2012, 16, 210-224.
 * @author Teemu Peltonen
 * @version May 21, 2015
 */
public class CCPSO2 extends Algorithm
{	
    private static final Locale locale = Locale.ENGLISH;
    private static final String format = "%.1f";
	
	private double fBest;
    private int s; // current group size
    
    /**
     * @param parameters parameters for the algorithm such that
     *        parameters[0] is the population size and
     *        parameters[1] is the parameter p (the probability
     *        for the use of Cauchy distribution in the position
     *        update)
     */
    public CCPSO2(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public CCPSO2(Algorithm algorithm) {
        super(algorithm);
    }
    
	@Override
	public Vector<Best> execute(Problem problem, int maxEvaluations,
	        String folder, int repetitionNr)
	                throws FileNotFoundException
	{	
	    repNr = repetitionNr;
	    
		int populationSize = this.getParameter(0).intValue();
		double p = this.getParameter(1).doubleValue();
		
		Vector<Best> bests = new Vector<Best>();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();
		
		int[] S;

        // find all the possible group sizes
        Vector<Integer> sVector = new Vector<Integer>();
        for (int i = 1; i <= problemDimension; i++)
            if (problemDimension % i == 0)
                sVector.add(i);
    
        S = new int[sVector.size()];
        for (int i = 0; i < sVector.size(); i++)
                S[i] = sVector.get(i);

        /*
		switch (problemDimension) {

		    case 4: S = new int[]{2, 4};
		        break;
		    case 6: S = new int[]{2, 3, 6};
		        break;
		    case 9: S = new int[]{3, 9};
		        break;
		    case 12: S = new int[]{2, 4, 6, 12};
		        break;
		    case 15: S = new int[]{3, 5, 15};
		        break;
		    case 28: S = new int[]{2, 3, 6, 8, 12};
		        break;
		    case 30: S = new int[]{2, 5, 10, 15, 30};
		        break;
		    case 45: S = new int[]{3, 5, 9, 15, 45};
		        break;
		    case 60: S = new int[]{2, 3, 6, 10, 30, 60};
		        break;
		    case 75: S = new int[]{3, 5, 15, 25, 75};
		        break;
		    case 90: S = new int[]{3, 9, 10, 30, 45, 90};
		        break;
		    case 100: S = new int[]{2, 5, 10, 50, 100};
		        break;
		    case 120: S = new int[]{2, 4, 10, 40, 60, 120};
		        break;
		    case 180: S = new int[]{2, 4, 10, 30, 90, 180};
		        break;
		    case 250: S = new int[]{2, 5, 10, 50, 100, 250};
		        break;
		    case 300: S = new int[]{2, 5, 10, 50, 100, 150, 300};
		        break;
		    case 500: S = new int[]{2, 5, 10, 50, 100, 250, 500};
		        break;
		    case 600: S = new int[]{2, 5, 10, 50, 100, 150, 300, 600};
		        break;
		    default: 
    		    String msg = "Problem dimension " + problemDimension +
    		        " not allowed for CCPSO2.";
    		    throw new IllegalArgumentException(msg);
		}
		*/

		// initialize the current permutation to the trivial one
		int[] perm = new int[problemDimension];
		for(int k=0; k<problemDimension; k++)
			perm[k] = k;

		s = S[RandUtils.randomInt(S.length)];
		int K = problemDimension / s;

		// current population
		double[][] X = new double[populationSize][problemDimension];
		// personal bests of the current population
		double[][] Y = new double[populationSize][problemDimension];

		// sample the initial population
		for (int i = 0; i < populationSize; i++)
		{
			double[] tmp = generateRandomSolution(bounds, problemDimension);
			for (int j = 0; j < problemDimension; j++)
			{
				X[i][j] = tmp[j];
				Y[i][j] = tmp[j];
			}
		}

		double currentFit;
		// yHat == the global best vector found so far
		// A MODIFICATION: In the original version, yHat was
		// not initialized at all, so the compiler initialized
		// it to [0, 0, ..., 0] (this was perhaps a bug). Then
		// during the first generation, instead of swarm bests,
		// random particles from each swarm were chosen to the
		// context vector. In this new version a random row from pop
		// is set to yHat. This is much simpler, and it doesn't
		// change the behavior of the algorithm.
		/*
		double[] yHat = AlgorithmUtils.generateRandomSolution(bounds,
		        problemDimension);
		        */
		int randRow = RandUtils.randomInt(populationSize);
		double[] yHat = getVector(X, 0, randRow,
		        problemDimension);
		// yHat fitness
		double fHat = problem.f(yHat);
		evaluations = 1;
		// note: fBest == fHat all the time
		fBest = fHat;

		double[] sBestsFits;
		double[][] sBests;
		double[][] pBestsFits;
		double[][][] pBests;

		boolean improved = false; 
		
		// iterate
		while (evaluations < maxEvaluations) {
		    
		    // if the solution didn't improve, pick a new random
		    // group size s from the set S
			if (!improved) {
				s = S[RandUtils.randomInt(S.length)];
				K = problemDimension / s;
			}

    		// initialize swarms and particles
    		sBestsFits = new double[K];
    		sBests = new double[K][s];
    		pBestsFits = new double[K][populationSize];
    		pBests = new double[K][populationSize][s];
    		
    		// pick personal bests from the permuted Y matrix
    		for (int i = 0; i < K; i++) {
    		    for (int j = 0; j < populationSize; j++)
    		        pBests[i][j] =
    		            getVector(Y, perm, i, j, s);

    		    // pick swarm bests from the permuted yHat vector
    		    sBests[i] = getSwarmthPart(yHat, perm, s, i);
    		    sBestsFits[i] = fHat;
    		}
    		

    		// a temporary vector
    		double[] currentVector;
    		
			int check = 0;
			// for each swarm
			for (int i = 0; i < K; i++) {

			    // for each particle in the swarm
				for (int j = 0; j < populationSize; j++) {	

				    // The context vector for calculating fitnesses
				    // of lower-dimensional vectors. This is always a
				    // concatenation of current swarm bests at any
				    // given time.
				    double[] context = new double[problemDimension];
			        for (int k = 0; k < K; k++)
			            setSwarmthPart(sBests[k], context, k);
			    
				    // calculate the new fitnesses of the current
				    // population in the new grouping (the context
				    // changes when the grouping changes)
			        currentVector = getVector(X, perm, i, j, s);
    		        double[] currentFullVector = b(currentVector, context, i);
				    // reverse to original order and calculate the
    		        // fitness for this
    		        currentFullVector = reverse(currentFullVector, perm);
    		        currentFit = problem.f(currentFullVector);
    		        evaluations++;
				    
				    currentFullVector = b(pBests[i][j], context, i);
				    currentFullVector = reverse(currentFullVector, perm);
				    // calculate the fitnesses of personal bests in
				    // the new context
    		        pBestsFits[i][j] = problem.f(currentFullVector);
    		        evaluations++;

    		        // A MODIFICATION: this is not needed
    		        /*
    		        if (generation == 0)
    		            pBestsFits[j][i] = currentFit;
    		        */
    		        
    		        // update the personal best
    		        if (currentFit < pBestsFits[i][j]) {
    		            pBestsFits[i][j] = currentFit;
    		            for (int k = 0; k < s; k++)
    		                pBests[i][j][k] = currentVector[k];
    		        }
    		        
    		        // update the swarm best
    		        // note: this changes the context vector for the
    		        // fitness calculations of the next swarm
    		        if (pBestsFits[i][j] < sBestsFits[i]) {
    		            sBestsFits[i] = pBestsFits[i][j];
    		            for (int k = 0; k < s; k++)
    		                sBests[i][k] = pBests[i][j][k];
    		        }
    		        
				}
				
		        // update the swarm'th part of the permutated yHat
		        if (sBestsFits[i] < fHat) {
		            check++;
		            fHat = sBestsFits[i];
		            fBest = fHat;
		            for (int k = 0; k < K; k++)
		                setSwarmthPart(sBests[k], yHat, perm, k);
		        }

			}
			improved = check > 0;
			
			for (int i = 0; i < K; i++) {

				for (int j = 0; j < populationSize; j++) {	
				    double[] nBest = new double[s];

				    // neighborhood best update
					int[] PN = circularIndex(j, populationSize);
					int imin = localBest(pBestsFits, i, j, PN[0], PN[1]);
					for (int k = 0; k < s; k++)
					    nBest[k] = pBests[i][imin][k];

					// position update
					double[] pBest = pBests[i][j];
					currentVector = updatePosition(pBest, nBest, p);
					currentVector = saturateToro(currentVector, bounds);
					//currentVector = saturateMirror(currentVector, bounds);
					setVector(X, perm, currentVector, i, j);
					setVector(Y, perm, pBest, i, j);
				}
			}
			
		    this.callGenerationLoggers();
			generation++;

			// draw a new permutation
    	    RandUtils.randomPermutation(perm);
			
		}
		
		this.finalBest = yHat;
		this.finalFbest = fBest;
		
		bests.add(new Best(evaluations, fBest));
		
		return bests;
	}

    /**
     * Reverse the permutation of *vector* back to its original
     * order when it has been permutated by *perm*
	 * @param vector the array whose order to reverse back to its
	 *        original
	 * @param perm the permutation array
	 * @return a new array that is *vector* permutated back to 
	 *         its original order
	 */
	public double[] reverse(double[] vector, int[] perm) {
	    
	    double[] orderedVector = new double[vector.length];
	    for (int i = 0; i < vector.length; i++)
	        orderedVector[perm[i]] = vector[i];
	    return orderedVector;
	}
	
	/**
	 * Form a new vector where the *swarm*'th component of
	 * *contextVector* is replaced by the given *vector*, when the 
	 * *contextVector* is divided into parts according to the length
	 * of the *vector*. contextVector.length must be divisible by
	 * by vector.length.
	 * @param vector the vector that replaces part of contextVector
	 * @param contextVector the context vector in which a part is to
	 *        be replaced
	 * @param swarm in which part to do the exchange
	 * @return a new vector where the *swarm*'th part of contextVector is
	 *         replaced by the given vector
	 * @example
	 * <pre name="test">
	 *  #import java.util.Arrays;
	 *  #import java.util.Vector;
	 *  CCPSO2 alg = new CCPSO2(new Vector<Number>());
	 *  double[] u;
	 *  
	 *  double[] contextVector = {0, 1, 2, 3};
	 *  
	 *  double[] vector = {-5};
	 *  u = alg.b(vector, contextVector, 0);
	 *  Arrays.toString(u) === "[-5.0, 1.0, 2.0, 3.0]";
	 *  u = alg.b(vector, contextVector, 1);
	 *  Arrays.toString(u) === "[0.0, -5.0, 2.0, 3.0]";
	 *  u = alg.b(vector, contextVector, 2);
	 *  Arrays.toString(u) === "[0.0, 1.0, -5.0, 3.0]";
	 *  u = alg.b(vector, contextVector, 3);
	 *  Arrays.toString(u) === "[0.0, 1.0, 2.0, -5.0]";
	 *  alg.b(vector, contextVector, 4); #THROWS IllegalArgumentException
	 *  alg.b(vector, contextVector, -1); #THROWS IllegalArgumentException
	 *  
	 *  vector = new double[]{-5, -6};
	 *  u = alg.b(vector, contextVector, 0);
	 *  Arrays.toString(u) === "[-5.0, -6.0, 2.0, 3.0]";  
	 *  u = alg.b(vector, contextVector, 1);
	 *  Arrays.toString(u) === "[0.0, 1.0, -5.0, -6.0]";  
	 *  // now the context vector has only swarms 0 and 1
	 *  alg.b(vector, contextVector, 2); #THROWS IllegalArgumentException
	 *  alg.b(vector, contextVector, -1); #THROWS IllegalArgumentException
	 *  
	 *  vector = new double[]{-5, -6, -7};
	 *  // now the the length of the context vector is not divisible
	 *  // by the length of the vector
	 *  alg.b(vector, contextVector, 0); #THROWS IllegalArgumentException
	 *  
	 *  vector = new double[]{-5, -6, -7, -8};
	 *  u = alg.b(vector, contextVector, 0);
	 *  Arrays.toString(u) === "[-5.0, -6.0, -7.0, -8.0]";  
	 *  alg.b(vector, contextVector, 1); #THROWS IllegalArgumentException
	 *  
	 *  vector = new double[]{-5, -6, -7, -8, -9};
	 *  // now the vector is longer than the context vector
	 *  alg.b(vector, contextVector, 0); #THROWS IllegalArgumentException
	 * </pre>
	 */
	public double[] b(double[] vector, double[] contextVector,
	        int swarm)
	{
		int c = 0;
		int s = vector.length;
		int probDim = contextVector.length;

	    if (s > probDim) {
	        String msg = "The context vector should be longer in " +
	                     "length than the replacing vector.";
	        throw new IllegalArgumentException(msg);
	    }
	    if (probDim % s != 0) {
	        String msg = "The length of the context vector must be " +
	            "divisible by the length of the replacing vector.";
	        throw new IllegalArgumentException(msg);
	    }
	    int nSwarms = probDim / s;
	    if (swarm < 0 || swarm >= nSwarms) {
	        String msg = "The context vector does not have the " +
	                     "given swarm.";
	        throw new IllegalArgumentException(msg);
	    }
	        
		double[] temp  = new double[probDim];
		for(int n=0; n < probDim; n++)
			if (n < s*swarm || n >= s*(swarm + 1))
				temp[n] = contextVector[n];
			else
			{
				temp[n] = vector[c];
				c++;
			}
		
		return temp;
	}
	
	private double[] updatePosition(double[] pBest,
	        double[] nBest, double prob)
	{
		int s = nBest.length; 
		double[] position = new double[s];
		for(int k=0; k < s; k++)
			if(RandUtils.random() <= prob)
				position[k] = RandUtils.cauchy(pBest[k],
				        Math.abs(pBest[k] - nBest[k]) / 2.0);
			else
				position[k] = RandUtils.gaussian(nBest[k],
				        Math.abs(pBest[k] - nBest[k]) / 2.0);
		
		return position;
	}
	
	/**
	 * From the given index, generate the previous and the next
	 * indices and move them inside the integer interval [0, size[
	 * using periodic boundary conditions.
	 * @param index the given index
	 * @param size endpoint of the interval (exclusive)
	 * @return the previous and the next indices moved inside the
	 *         interval [0, size[ using periodic boundary conditions
	 * @example
	 * <pre name="test">
	 *  #import java.util.Arrays;
	 *  #import java.util.Vector;
	 *  CCPSO2 alg = new CCPSO2(new Vector<Number>());
	 *  int[] indices;
	 *  indices = alg.circularIndex(0, 1); 
	 *  Arrays.toString(indices) === "[0, 0]";
	 *  indices = alg.circularIndex(0, 2);
	 *  Arrays.toString(indices) === "[1, 1]";
	 *  indices = alg.circularIndex(1, 2);
	 *  Arrays.toString(indices) === "[0, 0]";
	 *  indices = alg.circularIndex(-1, 2); #THROWS IllegalArgumentException
	 *  indices = alg.circularIndex(2, 2); #THROWS IllegalArgumentException
	 *  indices = alg.circularIndex(0, 3);
	 *  Arrays.toString(indices) === "[2, 1]";
	 *  indices = alg.circularIndex(1, 3);
	 *  Arrays.toString(indices) === "[0, 2]";
	 *  indices = alg.circularIndex(2, 3);
	 *  Arrays.toString(indices) === "[1, 0]";
	 * </pre>
	 */
	public int[] circularIndex(int index, int size)
	{
	    if (size == 0) {
	        String msg = "The given size must be at least 1.";
	        throw new IllegalArgumentException(msg);
	    }
	    if (index < 0 || index > size-1) {
	        String msg = "The given index must be in the interval [0, size-1]";
	        throw new IllegalArgumentException(msg);
	    }
	    
	    if (size == 1)
	        return new int[]{0, 0};

		int[] pn = new int[2];
		pn[0] = index - 1;
		pn[1] = index + 1;
		if(index == 0)
			pn[0] = size - 1;
		else if(index == size - 1)
			pn[1] = 0;
		return pn;
	}
	
	/**
	 * Get the index of the best-fit particle in the local
	 * neighborhood of *part*'th particle in the *swarm*'th swarm.
	 * The neighborhood is defined by the indices *previous* and
	 * *next*.
	 * @param fit a 2D-array including all the fitnesses s.t.
	 *        fit[j][k] is the fitness of the k'th particle in the
	 *        j'th swarm
	 * @param swarm the index of the swarm
	 * @param part which part of the swarm
	 * @param previous the index of the previous particle
	 * @param next the index of the next particle
	 * @return the index of the best-fit particle in the local
	 *         neighborhood of *part*'th particle in the *swarm*'th
	 *         swarm
	 */
	public int localBest(double[][] fit, int swarm, int part, int previous, int next)
	{   
		int indexMin = part;
		if(fit[swarm][previous] < fit[swarm][indexMin])
			indexMin = previous;
		if(fit[swarm][next] < fit[swarm][indexMin])
			indexMin = next;
		
		return indexMin;
	}
	
	/**
	 * Get the *parth*'th part in the *swarm*'th swarm. The
	 * whole "population" is included in the *matrix*.
	 * @param matrix a 2D-array including the whole "population" s.t.
	 *        matrix[i] is the i'th particle
	 * @param swarm the index of the swarm
	 * @param part which part of the swarm
	 * @param dim the size of each swarm (swarmSize in the paper)
	 * @return the *part*'th particle in the *swarm*'th swarm
	 * @example
	 * <pre name="test">
	 *  #import java.util.Arrays;
	 *  #import java.util.Vector;
	 *  CCPSO2 alg = new CCPSO2(new Vector<Number>());
	 *  // Possible swarm sizes for this matrix are 1, 2 and 4.
	 *  // For 4 swarms, they are as follows:
	 *  //          swarm 0 __       __ swarm 2
	 *  //                    |     |
	 *  //                    v     v
	 *  double[][] matrix = {{1, 2, 3, 4},
	 *                       {5, 6, 7, 8}};
	 *  //                       ^     ^
	 *  //                       |     |
	 *  //              swarm1 ---     --- swarm 3
	 *  double[] u;
	 *  
	 *  // now 4 swarms (K == 4)
	 *  int dim = 1;
	 *  u = alg.getVector(matrix, 0, 0, dim);
	 *  Arrays.toString(u) === "[1.0]";
	 *  u = alg.getVector(matrix, 0, 1, dim);
	 *  Arrays.toString(u) === "[5.0]";
	 *  u = alg.getVector(matrix, 1, 0, dim);
	 *  Arrays.toString(u) === "[2.0]";
	 *  u = alg.getVector(matrix, 1, 1, dim);
	 *  Arrays.toString(u) === "[6.0]";
	 *  u = alg.getVector(matrix, 2, 0, dim);
	 *  Arrays.toString(u) === "[3.0]";
	 *  u = alg.getVector(matrix, 2, 1, dim);
	 *  Arrays.toString(u) === "[7.0]";
	 *  u = alg.getVector(matrix, 3, 0, dim);
	 *  Arrays.toString(u) === "[4.0]";
	 *  u = alg.getVector(matrix, 3, 1, dim);
	 *  Arrays.toString(u) === "[8.0]";
	 *  
	 *  // now 2 swarms (K == 2)
	 *  dim = 2;
	 *  u = alg.getVector(matrix, 0, 0, dim);
	 *  Arrays.toString(u) === "[1.0, 2.0]";
	 *  u = alg.getVector(matrix, 0, 1, dim);
	 *  Arrays.toString(u) === "[5.0, 6.0]";
	 *  u = alg.getVector(matrix, 1, 0, dim);
	 *  Arrays.toString(u) === "[3.0, 4.0]";
	 *  u = alg.getVector(matrix, 1, 1, dim);
	 *  Arrays.toString(u) === "[7.0, 8.0]";
	 *  
	 *  // now 1 swarm (K == 1)
	 *  dim = 4;
	 *  u = alg.getVector(matrix, 0, 0, dim);
	 *  Arrays.toString(u) === "[1.0, 2.0, 3.0, 4.0]";
	 *  u = alg.getVector(matrix, 0, 1, dim);
	 *  Arrays.toString(u) === "[5.0, 6.0, 7.0, 8.0]";
	 *  
	 *  // dim should be > 0
	 *  alg.getVector(matrix, 0, 0, 0); #THROWS IllegalArgumentException
	 *  // the matrix row-length is not divisible by the given dim 
	 *  alg.getVector(matrix, 0, 0, -1); #THROWS IllegalArgumentException
	 *  alg.getVector(matrix, 0, 0, 3); #THROWS IllegalArgumentException
	 *  alg.getVector(matrix, 0, 0, 5); #THROWS IllegalArgumentException
	 *  // the matrix cannot have this many swarms
	 *  alg.getVector(matrix, -1, 0, 1); #THROWS IllegalArgumentException
	 *  alg.getVector(matrix, 4, 0, 1); #THROWS IllegalArgumentException
	 *  // the matrix cannot have this many particles
	 *  alg.getVector(matrix, 0, -1, 1); #THROWS IllegalArgumentException
	 *  alg.getVector(matrix, 0, 2, 1); #THROWS IllegalArgumentException
	 * </pre>
	 */
	public double[] getVector(double[][] matrix, int swarm, int part, int dim) {

		if (dim <= 0) {
		    String msg = "The dim should be > 0.";
		    throw new IllegalArgumentException(msg);
		}
		if (matrix[0].length % dim != 0) {
		    String msg = "The matrix row length has to be " +
		                 "divisible by the given dimension.";
		    throw new IllegalArgumentException(msg);
		}
		int K = matrix[0].length / dim;
		if (swarm < 0 || swarm >= K) {
		    String msg = "The matrix cannot have " + swarm +
		                 " swarms.";
		    throw new IllegalArgumentException(msg);
		}
		if (part < 0 || part >= matrix.length) {
		    String msg = "The matrix cannot have " + part +
		                 " particles.";
		    throw new IllegalArgumentException(msg);
		}

		double[] vector = new double[dim];
		for (int k = 0; k < dim; k++) 
			vector[k] = matrix[part][swarm*dim + k];	
		
		return vector;
	}
	
	/** Get the *parth*'th part in the *swarm*'th swarm, when
	 * the indices of *matrix* have been permutated by the
	 * index-array *perm*. The whole "population" is included in
	 * *matrix*.
	 * @param matrix a 2D-array including the whole "population" s.t.
	 *        matrix[i] is the i'th particle
	 * @param perm the permutation array
	 * @param swarm the index of the swarm
	 * @param part which part of the swarm
	 * @param dim the size of each swarm (swarmSize in the paper)
	 * @return the *part*'th particle in the *swarm*'th swarm when
	 *         the indices of *matrix* have been shuffled by the
	 *         index-array *perm*
	 */
	public double[] getVector(double[][] matrix, int[] perm,
	        int swarm, int part, int dim) {

		double[] vector = new double[dim];
		for (int k = 0; k < dim; k++)
			vector[k] = matrix[part][perm[swarm*dim + k]];	
		
		return vector;
	}

	/**
	 * The same as the getVector method, but for setting.
	 * @param matrix see the method getVector  
	 * @param vector the vector to set
	 * @param swarm see the method getVector 
	 * @param part see the method getVector
	 * @example
	 * <pre name="test">
	 *  #import java.util.Arrays;
	 *  #import java.util.Vector;
	 *  CCPSO2 alg = new CCPSO2(new Vector<Number>());
	 *  double[][] matrix = {{1, 2, 3, 4},
	 *                       {5, 6, 7, 8}};
	 *  double[] u;
	 *  
	 *  // now 4 swarms (K == 4)
	 *  alg.setVector(matrix, new double[]{-1}, 0, 0);
	 *  Arrays.toString(matrix[0]) === "[-1.0, 2.0, 3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[5.0, 6.0, 7.0, 8.0]";
	 *  alg.setVector(matrix, new double[]{-2}, 0, 1);
	 *  Arrays.toString(matrix[0]) === "[-1.0, 2.0, 3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[-2.0, 6.0, 7.0, 8.0]";
	 *  alg.setVector(matrix, new double[]{-3}, 2, 0);
	 *  Arrays.toString(matrix[0]) === "[-1.0, 2.0, -3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[-2.0, 6.0, 7.0, 8.0]";
	 *  
	 *  // 2 swarms (K == 2)
	 *  alg.setVector(matrix, new double[]{-7, -8}, 1, 1);
	 *  Arrays.toString(matrix[0]) === "[-1.0, 2.0, -3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[-2.0, 6.0, -7.0, -8.0]";
	 *  alg.setVector(matrix, new double[]{-1, -2}, 0, 0);
	 *  Arrays.toString(matrix[0]) === "[-1.0, -2.0, -3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[-2.0, 6.0, -7.0, -8.0]";
	 *  
	 *  // 1 swarm (K == 1)
	 *  alg.setVector(matrix, new double[]{1, 2, 3, 4}, 0, 0);
	 *  Arrays.toString(matrix[0]) === "[1.0, 2.0, 3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[-2.0, 6.0, -7.0, -8.0]";
	 *  alg.setVector(matrix, new double[]{5, 6, 7, 8}, 0, 1);
	 *  Arrays.toString(matrix[0]) === "[1.0, 2.0, 3.0, 4.0]";
	 *  Arrays.toString(matrix[1]) === "[5.0, 6.0, 7.0, 8.0]";
	 *  
	 *  // the matrix row-length is not divisible by the vector length
	 *  alg.setVector(matrix, new double[]{1, 2, 3, 4, 5}, 0, 0); #THROWS IllegalArgumentException
	 *  alg.setVector(matrix, new double[]{1, 2, 3}, 0, 0); #THROWS IllegalArgumentException
	 *  // the matrix cannot have this many swarms
	 *  alg.setVector(matrix, new double[]{1}, -1, 0); #THROWS IllegalArgumentException
	 *  alg.setVector(matrix, new double[]{1}, 4, 0); #THROWS IllegalArgumentException
	 *  // the matrix cannot have this many particles
	 *  alg.setVector(matrix, new double[]{1}, 0, -1); #THROWS IllegalArgumentException
	 *  alg.setVector(matrix, new double[]{1}, 0, 2); #THROWS IllegalArgumentException
	 * </pre>
	 */
	public void setVector(double[][] matrix, double[] vector, int swarm, int part)
	{
		int dim = vector.length;
		if (dim == 0) {
		    String msg = "The vector length should be > 0.";
		    throw new IllegalArgumentException(msg);
		}
		if (matrix[0].length % dim != 0) {
		    String msg = "The matrix row length has to be " +
		                 "divisible by the vector length.";
		    throw new IllegalArgumentException(msg);
		}
		int K = matrix[0].length / dim;
		if (swarm < 0 || swarm >= K) {
		    String msg = "The matrix cannot have " + swarm +
		                 "swarms.";
		    throw new IllegalArgumentException(msg);
		}
		if (part < 0 || part >= matrix.length) {
		    String msg = "The matrix cannot have " + part +
		                 "particles.";
		    throw new IllegalArgumentException(msg);
		}
		
		for (int k = 0; k < dim; k++) 
			matrix[part][swarm*dim + k] = vector[k];	
		
	}
	
	/**
	 * The same as the getVector method, but for setting.
	 * @param matrix see the method getVector  
	 * @param perm the permutation array for the columns of *matrix*
	 * @param vector the vector to set
	 * @param swarm see the method getVector 
	 * @param part see the method getVector
	 */
	public void setVector(double[][] matrix, int[] perm,
	        double[] vector, int swarm, int part) {

		int dim = vector.length;
		for (int k = 0; k < dim; k++) 
			matrix[part][perm[swarm*dim + k]] = vector[k];	
	}
	
	/**
	 * If the *contextVector* is divided into
	 * contextVector/vector.length parts, replace the *swarm*'th part
	 * of *contextVector* with *vector*.
	 * @param vector the vector to substitute into contextVector
	 * @param contextVector the vector where to make the replacement
	 * @param swarm which part of contextVector to replace
	 * @example
	 * <pre name="test">
	 *  #import java.util.Arrays;
	 *  #import java.util.Vector;
	 *  CCPSO2 alg = new CCPSO2(new Vector<Number>());
	 *  
	 *  double[] context = {0};
	 *  alg.setSwarmthPart(new double[]{1}, context, 0);
	 *  Arrays.toString(context) === "[1.0]";
	 *  // the context vector length is not divisible by the vector
	 *  // length
	 *  alg.setSwarmthPart(new double[]{}, context, 0); #THROWS IllegalArgumentException
	 *  alg.setSwarmthPart(new double[]{1, 2}, context, 0); #THROWS IllegalArgumentException
	 *  // illegal swarm
	 *  alg.setSwarmthPart(new double[]{1}, context, -1); #THROWS IllegalArgumentException
	 *  alg.setSwarmthPart(new double[]{1}, context, 2); #THROWS IllegalArgumentException
	 *  
	 *  context = new double[]{0, 1};
	 *  alg.setSwarmthPart(new double[]{-1}, context, 0);
	 *  Arrays.toString(context) === "[-1.0, 1.0]";
	 *  alg.setSwarmthPart(new double[]{-2}, context, 1);
	 *  Arrays.toString(context) === "[-1.0, -2.0]";
	 *  alg.setSwarmthPart(new double[]{-2}, context, 2); #THROWS IllegalArgumentException
	 *  alg.setSwarmthPart(new double[]{1, 2}, context, 0);
	 *  Arrays.toString(context) === "[1.0, 2.0]";
	 *  alg.setSwarmthPart(new double[]{1, 2}, context, 1); #THROWS IllegalArgumentException
	 *  
	 *  context = new double[]{0, 1, 2};
	 *  alg.setSwarmthPart(new double[]{-2}, context, 2);
	 *  Arrays.toString(context) === "[0.0, 1.0, -2.0]";
	 *  alg.setSwarmthPart(new double[]{-1, -2}, context, 0); #THROWS IllegalArgumentException
	 *  alg.setSwarmthPart(new double[]{-1, -2, -3}, context, 0);
	 *  Arrays.toString(context) === "[-1.0, -2.0, -3.0]";
	 *  alg.setSwarmthPart(new double[]{-1, -2, -3}, context, 1); #THROWS IllegalArgumentException
	 *  alg.setSwarmthPart(new double[]{-1, -2, -3, -4}, context, 0); #THROWS IllegalArgumentException
	 * </pre>
	 */
	public void setSwarmthPart(double[] vector,
	        double[] contextVector, int swarm) {

		int dim = vector.length;
		if (dim == 0) {
		    String msg = "The vector length should be > 0.";
		    throw new IllegalArgumentException(msg);
		}
		if (contextVector.length % dim != 0) {
		    String msg = "The length of the context vector has " +
	            "to be divisible by the length of the vector";
		    throw new IllegalArgumentException(msg);
		}
		int K = contextVector.length / dim;
		if (swarm < 0 || swarm >= K) {
		    String msg = "The context vector cannot have " + swarm +
		                 "parts.";
		    throw new IllegalArgumentException(msg);
		}
		
		for(int k = 0; k < dim; k++)
			contextVector[swarm*dim + k] = vector[k];
	}

	/**
	 * If the *contextVector* is divided into
	 * contextVector/vector.length parts and *contextVector* is
	 * permutated by *perm*, replace the *swarm*'th part
	 * of *contextVector* with *vector*.
	 * @param vector the vector to substitute into contextVector
	 * @param contextVector the vector where to make the replacement
	 * @param perm the permutation array
	 * @param swarm which part of contextVector to replace
	 */
	public void setSwarmthPart(double[] vector,
	        double[] contextVector, int[] perm, int swarm) {

		int dim = vector.length;
		for(int k = 0; k < dim; k++)
			contextVector[perm[swarm*dim + k]] = vector[k];
	}

	/**
	 * Get the *swarm*'th part of *vector* when *vector* is divided
	 * into *swarmDim* parts.
	 * @param vector the vector from which to get the part
	 * @param swarmDim dimension of each swarm
	 * @param swarm index of the swarm
	 * @return the swarm'th part of vector 
	 */
	public double[] getSwarmthPart(double[] vector, int swarmDim,
	        int swarm) {
	    
	    double[] swarmthPart = new double[swarmDim];
	    for (int k = 0; k < swarmDim; k++)
	        swarmthPart[k] = vector[swarm * swarmDim + k];
	    
	    return swarmthPart;
	}

	/**
	 * Get the *swarm*'th part of *vector* when *vector* is divided
	 * into *swarmDim* parts and the dimension indices of *vector*
	 * have been permutated by the index-array *perm*.
	 * @param vector the vector from which to get the part
	 * @param perm the permutation array
	 * @param swarmDim dimension of each swarm
	 * @param swarm index of the swarm
	 * @return the swarm'th part of vector 
	 */
	public double[] getSwarmthPart(double[] vector, int[] perm,
	        int swarmDim, int swarm) {
	    
	    double[] swarmthPart = new double[swarmDim];
	    for (int k = 0; k < swarmDim; k++)
	        swarmthPart[k] = vector[perm[swarm * swarmDim + k]];
	    
	    return swarmthPart;
	}

    @Override
    public String getParametersAsFilenameString() {

		int populationSize = this.getParameter(0).intValue();
		String p = String.format(locale, format,
			   this.getParameter(1).doubleValue());
		return "NP"+populationSize+"_p"+p;
    }

    @Override
    public String getParametersAsSaveableString() {

		int populationSize = this.getParameter(0).intValue();
		String p = String.format(locale, format,
			   this.getParameter(1).doubleValue());
		return "NP "+populationSize+" p "+p;
    }

    @Override
    public String getParametersAsPrintableString() {

		int populationSize = this.getParameter(0).intValue();
		String p = String.format(locale, format,
			   this.getParameter(1).doubleValue());
		return "NP "+populationSize+", p "+p;
    }
    
    @Override
    public Algorithm copy() {
        return new CCPSO2(this);
    }
    
    @Override
    public Vector<Number> getLoggableVariables() {

        Vector<Number> variables = this.getBasicLoggableVariables();
        variables.add(new Double(this.fBest));
        variables.add(new Integer(this.s));
        return variables;
    }

    /**
     * A logger class for logging the group size.
     * @author Teemu Peltonen
     * @version Dec 5, 2014
     */
    public class SaveGroupSize extends GenerationLogger {
        
        /**
         * @param path path of the folder for the log file
         * @param interval the interval between the consecutive
         *        log steps
         */
        public SaveGroupSize(String path, int interval) {
            super(path, "groupsize", interval);
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int[] array = new int[2];
            // generation
            array[0] = loggableVariables.get(0).intValue();
            // group size
            array[1] = loggableVariables.get(3).intValue();
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.saveArrayAsRow(array, filename, true);
        }
    }

}
