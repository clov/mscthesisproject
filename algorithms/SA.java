package algorithms;

import static algorithms.utils.AlgorithmUtils.generateRandomSolution;

import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Vector;

import problems.interfaces.Problem;

import utils.FileUtils;
import utils.MathUtils;
import utils.random.RandUtils;
import algorithms.interfaces.Algorithm;
import algorithms.interfaces.GenerationLogger;
import algorithms.utils.Best;

/**
 * A "standard" simulated annealing algorithm.
 * 
 * @author Teemu Peltonen
 * @version Apr 8, 2015
 */
public class SA extends Algorithm
{
    private static final Locale locale = Locale.ENGLISH;
    private static final String format = "%.4f";

	private double fBest;
	// temperature at generation k
	private double tk;
	// step size at generation k
	private double thetak;
	// the acceptance ratio during the last Navg steps
	private double acceptanceRatio;
	
    /**
     * @param parameters parameters for the algorithm
     */
    public SA(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public SA(Algorithm algorithm) {
        super(algorithm);
    }
    
	@Override
	public Vector<Best> execute(Problem problem, int maxEvaluations,
	        String folder, int repetitionNr)
	                throws FileNotFoundException
	{
	    repNr = repetitionNr;
	    
	    // factor in the temperature decreasing process
		double khi = this.getParameter(0).doubleValue();
		// initial temperature
		int T0 = this.getParameter(1).intValue();
		// constant step size parameter
		double r = this.getParameter(2).doubleValue();
		// number of steps at each temperature
		int lk = this.getParameter(3).intValue();
		
		Vector<Best> bests = new Vector<Best>();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] oldPt = new double[problemDimension];
		double[] newPt = new double[problemDimension];

		double fOld;
		double fNew;

		// initialize first point
		oldPt = generateRandomSolution(bounds, problemDimension);
		fNew = problem.f(oldPt);
		bests.add(new Best(0, fNew));
		fBest = fNew;
		fOld = fNew;
		
		generation = 0;
		evaluations = 0;
		tk = T0;
		// random direction vector
		double theta[];

		while (evaluations < maxEvaluations) {

		    // generate the new neighbour
			//newPt = generateRandomSolution(bounds, problemDimension);
		    theta = generateRandomSolution(bounds, problemDimension);
		    MathUtils.toLength(theta, r);
		    for (int i = 0; i < problemDimension; i++) {
		        //FIXME
		        //newPt[i] = oldPt[i] + thetak * RandUtils.random(-1, 1);
		        //newPt[i] = RandUtils.gaussian(oldPt[i], tk);
		        //newPt[i] = bestPt[i] + RandUtils.random(-tk, tk);
		        newPt[i] = oldPt[i] + theta[i];
		    }

			// evaluate fitness
			fNew = problem.f(newPt);
			evaluations++;

			// move to the neighbor point
			if ((fNew <= fOld) || (Math.exp((fOld-fNew)/tk) > RandUtils.random()))
			{
				for (int k = 0; k < problemDimension; k++)
					oldPt[k] = newPt[k];
				fOld = fNew;
				fBest = fNew;
			}

		    if (generation % lk == 0)
		        tk *= khi;

			generation++;
			this.callGenerationLoggers();
		}
		
		this.finalBest = oldPt;
		this.finalFbest = fBest;
		
		bests.add(new Best(evaluations, fBest));

		return bests;
	}
	
	private String[] getParameters() {
	    
		String khi = String.format(locale, format, 
	                this.getParameter(0).doubleValue());
		String T0 = String.format(locale, format, 
	                this.getParameter(1).doubleValue());
		String r = String.format(locale , format, 
	                this.getParameter(2).doubleValue());
		String lk = this.getParameter(3).toString();
		
		return new String[]{khi, T0, r, lk};
	}
	
    @Override
    public String getParametersAsFilenameString() {

        String[] p = getParameters();
		return "khi"+p[0]+"_T0"+p[1]+"_r"+p[2]+"_lk"+p[3];
    }

    @Override
    public String getParametersAsSaveableString() {

        String[] p = getParameters();
		return "khi "+p[0]+" T0 "+p[1]+" r "+p[2]+
		        " lk "+p[3];
    }

    @Override
    public String getParametersAsPrintableString() {

        String[] p = getParameters();
		return "khi "+p[0]+", T0 "+p[1]+", r "+p[2]+
		        ", lk "+p[3];
    }

    @Override
    public Algorithm copy() {
        return new SA(this);
    }

    @Override
    public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		vars.add(new Double(this.tk));
		vars.add(new Double(this.thetak));
		vars.add(new Double(this.acceptanceRatio));
		
		return vars;
    }

    /**
     * A logger class for logging the temperature.
     * @author Teemu Peltonen
     * @version Mar 30, 2015
     */
    public class SaveTemp extends GenerationLogger {
        
        /**
         * @param path path of the folder for the log file
         * @param interval the interval between the consecutive
         *        log steps
         */
        public SaveTemp(String path, int interval) {
            super(path, "temp", interval);
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int gen = loggableVariables.get(0).intValue();
            double temp = loggableVariables.get(3).doubleValue();
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.save(gen, temp, filename, true);
        }
    }

}
