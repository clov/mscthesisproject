package algorithms;

import static algorithms.utils.AlgorithmUtils.generateRandomSolution;

import java.io.FileNotFoundException;
import java.util.Vector;

import problems.interfaces.Problem;

import algorithms.interfaces.Algorithm;
import algorithms.utils.Best;

/**
 * A dummy minimization algorithm.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public class Dummy extends Algorithm {
    
	private double fBest;
	
    /**
     * Instantiate a new Dummy algorithm with no parameters
     */
    public Dummy() {
        super();
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public Dummy(Algorithm algorithm) {
        super(algorithm);
    }

    @Override
    public Vector<Best> execute(Problem problem, int maxEvaluations,
            String folder, int repetitionNr) throws FileNotFoundException {
        
        this.repNr = repetitionNr;

        int problemDimension = problem.getDimension();
        double[][] bounds = problem.getBounds();

        double[] x = new double[problemDimension];
        double[] xBest = new double[problemDimension];
        this.fBest = Double.MAX_VALUE;

        double fx;
        Vector<Best> bests = new Vector<Best>();

        for (int i = 0; i < maxEvaluations; i++) {
            // generate a new random solution
            x = generateRandomSolution(bounds, problemDimension);
            // evaluate the fitness function
            fx = problem.f(x);
            // if better, replace the previous best
            if (fx < this.fBest) {
                xBest = x.clone();
                this.fBest = fx;
                // save the fitness value of the best configuration at
                // each step its getting better (now the population
                // size is 1, so there is always only one
                // configuration)
                bests.add(new Best(i, this.fBest));
            }
            this.callGenerationLoggers();
        }
        this.finalBest = xBest;
        return bests;
    }

    @Override
    public String getParametersAsFilenameString() {
        return "";
    }

    @Override
    public String getParametersAsSaveableString() {
        return "";
    }

    @Override
    public String getParametersAsPrintableString() {
        return "";
    }

    @Override
    public Algorithm copy() {
        return new Dummy(this);
    }

	@Override
	public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		
		return vars;
	}

}
