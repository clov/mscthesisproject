package algorithms;


import static algorithms.utils.AlgorithmUtils.generateRandomSolution;
import static algorithms.utils.AlgorithmUtils.saturateToro;

import java.io.FileNotFoundException;
import java.util.Vector;

import problems.interfaces.Problem;

import algorithms.interfaces.Algorithm;
import algorithms.utils.Best;


/**
 * @author modified by Teemu Peltonen
 * @version Dec 5, 2014
 */
public class S extends Algorithm
{
    
	private double fBest;
	
    /**
     * @param parameters parameters for the algorithm
     */
    public S(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public S(Algorithm algorithm) {
        super(algorithm);
    }

	@Override
	public Vector<Best> execute(Problem problem, int maxEvaluations,
	        String folder, int repetitionNr)
	                throws FileNotFoundException
	{
	    repNr = repetitionNr;
 
		int deepLSSteps = this.getParameter(0).intValue(); 
		double deepLSRadius = this.getParameter(1).doubleValue();
		
		Vector<Best> bests = new Vector<Best>();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();


		// current best
		double[] best = new double[problemDimension];

		int i = 0;
		if (this.initialSolution != null)
		{
			best = this.initialSolution;
			fBest = this.initialFitness;
		}
		else
		{
			best = generateRandomSolution(bounds, problemDimension);		
			fBest = problem.f(best);
			i++;
		}
		
		
		while (i < maxEvaluations)
		{
		
			double[] SR = new double[problemDimension];
			for (int k = 0; k < problemDimension; k++)
				SR[k] = (bounds[k][1] - bounds[k][0]) * deepLSRadius;
			boolean improve = true;
			int j = 0;
			while ((j < deepLSSteps) && (i < maxEvaluations))
			{
				double[] Xk = new double[problemDimension];
				double[] Xk_orig = new double[problemDimension];
				for (int k = 0; k < problemDimension; k++)
				{
					Xk[k] = best[k];
					Xk_orig[k] = best[k];
				}
				double fXk_orig = fBest;
				if (!improve)
				{
					for (int k = 0; k < problemDimension; k++)
						SR[k] = SR[k]/2;
				}
				improve = false;
				int k = 0;
				while ((k < problemDimension) && (i < maxEvaluations))
				{
					Xk[k] = Xk[k] - SR[k];
					Xk = saturateToro(Xk, bounds);
					double fXk = problem.f(Xk);
					i++;
					// best update
					if (fXk < fBest)
					{
						fBest = fXk;
						for (int n = 0; n < problemDimension; n++)
							best[n] = Xk[n];
					}
					if (i < maxEvaluations)
					{
						if (fXk == fXk_orig)
						{
							for (int n = 0; n < problemDimension; n++)
								Xk[n] = Xk_orig[n];
						}
						else
						{
							if (fXk > fXk_orig)
							{
								Xk[k] = Xk_orig[k];
								Xk[k] = Xk[k] + 0.5*SR[k];
								Xk = saturateToro(Xk, bounds);
								fXk = problem.f(Xk);
								//dCounter++;
								i++;
								// best update
								if (fXk < fBest)
								{
									fBest = fXk;
									for (int n = 0; n < problemDimension; n++)
										best[n] = Xk[n];
								}
								if (fXk >= fXk_orig)
									Xk[k] = Xk_orig[k];
								else
									improve = true;
							}
							else
								improve = true;
						}
					}
					k++;
				}
				j++;
			}
			this.callGenerationLoggers();
			break;
		}

		this.finalBest = best;
		bests.add(new Best(i,fBest));
		return bests;
	}

    @Override
    public String getParametersAsFilenameString() {

		int deepLSSteps = this.getParameter(0).intValue(); 
		double deepLSRadius = this.getParameter(1).doubleValue();
		return "deepLSSteps"+deepLSSteps+"_deepLSRadius"+deepLSRadius;
    }

    @Override
    public String getParametersAsSaveableString() {

		int deepLSSteps = this.getParameter(0).intValue(); 
		double deepLSRadius = this.getParameter(1).doubleValue();
		return "deepLSSteps "+deepLSSteps+" deepLSRadius "+deepLSRadius;
    }

    @Override
    public String getParametersAsPrintableString() {

		int deepLSSteps = this.getParameter(0).intValue(); 
		double deepLSRadius = this.getParameter(1).doubleValue();
		return "deepLSSteps "+deepLSSteps+", deepLSRadius "+deepLSRadius;
    }

    @Override
    public Algorithm copy() {
        return new S(this);
    }

    @Override
    public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		
		return vars;
    }
}
