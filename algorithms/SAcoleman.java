package algorithms;

import static algorithms.utils.AlgorithmUtils.generateRandomSolution;

import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Vector;

import problems.interfaces.Problem;

import utils.FileUtils;
import utils.random.RandUtils;
import algorithms.interfaces.Algorithm;
import algorithms.interfaces.GenerationLogger;
import algorithms.utils.Best;

/**
 * The "standard" Simulated Annealing as presented in [1]
 * (IEESA with lambda=0).
 * 
 * [1] Coleman, T. F. et al., Isotropic Effective Energy Simulated
 * Annealing Searches for Low Energy Molecular Cluster States,
 * Technical Report CTC92TR113, Advanced Computing Research Institute,
 * Cornell University, Ithaca, NY, 1992
 * @author Teemu Peltonen
 * @version Apr 8, 2015
 */
public class SAcoleman extends Algorithm
{
    private static final Locale locale = Locale.ENGLISH;
    private static final String format = "%.3f";

	private double fBest;
	// temperature at generation k
	private double tk;
	// step size at generation k
	private double thetak;
	// the acceptance ratio during the current coolign step
	private double acceptanceRatio;
	
    /**
     * @param parameters parameters for the algorithm
     */
    public SAcoleman(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public SAcoleman(Algorithm algorithm) {
        super(algorithm);
    }
    
	@Override
	public Vector<Best> execute(Problem problem, int maxEvaluations,
	        String folder, int repetitionNr)
	                throws FileNotFoundException
	{
	    repNr = repetitionNr;
	    
	    // factor in the temperature decreasing process
		double khi = this.getParameter(0).doubleValue();
		// initial temperature
		double T0 = this.getParameter(1).doubleValue();
		// initial step size parameter
		double theta0 = this.getParameter(2).doubleValue();
		
		double acceptanceMin = 0.25;
		double acceptanceMax = 0.50;
		
		Vector<Best> bests = new Vector<Best>();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] oldPt = new double[problemDimension];
		double[] newPt = new double[problemDimension];

		double fOld;
		double fNew;

		// initialize first point
		oldPt = generateRandomSolution(bounds, problemDimension);
		fNew = problem.f(oldPt);
		bests.add(new Best(0, fNew));
		fBest = fNew;
		fOld = fNew;
		
		generation = 0;
		evaluations = 0;
		tk = T0;
		thetak = theta0;
		// number of steps in each temperature
		int lk;
		// how many have been accepted in the current cooling step
		int nAccepted;

		while (evaluations < maxEvaluations) {

		    // number of generations for each cooling step
		    // (this assumes problemDimension >= 3 and tk <= e all
		    // the time)
			lk = (int)Math.floor(problemDimension/3.0 *
			        Math.log(problemDimension/3.0) *
			        (1 - Math.log(tk)));
			
			nAccepted = 0;

			// lk cooling steps at current temperature
			for (int l = 0; l < lk; l++) {
			    
    		    // generate the new neighbour
    		    for (int i = 0; i < problemDimension; i++) {
    		       newPt[i] = oldPt[i] + thetak * RandUtils.random(-1, 1);
    		    }
    
    			// evaluate fitness
    			fNew = problem.f(newPt);
    			evaluations++;
    
    			// move to the neighbor point
    			if ((fNew <= fOld) || (Math.exp((fOld-fNew)/tk) > RandUtils.random()))
    			{
    				for (int k = 0; k < problemDimension; k++)
    					oldPt[k] = newPt[k];
    				fOld = fNew;
    				fBest = fNew;
    				nAccepted++;
    			}
    
			}

			// update thetak (1.0 to make the ratio double)
	        acceptanceRatio = 1.0 * nAccepted / lk;
	        if (acceptanceRatio > acceptanceMax)
	            thetak *= 2.0;
	        else if (acceptanceRatio < acceptanceMin)
	            thetak /= 2.0;

    		// update temperature
            tk *= khi;

			generation++;
			this.callGenerationLoggers();
		}
		
		this.finalBest = oldPt;
		this.finalFbest = fBest;
		
		bests.add(new Best(evaluations, fBest));

		return bests;
	}
	
	private String[] getParameters() {
	    
		String alpha = String.format(locale, format, 
	                this.getParameter(0).doubleValue());
		String T0 = String.format(locale, format, 
	                this.getParameter(1).doubleValue());
		String theta0 = String.format(locale , format, 
	                this.getParameter(2).doubleValue());
		
		return new String[]{alpha, T0, theta0};
	}
	
    @Override
    public String getParametersAsFilenameString() {

        String[] p = getParameters();
		return "khi"+p[0]+"_T0"+p[1]+"_theta0"+p[2];
    }

    @Override
    public String getParametersAsSaveableString() {

        String[] p = getParameters();
		return "khi "+p[0]+" T0 "+p[1]+" theta0 "+p[2];
    }

    @Override
    public String getParametersAsPrintableString() {

        String[] p = getParameters();
		return "khi "+p[0]+", T0 "+p[1]+", theta0 "+p[2];
    }

    @Override
    public Algorithm copy() {
        return new SAcoleman(this);
    }

    @Override
    public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		vars.add(new Double(this.tk));
		vars.add(new Double(this.thetak));
		vars.add(new Double(this.acceptanceRatio));
		
		return vars;
    }

    /**
     * A logger class for logging the temperature.
     * @author Teemu Peltonen
     * @version Mar 30, 2015
     */
    public class SaveTemp extends GenerationLogger {
        
        /**
         * @param path path of the folder for the log file
         * @param interval the interval between the consecutive
         *        log steps
         */
        public SaveTemp(String path, int interval) {
            super(path, "temp", interval);
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int gen = loggableVariables.get(0).intValue();
            double temp = loggableVariables.get(3).doubleValue();
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.save(gen, temp, filename, true);
        }
    }

    /**
     * A logger class for logging the step size.
     * @author Teemu Peltonen
     * @version Apr 2, 2015
     */
    public class SaveStepSize extends GenerationLogger {
        
        /**
         * @param path path of the folder for the log file
         * @param interval the interval between the consecutive
         *        log steps
         */
        public SaveStepSize(String path, int interval) {
            super(path, "stepsize", interval);
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int gen = loggableVariables.get(0).intValue();
            double theta = loggableVariables.get(4).doubleValue();
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.save(gen, theta, filename, true);
        }
    }

    /**
     * A logger class for logging the acceptance ratio.
     * @author Teemu Peltonen
     * @version Apr 2, 2015
     */
    public class SaveAcceptanceRatio extends GenerationLogger {
        
        /**
         * @param path path of the folder for the log file
         * @param interval the interval between the consecutive
         *        log steps
         */
        public SaveAcceptanceRatio(String path, int interval) {
            super(path, "acceptanceratio", interval);
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int gen = loggableVariables.get(0).intValue();
            double ar = loggableVariables.get(5).doubleValue();
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.save(gen, ar, filename, true);
        }
    }
}
