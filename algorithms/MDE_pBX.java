package algorithms;

import static algorithms.utils.AlgorithmUtils.crossOverBin;
import static algorithms.utils.AlgorithmUtils.generateRandomSolution;
import static algorithms.utils.AlgorithmUtils.saturateToro;
import static utils.MathUtils.indexMin;

import java.io.PrintStream;
import java.util.Vector;

import problems.interfaces.Problem;

import utils.random.RandUtils;
import utils.FileUtils;
import algorithms.interfaces.Algorithm;
import algorithms.utils.Best;

/**
 * The MDE_pBX (Modified Differential Evolution with p-Best
 * Crossover) algorithm by Islam et al. [1].
 * 
 * [1] Islam, S. et al. An Adaptive Differential Evolution Algorithm
 * With Novel Mutation and Crossover Strategies for Global Numerical
 * Optimization. IEEE Transactions on Systems, Man, and Cybernetics,
 * Part B: Cybernetics, 2012, 42, 482-500
 * @author modified by Teemu Peltonen
 * @version Dec 5, 2014
 */
public class MDE_pBX extends Algorithm
{

	private double fBest;
	
    /**
     * @param parameters parameters for the algorithm such that
     *        parameters[0] is the population size and
     *        parameters[1] is the parameter q (in the
     *        current-to_gr-best scheme, the group size is q times
     *        the population size)
     */
    public MDE_pBX(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public MDE_pBX(Algorithm algorithm) {
        super(algorithm);
    }

    @Override
    public Vector<Best> execute(Problem problem, int maxEvaluations,
            String folder, int repetitionNr) throws Exception
    {
        repNr = repetitionNr;
        
        int populationSize = this.getParameter(0).intValue(); 
        double q = this.getParameter(1).doubleValue(); 

        double CRm = 0.6;
        double Fm = 0.5;
        double N = 1.5;

        int groupSize = (int)(populationSize*q);

        Vector<Best> bests = new Vector<Best>();
        int problemDimension = problem.getDimension(); 
        double[][] bounds = problem.getBounds();

        int G = 1;
        int Gmax = maxEvaluations%populationSize;
        if(Gmax == 0)
            Gmax = maxEvaluations/populationSize;
        else
            Gmax = maxEvaluations/populationSize + 1;

        double[][] population = new double[populationSize][problemDimension];
        double[] fitnesses = new double[populationSize];

        double[] best = new double[problemDimension];
        fBest = Double.NaN;

        int i = 0;

        // evaluate initial population
        for (int j = 0; j < populationSize; j++)
        {
            double[] tmp;
            if (j == 0 && initialSolution != null)
                tmp = initialSolution;
            else
                tmp = generateRandomSolution(bounds, problemDimension);

            for (int n = 0; n < problemDimension; n++)
                population[j][n] = tmp[n];
            fitnesses[j] = problem.f(population[j]);

            if (j == 0 || fitnesses[j] < fBest)
            {
                fBest = fitnesses[j];
                for (int n = 0; n < problemDimension; n++)
                    best[n] = population[j][n];
                //bests.add(new Best(i, fBest));
            }
            if(i%problemDimension==0)
                bests.add(new Best(i, fBest));
            i++;
        }

        // temp variables
        double[] currPt = new double[problemDimension];
        double[] newPt = new double[problemDimension];
        double[] crossPt = new double[problemDimension];
        double[] temp;
        double currFit = Double.NaN;
        double crossFit = Double.NaN;

        String filename = folder+"FmEvolution_"+repNr+".txt";
        PrintStream FmFile = FileUtils.openFileForWriting(filename);
        filename = folder+"CRmEvolution_"+repNr+".txt";
        PrintStream CRmFile = FileUtils.openFileForWriting(filename);
        
        // iterate
        while (i < maxEvaluations)
        {
            double FmMeanPowSum = 0;
            double CrMeanPowSum = 0;
            int counter = 0; // how many successful replacements
            // for each particle
            for (int j = 0; j < populationSize && i < maxEvaluations; j++)
            {
                if(i%problemDimension==0)
                    bests.add(new Best(i, fBest));
                for (int n = 0; n < problemDimension; n++)
                    currPt[n] = population[j][n];
                currFit = fitnesses[j];

                //MUTATION
                temp = new double[populationSize];
                for(int n=0; n<populationSize; n++)
                    temp[n] = n; //vector of indexes
                int c = 1; 
                int r = RandUtils.randomInt(populationSize-c); 
                int I = (int)temp[r]; int indMin = I;
                temp = removeElementAtIndex(r,temp); c++;
                double[] XgrBest = population[I];
                double grMin = fitnesses[I];  
                for(int n=0; n<groupSize-1; n++) 
                {
                    r = RandUtils.randomInt(populationSize-c);
                    I = (int)temp[r];
                    temp = removeElementAtIndex(r,temp); c++;
                    if(fitnesses[I] < grMin)
                    {
                        grMin = fitnesses[I];
                        XgrBest = population[I]; 
                        indMin = I;
                    }

                }

                /*int r1 = randInteger(populationSize-1);
				int r2 = randInteger(populationSize-1);
				boolean exit = (r1 != j) & (r1 != indMin) & (r1 != r2) & (r2 != j) & (r2 != indMin);
				while(!exit)
				{
					r1 = randInteger(populationSize-1);
					r2 = randInteger(populationSize-1);
					exit = (r1 != j) & (r1 != I) & (r1 != r2) & (r2 != j) & (r2 != indMin);
				}*/
                // generate mutually distinct random numbers
                temp = new double[populationSize];
                for(int n=0; n<populationSize; n++)
                    temp[n] = n; //vector of indexes
                temp = removeElementAtIndex(j,temp);
                temp = removeElementAtIndex(indMin,temp);
                r = RandUtils.randomInt(populationSize-3);
                double[] Xr1 = population[(int)temp[r]];
                temp = removeElementAtIndex(r,temp);				
                r = RandUtils.randomInt(populationSize-4);
                double[] Xr2 = population[(int)temp[r]];
                double F = generateF(Fm);

                newPt = currentToGrBest( XgrBest, currPt, Xr1, Xr2,  F);

                // CROSSOVER
                int P = (int)Math.ceil( (populationSize/2)*(1 - G/Gmax) );
                int Pindex = RandUtils.randomInt(P-1);
                temp = new double[populationSize];
                for(int n=0; n<populationSize;n++)
                    temp[n] = fitnesses[n];                 
                // random member of the p-best population
                double[] pBest = null;
                for(int n=0; n<=Pindex;n++)
                {
                    int min = indexMin(temp);
                    pBest = population[min];
                    temp = removeElementAtIndex(min, temp);
                }	
                double CR = generateCR(CRm);
                crossPt = crossOverBin(pBest, newPt, CR);			

                //saturation
                crossPt = saturateToro(crossPt, bounds);

                crossFit = problem.f(crossPt);
                i++;

                // best update
                if (crossFit < fBest)
                {
                    fBest = crossFit;
                    for (int n = 0; n < problemDimension; n++)
                        best[n] = crossPt[n];
                    //bests.add(new Best(i, fBest));
                }


                // replacement
                if (crossFit < currFit)
                {
                    for (int n = 0; n < problemDimension; n++)
                        population[j][n] = crossPt[n];
                    fitnesses[j] = crossFit;
                    // Fsuccessful and CRsuccessful sets:
                    // evaluate the sum part of the power mean
                    counter++;
                    FmMeanPowSum += Math.pow(F, N);
                    CrMeanPowSum += Math.pow(CR , N);

                }
                else // di troppo?
                {
                    for (int n = 0; n < problemDimension; n++)
                        population[j][n] = currPt[n];
                    fitnesses[j] = currFit;
                }
            }
            G++;
            // Fm and CRm update
            // General power mean definition:
            // meanpowN(A) = (1/#A sum_{x\in A} x^N)^(1/N) for all #A > 0
            // If #A == 0 (i.e. counter == 0), don't update Fm and CRm
            if (counter > 0) {
                double Wf = 0.9 + 0.2*RandUtils.random();
                double FmMeanPow = Math.pow(FmMeanPowSum / counter, 1.0 / N);
                Fm = Wf*Fm + (1 - Wf)*FmMeanPow;

                double Wc = 0.9 + 0.1*RandUtils.random();
                double CrMeanPow = Math.pow(CrMeanPowSum / counter, 1.0 / N);
                CRm = Wc*CRm + (1 - Wc)*CrMeanPow;
            }
            FmFile.println(Fm);
            CRmFile.println(CRm);
            this.callGenerationLoggers();
        }

        FmFile.close();
        CRmFile.close();

        this.finalBest = best;

        bests.add(new Best(i, fBest));

        return bests;
    }	

    private double[] currentToGrBest(double[] X_gr_best, double[] X_target, double[] X_r1, double[] X_r2, double F)
    {	
        int dim = X_target.length;
        double[] v = new double[dim];
        for(int i=0; i<dim; i++)
            v[i] = X_target[i] + F*(X_gr_best[i] - X_target[i] + X_r1[i] - X_r2[i]);

        return v;
    }

    private double[] removeElementAtIndex(int ind, double[] array)
    {
        double[] newArray = new double[array.length - 1];
        //TODO this can be done in a better way, obviously
        for(int n=0; n<newArray.length; n++)
        {
            if(n<ind)
                newArray[n] = array[n];
            else
                newArray[n] = array[n+1];
        }
        return newArray;
    }

    private double generateCR(double mean)
    {	
        //TODO do while...
        double cr = Double.NaN;
        boolean generate = true;
        while(generate)
        {
            cr = RandUtils.gaussian(mean, 0.1);
            generate = cr<0 || cr>1;
        }	
        return cr;
        /*
        double cr = RandUtils.gaussian(mean,0.1);
        if (cr > 1)
            cr = cr - fix(cr);
        else if (cr < 0)
            cr=1-Math.abs(cr - fix(cr));
        return cr;
         */
    }
    
    private double generateF(double Fm)
    {
        double F = Double.NaN;
        boolean generate = true;
        while(generate)
        {
            F = RandUtils.cauchy(Fm, 0.1);
            generate = (F<=0 || F>1);
        }
        return F;
        /*
        F = RandUtils.cauchy(Fm , 0.1);
        double Fsat = (F - Double.MIN_VALUE)/(1 -Double.MIN_VALUE);
        if (Fsat > 1)
            F = Fsat - fix(Fsat);
        else if (Fsat < 0)
            F=1-Math.abs(Fsat-fix(Fsat));
        return F;
         */
    }

    @Override
    public String getParametersAsFilenameString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP"+populationSize+"_q"+q;
    }

    @Override
    public String getParametersAsSaveableString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP "+populationSize+" q "+q;
    }

    @Override
    public String getParametersAsPrintableString() {

	    int populationSize = this.getParameter(0).intValue();
	    double q = this.getParameter(1).doubleValue();
	    return "NP "+populationSize+", q "+q;
    }

    @Override
    public Algorithm copy() {
        return new MDE_pBX(this);
    }

	@Override
	public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.fBest));
		
		return vars;
	}

}