package algorithms.interfaces;

import java.io.FileNotFoundException;

/**
 * A base class for saving a log for a run of an algorithm.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public abstract class Logger {
    
    private final String path;
    private final String name;
    
    /**
     * @param path the path of the folder for the log file
     * @param name name of the logger
     */
    public Logger(String path, String name) {
        this.path = path;
        this.name = name;
    }
    
    /**
     * @return the path of the folder for the log file
     */
    public String getPath() {
        return this.path;
    }
    
    /**
     * @return the name of the logger
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Call the logger.
     * @param algorithm the algorithm instance that owns this logger
     * @throws FileNotFoundException if the file cannot be found
     */
    public abstract void call(Algorithm algorithm)
            throws FileNotFoundException;
    
}
