package algorithms.interfaces;

import java.io.FileNotFoundException;

import algorithms.interfaces.Logger;

/**
 * A logger that is called after each new generation, but in
 * multiples of the given interval.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public abstract class GenerationLogger extends Logger {

    private final int interval;

    /**
     * @param path the path of the folder for the log file
     * @param name name of the logger
     * @param interval The interval between the consevutive logs.
     */
    public GenerationLogger(String path, String name, int interval) {
        super(path, name);
        this.interval = interval;
    }

    /**
     * @return the interval between the consecutive logs
     */
    public int getInterval() {
        return this.interval;
    }

}
