package algorithms.interfaces;

import java.io.FileNotFoundException;
import java.util.Vector;
import algorithms.utils.Best;
import problems.interfaces.Problem;

/**
 * A class for a general algorithm.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public abstract class Algorithm {

    /** Parameters of the algorithm */
    protected Vector<Number> parameters;
    /** Initial solution vector */
    protected double[] initialSolution = null;
    /** Fitness of the initial solution */
    protected double initialFitness = Double.NaN;
    /** Final best solution vector */
    protected double[] finalBest;
    /** Final best fitness */
    protected double finalFbest;
    //TODO remove the finalBest and finalFbest
    /** generational loggers for the algorithm */
    protected GenerationLoggers generationLoggers = null;
    /** loggers for each of the individuals */
    protected IndividualLoggers individualLoggers = null;
    /** number of the current generation */
    protected int generation;
    /** number of function evaluations */
    protected int evaluations;
    /** number of the current repetition */
    protected int repNr;
    /** whether the best fitness was improved or not */
    protected boolean bestFitnessImproved;

    /**
     * Instantiate a new algorithm with given parameters.
     * @param parameters parameters for the algorithm
     */
    public Algorithm(Vector<Number> parameters) {
        this.parameters = parameters;
    }
    
    /**
     * Instantiate a new algorithm with no parameters.
     */
    public Algorithm() {
        this.parameters = null;
    }
    
    /**
     * Instantiate a new shallow copy of the given algorithm.
     * @param algorithm the algorithm to copy
     */
    public Algorithm(Algorithm algorithm) {
        this.parameters = algorithm.parameters;
        this.initialSolution = algorithm.initialSolution;
        this.initialFitness = algorithm.initialFitness;
        this.generationLoggers = algorithm.generationLoggers;
        this.individualLoggers = algorithm.individualLoggers;
        this.repNr = algorithm.repNr;
    }
    
    /**
     * Set the parameters of the algorithm.
     * @param parameters parameters of the algorithm to set
     */
    public void setParameters(Vector<Number> parameters) {
        this.parameters = parameters;
    }
    
    /**
     * Add generational loggers to the algorithm.
     * @param loggers loggers to be added
     */
    public void setLoggers(GenerationLoggers loggers) {
        this.generationLoggers = loggers;
    }
    
    /**
     * Add loggers of the individuals to the algorithm.
     * @param loggers loggers to be added
     */
    public void setLoggers(IndividualLoggers loggers) {
        this.individualLoggers = loggers;
    }
    
    /**
     * Execute the algorithm and save some diagnostics.
     * @param problem problem to be optimized
     * @param folder where to save the diagnostics files
     * @param repetitionNr repetition id
     * @param maxEvaluations maximum number of main loops in the
     *        algorithm
     * @return a vector including the information of the best fitness
     *         values at each step when the fitness has lowered
     * @throws Exception if something went wrong
     */
    public abstract Vector<Best> execute(Problem problem,
            int maxEvaluations, String folder, int repetitionNr)
            throws Exception;
    
    /**
     * @return the name of the algorithm
     */
    public String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * @return the final best solution
     */
    public double[] getFinalBest() {
        return this.finalBest;
    }
    
    /**
     * Set the final configuration to x.
     * @param x see above
     */
    protected void setFinalBest(double[] x) {
        this.finalBest = x;
    }

    /**
     * @return the final best solution fitness
     */
    public double getFinalFbest() {
        return this.finalFbest;
    }
    
    /**
     * Set the final fitness to f.
     * @param f see above
     */
    protected void setFinalFbest(double f) {
        this.finalFbest = f;
    }
    
    /**
     * Get the parameter from the index i.
     * @param i index of the parameter
     * @return the corresponding parameter
     */
    public Number getParameter(int i) {
        if (parameters == null)
            return null;
        return parameters.get(i);
    }
    
    /**
     * @return the initial solution vector
     */
    public double[] getInitialSolution() {
        return this.initialSolution;
    }
    
    /**
     * @return the fitness of the initial solution
     */
    public double getInitialFitness() {
        return this.initialFitness;
    }
    
    /**
     * Set the initial solution vector and its fitness.
     * @param initialSolution the new initial solution
     * @param fitness fitness of the initial solution vector
     */
    public void setInitialSolutionAndFitness(double[]
            initialSolution, double fitness) {
        this.initialSolution = initialSolution;
        this.initialFitness = fitness;
    }
    
    /**
     * @return the parameters as a filename string
     */
    public abstract String getParametersAsFilenameString();
    
    /**
     * @return the parameters as a saveable string
     */
    public abstract String getParametersAsSaveableString();
    
    /**
     * @return the parameters as a printable string
     */
    public abstract String getParametersAsPrintableString();
    
    /**
     * Get a new copy of the algorithm instance, such that the
     * loggers attribute is copied deeply and every other attribute
     * shallowly.
     * @return a new copy of the algorithm instance
     */
    public abstract Algorithm copy();
    
    /**
     * Call the generational loggers.
     * @throws FileNotFoundException if the log file was not found
     */
    public void callGenerationLoggers()
            throws FileNotFoundException {
        if (this.generationLoggers != null)
            this.generationLoggers.call(this);
    }
    
    /**
     * Call the loggers of the individuals.
     * @throws FileNotFoundException if hte log file was not found
     */
    public void callIndividualLoggers()
            throws FileNotFoundException {
        if (this.individualLoggers != null)
            this.individualLoggers.call(this);
    }
    
    /**
     * Get the path of the algorithm.
     * @param mainDir the main directory for all the algorithms
     * @param problem the problem to be used
     * @return the path of the algorithm
     */
    public String getPath(String mainDir, Problem problem) {
        return mainDir + problem.getName() + "_" +
                problem.getDimension() + "D/" +
                this.getName() + "/" +
                this.getParametersAsFilenameString() + "/";
    }
    
    /**
     * @return the basic loggable variables common for all
     *         algorithms
     */
    public Vector<Number> getBasicLoggableVariables() {

        Vector<Number> vars = new Vector<Number>();
        vars.add(new Integer(this.generation));
        vars.add(new Integer(this.evaluations));
       
        return vars;
    }
    
    /**
     * Get the loggable variables of the algorithm. This method
     * should be overrided if the algorithm has more loggable
     * variables than the basic ones. The first three elements must
     * be
     * 	x[0] = generation, x[1] = evaluations, x[2] = bestfitness
     * and the rest are arbitrary.
     * @return the loggable variables
     */
    public abstract Vector<Number> getLoggableVariables();

    /**
     * @return the current generation
     */
    public int getGeneration() {
        return this.generation;
    }
    
    /**
     * @return the current repetition number
     */
    public int getRepNr() {
        return this.repNr;
    }
    
    /**
     * @return whether the best fitness got improved or not
     */
    public boolean bestFitnessImproved() {
        return this.bestFitnessImproved;
    }
    
}
