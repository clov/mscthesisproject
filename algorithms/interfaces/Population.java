package algorithms.interfaces;

import problems.interfaces.Problem;
import algorithms.utils.AlgorithmUtils;
import utils.MatrixUtils;

/**
 * A class that represents a population of solutions.
 * @author Teemu Peltonen
 * @version Dec 15, 2014
 */
public class Population {
    
    private final int populationSize;
    private final int individualDimension;
    /** the population */
    // note: the final only constrains the reference, not the values
    //FIXME make private
    public final double[][] population;
    private final double[] fitnesses;
    private int bestIndividualIdx;
    private double bestFitness = Double.MAX_VALUE;
    
    /**
     * Initialize a new population with the given size and individual
     * dimension.
     * @param populationSize number of individuals in the population
     * @param individualDimension the dimension of the invididuals in
     *        the population
     */
    public Population(int populationSize, int individualDimension) {
        
        this.populationSize = populationSize;
        this.individualDimension = individualDimension;
        this.population = new double[populationSize][individualDimension];
        this.fitnesses = new double[populationSize];
    }
    
    /**
     * Get the population size
     * @return the number of individuals in the population
     */
    public int getPopulationSize() {
        return this.populationSize;
    }
    
    /**
     * Get the individual dimension
     * @return the dimension of individuals in the population
     */
    public int getIndividualDimension() {
        return this.individualDimension;
    }
    
    /**
     * Get the problem dimension.
     * @return the dimension of the problem
     */
    public int getProblemDimension() {
        return this.individualDimension;
    }
    
    /**
     * Get the current population
     * @return the current population
     */
    public double[][] getPopulation() {
        return this.population;
    }
    
    /**
     * Get the i'th individual from the population
     * @param i the index of the individual
     * @return the i'th individual from the population
     */
    public double[] getIndividual(int i) {
        return this.population[i];
    }
    
    /**
     * Replace the i'th individual with x.
     * @param i the index of the individual to be replaced
     * @param x the new individual, whose dimension must be the same
     *        as the old one's
     */
    protected void setIndividual(int i, double[] x) {
        
        if (x.length != getIndividual(i).length) {
            String msg = "The dimension of the new individual must";
            msg += " be the same as the old one's.";
            throw new IllegalArgumentException(msg);
        }
        this.population[i] = x;
    }
    
    /**
     * @return the index of the best individual
     */
    public int getBestIndividualIdx() {
        return this.bestIndividualIdx;
    }
    
    /**
     * Set the index of the best individual to i
     * @param i the index of the new best individual
     */
    protected void setBestIndividualIdx(int i) {
        this.bestIndividualIdx = i;
    }
    
    /**
     * @return the best individual
     */
    public double[] getBestIndividual() {
        return this.getIndividual(this.getBestIndividualIdx());
    }
    
    /**
     * @return the best solution
     */
    public double[] getBestSolution() {
        return this.getBestIndividual();
    }
    
    /**
     * @return the fitness of the best individual
     */
    public double getBestFitness() {
        return this.bestFitness;
    }
    
    /**
     * Set the best fitness to f
     * @param f the new best fitness value
     */
    protected void setBestFitness(double f) {
        this.bestFitness = f;
    }
    
    /**
     * Set the fitness of the i'th individual to f.
     * @param i see above
     * @param f see above
     */
    protected void setFitness(int i, double f) {
        this.fitnesses[i] = f;
    }
    
    /**
     * Calculate the fitness of the i'th individual. Also replaces
     * the best individual and the best fitness information of the
     * population if the new fitness was better.
     * @param i index of the individual
     * @param problem problem to be used for the fitness calculation
     * @return the fitness of the i'th individual
     */
    public double calcFitnessAndUpdateBest(int i, Problem problem) {

        double f = problem.f(getIndividual(i));
        this.setFitness(i, f);

        if (f < this.getBestFitness()) {
            this.setBestIndividualIdx(i);
            this.setBestFitness(f);
        }
        return f;
    }
    
    /**
     * Calculate the fitnesses for the whole population. Also
     * refreshes the best individual and the best fitness
     * information.
     * @param problem problem to be used for the fitness calculation
     */
    public void calcFitnessesAndUpdateBest(Problem problem) {
        for (int i = 0; i < this.getPopulationSize(); i++)
            this.calcFitnessAndUpdateBest(i, problem);
    }

    /**
     * Get the fitness of the i'th individual. The function
     * calcFitness has to be called for the i'th individual
     * before calling this.
     * @param i index of the individual
     * @return the fitness of the i'th individual
     */
    public double getFitness(int i) {
        return this.fitnesses[i];
    }
    
    /**
     * Generate a random population within the given bounds.
     * @param bounds bounds for each coordinate
     */
    private void generateRandomPopulation(double[][] bounds) {
        
        double[] x;
        int popSize = this.getPopulationSize();
        int D = this.getIndividualDimension();

        for (int i = 0; i < popSize; i++) {
            x = AlgorithmUtils.generateRandomSolution(bounds, D);
            this.setIndividual(i, x);
        }
    }
    
    /**
     * Generate a random population within the given bounds,
     * but replace the first individual with the given initial
     * solution
     * @param bounds bounds for each coordinate
     * @param initialSolution the initial solution vector
     */
    private void generateSemiRandomPopulation(double[][] bounds,
            double[] initialSolution) {
        
        double[] x;
        int popSize = this.getPopulationSize();
        int D = this.getIndividualDimension();
        
        this.setIndividual(0, initialSolution);

        for (int i = 1; i < popSize; i++) {
            x = AlgorithmUtils.generateRandomSolution(bounds, D);
            this.setIndividual(i, x);
        }
    }
    
    /**
     * Generate a random population within the given bounds,
     * calculate the corresponding fitnesses and update the
     * best individual information.
     * @param bounds bounds for each coordinate
     * @param problem problem to be used for the fitness calculation
     */
    public void generateRandomPopulationAndCalcFitnesses(double[][]
            bounds, Problem problem) {
        
        this.generateRandomPopulation(bounds);
        this.calcFitnessesAndUpdateBest(problem);
    }
    
    /**
     * Generate a random population within the given bounds and
     * replace the first individual with the problem-specific
     * initial solution. Also calculate the corresponding fitnesses
     * and update the best individual information.
     * @param bounds bounds for each coordinate
     * @param problem problem to be used for the fitness calculation
     */
    public void generateSemiRandomPopulationAndCalcFitnesses(double[][]
            bounds, Problem problem) {
        
        double[] initialSolution = problem.getInitialSolution();
        this.generateSemiRandomPopulation(bounds, initialSolution);
        this.calcFitnessesAndUpdateBest(problem);
    }
    
    /**
     * Replace the i'th individual with u if the fitness of u is
     * better. Update also the best information if needed.
     * @param i index of the individual
     * @param u the new individual
     * @param problem problem to be used for the fitness calculation
     * @return a length-2 boolean array, where the 0th component
     *         tells whether u has a better fitness than the i'th
     *         individual, and the 1st component tells whether
     *         u improved the best fitness
     */
    public boolean[] replaceIfBetterAndUpdateBest(int i, double[] u,
            Problem problem) {
        
        double fx = this.getFitness(i);
        double fu = problem.f(u);
        
        boolean[] fitnessImproved = {false, false};
        
        if (fu < fx) {
            this.setIndividual(i, u);
            this.setFitness(i, fu);
            fitnessImproved[0] = true;
            
            if (fu < this.getBestFitness()) {
                this.setBestIndividualIdx(i);
                this.setBestFitness(fu);
                fitnessImproved[1] = true;
            }
        }
        return fitnessImproved;
    }
    
    @Override
    public String toString() {
        return MatrixUtils.toString(getPopulation());
    }
    
}
