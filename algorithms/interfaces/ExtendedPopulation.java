package algorithms.interfaces;

import algorithms.utils.AlgorithmUtils;
import problems.interfaces.Problem;

/**
 * A population where the individuals have some extra information
 * encoded in them.
 * @author Teemu Peltonen
 * @version Dec 15, 2014
 */
public class ExtendedPopulation extends Population {
    
    private final int problemDimension;
    private final int extraComponents;
    
    /**
     * Initialize a new population with the given size, problem
     * dimension and the number of extra components per individual.
     * The total dimension of each individual will be
     * problemDimension + extraComponents.
     * @param populationSize the number of the individuals in the
     *        population
     * @param problemDimension the dimension of the original problem
     *        (without the extra components)
     * @param extraComponents the number of extra components that each
     *        individual has
     */
    public ExtendedPopulation(int populationSize,
            int problemDimension, int extraComponents) {
        super(populationSize, problemDimension+extraComponents);
        this.problemDimension = problemDimension;
        this.extraComponents = extraComponents;
    }
    
    /**
     * @return the dimension of the original problem (without the
     *         extra components)
     */
    @Override
    public int getProblemDimension() {
        return this.problemDimension;
    }
    
    /**
     * Get the solution-part of the i'th individual (without the
     * extra coomponents)
     * @param i index of the individual
     * @return the solution part of the i'th individual
     */
    public double[] getSolution(int i) {
        
        final int D = this.getProblemDimension();
        double[] x = new double[D];
        
        for (int j = 0; j < D; j++)
            x[j] = this.getIndividual(i)[j];
        
        return x;
    }
    
    @Override
    public double[] getBestSolution() {
        return this.getSolution(this.getBestIndividualIdx());
    }
    
    /**
     * @return the number of extra components in the individuals
     */
    public int getNrOfExtraComponents() {
        return this.extraComponents;
    }
    
    /**
     * Get the j'th extra component of the i'th individual.
     * @param i index of the individual
     * @param j see above
     * @return the j'th extra parameter of the i'th individual
     */
    public double getExtraComponent(int i, int j) {
        return this.getIndividual(i)[this.getProblemDimension()+j];
    }
    
    /**
     * Set the j'th extra component of the i'th individual to x.
     * @param i index of the individual
     * @param j see above
     * @param x see above
     */
    public void setExtraComponent(int i, int j, double x) {
        this.getIndividual(i)[this.getProblemDimension()+j] = x;
    }
    

    /**
     * Generate a random population within the given bounds.
     * The extra components are initialized to zero.
     * @param bounds bounds for each coordinate
     */
    public void generateRandomPopulationWoExtraComponents(
            double[][] bounds) {
        
        double[] x, xExt;
        int popSize = this.getPopulationSize();
        int D = this.getProblemDimension();

        for (int i = 0; i < popSize; i++) {
            x = AlgorithmUtils.generateRandomSolution(bounds, D);
            xExt = new double[this.getIndividualDimension()];
            
            for (int j = 0; j < D; j++)
                xExt[j] = x[j];
            for (int j = 0; j < this.getNrOfExtraComponents(); j++)
                xExt[D+j] = 0.0;

            this.setIndividual(i, xExt);
        }
    }
    
    @Override
    public double calcFitnessAndUpdateBest(int i, Problem problem) {

        double f = problem.f(this.getSolution(i));
        this.setFitness(i, f);
        
        if (f < this.getBestFitness()) {
            this.setBestIndividualIdx(i);
            this.setBestFitness(f);
        }
        return f;
    }
       
    /**
     * Replace the i'th individual with u if the fitness of u is
     * better. Update also the best information if needed.
     * @param i index of the individual
     * @param u the new individual
     * @param problem problem to be used for the fitness calculation
     * @return a length-2 boolean array, where the 0th component
     *         tells whether u has a better fitness than the i'th
     *         individual, and the 1st component tells whether
     *         u improved the best fitness
     */
    @Override
    public boolean[] replaceIfBetterAndUpdateBest(int i, double[] u,
            Problem problem) {
        
        double[] uExt =
            new double[u.length+this.getNrOfExtraComponents()];
        int probDim = this.getProblemDimension();
        
        for (int j = 0; j < u.length; j++)
            uExt[j] = u[j];
        for (int j = 0; j < this.getNrOfExtraComponents(); j++)
            uExt[probDim+j] = this.getExtraComponent(i, j);
        
        double fx = this.getFitness(i);
        double fu = problem.f(u);
        
        boolean[] fitnessImproved = {false, false};
        
        if (fu < fx) {
            this.setIndividual(i, uExt);
            this.setFitness(i, fu);
            fitnessImproved[0] = true;
            
            if (fu < this.getBestFitness()) {
                this.setBestIndividualIdx(i);
                this.setBestFitness(fu);
                fitnessImproved[1] = true;
            }
        }
        return fitnessImproved;
    }

}
