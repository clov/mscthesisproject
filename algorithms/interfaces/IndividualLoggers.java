package algorithms.interfaces;

import java.io.FileNotFoundException;
import java.util.Vector;

/**
 * A class for packacing Logger objects.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public class IndividualLoggers {
    
    private Vector<Logger> loggers;
    
    /**
     * Initialize a new Loggers object.
     */
    public IndividualLoggers() {
        this.loggers = new Vector<Logger>();
    }
    
    /**
     * Instantiate a new copy of the given loggers object.
     * @param loggers object to be copied
     */
    public IndividualLoggers(IndividualLoggers loggers) {
        this.loggers = new Vector<Logger>(loggers.loggers);
    }
    
    /**
     * Add a new logger to the set of loggers.
     * @param logger a new logger to be attached
     */
    public void add(Logger logger) {
        this.loggers.add(logger);
    }
    
    /**
     * Call the call()-methods of each of the attached loggers
     * if the fitness was improved. Whether the fitness to compare
     * to is the best fitness or something else, depends on the
     * algorithm.
     * @param algorithm the algorithm instance that owns these
     *        loggers
     * @throws FileNotFoundException if the log file cannot be found
     */
    public void call(Algorithm algorithm)
            throws FileNotFoundException {
        
        for (Logger logger : this.loggers) {
            
            if (algorithm.bestFitnessImproved())
                logger.call(algorithm);
        }
    }

}
