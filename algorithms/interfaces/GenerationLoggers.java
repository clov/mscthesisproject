package algorithms.interfaces;

import java.io.FileNotFoundException;
import java.util.Vector;

/**
 * A class for packacing GenerationLogger objects.
 * @author Teemu Peltonen
 * @version Dec 5, 2014
 */
public class GenerationLoggers {
    
    private Vector<GenerationLogger> loggers;
    
    /**
     * Initialize a new Loggers object.
     */
    public GenerationLoggers() {
        this.loggers = new Vector<GenerationLogger>();
    }
    
    /**
     * Instantiate a new copy of the given loggers object.
     * @param loggers object to be copied
     */
    public GenerationLoggers(GenerationLoggers loggers) {
        this.loggers = new Vector<GenerationLogger>(loggers.loggers);
    }
    
    /**
     * Add a new logger to the set of loggers.
     * @param logger a new logger to be attached
     */
    public void add(GenerationLogger logger) {
        this.loggers.add(logger);
    }
    
    /**
     * Call the call()-methods of each of the attached loggers.
     * @param algorithm the algorithm instance that owns these
     *        loggers
     * @throws FileNotFoundException if the log file cannot be found
     */
    public void call(Algorithm algorithm)
            throws FileNotFoundException {
        
        for (GenerationLogger logger : this.loggers) {
            
            if (algorithm.getGeneration() % logger.getInterval()
                    == 0)
                logger.call(algorithm);
        }
    }

}
