package algorithms.utils;

import static utils.MatrixUtils.reShape;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Vector;

import utils.FileUtils;

/**
 * Utilities for saving results of the runs.
 * @author Teemu Peltonen
 * @version Oct 15, 2014
 */
public class ResultsUtils {
    
    /**
     * Save the bests-vector into a file.
     * @param bests the Best-vector to be saved
     * @param filename name of the file where to save
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static void saveBests(Vector<Best> bests, String filename)
            throws FileNotFoundException {
        
        PrintStream fo = FileUtils.openFileForWriting(filename);
        
        fo.printf("# %s, %s\n", "Evaluation step", "Best fitness value");
        for (int i = 0; i < bests.size(); i++)
            fo.printf("%d %e\n", bests.get(i).getEvalNumber(),
                                 bests.get(i).getfBest());
        fo.close();
    }
    
    /**
     * Save the atomic coordinates into an .xyz file
     * @param x the atomic coordinates
     * @param atomicSymbol Chemical symbol of the atoms. This defines
     *                     the colour of the atoms when drawing with
     *                     visualizers.
     * @param filename name of the file where to save
     * @throws FileNotFoundException if the file couldn't be opened
     */
    public static void saveXYZ(double[] x, char atomicSymbol, String filename)
            throws FileNotFoundException {
        
        int n = x.length / 3;
        double[][] X = reShape(x, n, 3);

        PrintStream fo = FileUtils.openFileForWriting(filename);
        
        fo.println(n);
        fo.println("Final optimized structure of the best run");
        for (double[] singleAtomCoords : X) {
            fo.printf("%s %f %f %f\n", atomicSymbol,
                      singleAtomCoords[0], singleAtomCoords[1],
                      singleAtomCoords[2]);
        }
        fo.close();      
    }
    
}
