package algorithms.utils;

/**
 * A class that includes the fitness value of the best configuration
 * at a given step.
 * @author Teemu Peltonen
 * @version May 28, 2014
 */
public class Best {

    private int evalNumber;
    private double fBest;

    /**
     * @param evalNumber number of the evaluation
     * @param fBest fitness of the configuration
     */
    public Best(int evalNumber, double fBest) {
        this.evalNumber = evalNumber;
        this.fBest = fBest;
    }

    /**
     * @return the evaluation number
     */
    public int getEvalNumber() {
        return this.evalNumber;
    }

    /**
     * @return the best fitness value
     */
    public double getfBest() {
        return this.fBest;
    }
    
}
