package algorithms.utils;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import problems.interfaces.Problem;

import utils.FileUtils;

import algorithms.interfaces.Algorithm;

/**
 * A class for saving mean, standard deviation and standard error
 * of the mean for a group of runs of an algorithm with a given set
 * of parameters.
 * @author Teemu Peltonen
 * @version Feb 19, 2015
 */
public class MeanResult {
    
    private final Algorithm algorithm;
    private final Problem problem;
    private final double mean;
    private final double stddev;
    private final double stderr;
    
    /**
     * Instantiate a new MeanResults with the given algorithm,
     * problem, mean, standard deviation and standard error of
     * the mean.
     * @param algorithm algorithm that has been used to calculate
     *        the results
     * @param problem the problem that was optimized
     * @param mean mean of the fitnesses of the runs
     * @param stddev standard deviation
     * @param stderr standard error of the mean
     */
    public MeanResult(Algorithm algorithm, Problem problem,
            double mean, double stddev, double stderr) {
        this.algorithm = algorithm;
        this.problem = problem;
        this.mean = mean;
        this.stddev = stddev;
        this.stderr = stderr;
    }
    
    /**
     * @return the algorithm used
     */
    public Algorithm getAlgorithm() {
        return this.algorithm;
    }
    
    /**
     * @return the problem that was optimized
     */
    public Problem getProblem() {
        return this.problem;
    }
    
    /**
     * @return the mean of the runs
     */
    public double getMean() {
        return this.mean;
    }
    
    /**
     * @return the standard deviation of the runs
     */
    public double getStdDev() {
        return this.stddev;
    }
    
    /**
     * @return the standard error of the mean of the runs
     */
    public double getStdErr() {
        return this.stderr;
    }

    /**
     * Save the contents into a file.
     * @param filename the name of the file
     * @param append append in the end of the file or not
     * @throws FileNotFoundException if the file cannot be found
     */
    public void save(String filename, boolean append)
        throws FileNotFoundException {
        
        PrintStream f = FileUtils.openFileForWriting(filename,
                append);
        Algorithm a = this.getAlgorithm();
        Problem p = this.getProblem();

        f.printf("%s %d %s %s %e %e %e\n", p.getName(),
                p.getDimension(), a.getName(),
                a.getParametersAsSaveableString(),
                this.getMean(), this.getStdDev(), this.getStdErr());
        f.close();
    }

}
