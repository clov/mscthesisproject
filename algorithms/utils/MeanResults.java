package algorithms.utils;

import java.io.FileNotFoundException;
import java.util.Vector;

/**
 * A class for saving many MeanResult objects.
 * @author Teemu Peltonen
 * @version Feb 19, 2015
 */
public class MeanResults {
    
    Vector<MeanResult> meanResults = new Vector<MeanResult>();
    
    /**
     * Instantiate a new MeanResults object.
     */
    public MeanResults() {}

    /**
     * Get the i'th element.
     * @param i index of the element
     * @return the i'th element
     */
    public MeanResult get(int i) {
        return meanResults.get(i);
    }
    
    /**
     * Add a new MeanResult object.
     * @param meanResult object to be added
     */
    public void add(MeanResult meanResult) {
        meanResults.add(meanResult);
    }
    
    /**
     * @return number of the elements
     */
    public int size() {
        return this.meanResults.size();
    }
    
    /**
     * Get all the MeanResult objects whose algorithm is named
     * algorithmName.
     * @param algorithmName see above
     * @return a MeanResults object having all the MeanResult
     *         objects whose algorithm is named algorithmName
     */
    public MeanResults getAllOfType(String algorithmName) {

        MeanResults newMeanResults = new MeanResults();
        for (MeanResult meanResult : this.meanResults)
            if (meanResult.getAlgorithm().getName().equals(algorithmName))
                newMeanResults.add(meanResult);
        
        return newMeanResults;
    }
    
    /**
     * Save the contents into a file.
     * @param filename name of the file
     * @param append append in the end of the file or not
     * @throws FileNotFoundException if the file cannot be found
     */
    public void save(String filename, boolean append)
            throws FileNotFoundException {
        
        for (int i = 0; i < this.size(); i++)
            this.get(i).save(filename, append);
    }

}
