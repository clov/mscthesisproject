package algorithms.utils;

import java.io.FileNotFoundException;
import java.util.Vector;

import utils.FileUtils;

import algorithms.interfaces.Algorithm;
import algorithms.interfaces.GenerationLogger;

/**
 * A logger class for saving the best fitness.
 * @author Teemu Peltonen
 * @version Nov 27, 2014
 */
public class SaveBestFitness extends GenerationLogger {

    /**
     * @param path path of the folder for the log file
     * @param interval the interval between the consecutive
     *        log steps
     */
    public SaveBestFitness(String path, int interval) {
        super(path, "bestfitness", interval);
    }

    @Override
    public void call(Algorithm algorithm)
            throws FileNotFoundException {
        
        Vector<Number> loggableVariables =
                algorithm.getLoggableVariables();
        
        int a = loggableVariables.get(1).intValue();
        double b = loggableVariables.get(2).doubleValue();
        String filename = this.getPath() + this.getName() +
                "_rep" + algorithm.getRepNr() + ".txt";
        FileUtils.save(a, b, filename, true);
    }

}
