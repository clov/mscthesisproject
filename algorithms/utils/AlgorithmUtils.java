package algorithms.utils;

import utils.random.RandUtils;
import algorithms.interfaces.Population;

/**
 * Utilities for algorithms
 * @author Teemu Peltonen
 * @version May 28, 2014
 */
public class AlgorithmUtils {

    /**
     * Generates a random vector of length problemDimension whose
     * components are in the given bounds.
     * @param bounds a problemDimension*2 matrix, where i'th row tells
     *               the lower and upper bounds for the i'th component
     * @param problemDimension dimension of the problem
     * @return a random vector
     */
    public static double[] generateRandomSolution(double[][] bounds, int problemDimension) {

        double[] x = new double[problemDimension];
        double minbound;
        double maxbound;
        for (int i = 0; i < x.length; i++) {
            minbound = bounds[i][0];
            maxbound = bounds[i][1];
            x[i] = RandUtils.random(minbound, maxbound);
        }
        return x;
    }
    
    //TODO tests
    /**
     * If the number x is outside the feasible interval, mirror it
     * from the boundary.
     * @param x the number that might be outside the feasible
     *        interval
     * @param bounds the feasible interval [ bounds[0], bounds[1] ]
     * @return x moved to the feasible region
     *
     */
    public static double saturateMirror(double x, double[] bounds) {
        
        double minbound = bounds[0];
        double maxbound = bounds[1];
        
        if (x < minbound)
            return 2.0 * minbound - x;
        if (maxbound < x)
            return 2.0 * maxbound - x;
        return x;
    }
    
    /**
     * If the vector x is outside the feasible region, mirror it
     * from the boundary.
     * @param x the vector that might be outside the feasible region
     * @param bounds The feasible region. The size of the array must
     *        be [x.length][2], where each row has the lower and
     *        upper limits for each dimension.
     * @return a new vector which is x moved to the feasible region
     */
    public static double[] saturateMirror(double[] x, double[][] bounds) {
        
        int D = x.length;
        double[] xnew = new double[D];
        
        for (int i = 0; i < D; i++)
            xnew[i] = saturateMirror(x[i], bounds[i]);
        
        return xnew;
    }
    
    /**
     * If the number x is outside the feasible interval, move it to
     * the boundary.
     * @param x the number that might be outside the feasible
     *        interval
     * @param bounds the feasible interval [ bounds[0], bounds[1] ]
     * @return x moved to the feasible interval
     */
    public static double saturateAbsorbingWalls(double x,
            double[] bounds) {
        
        double minbound = bounds[0];
        double maxbound = bounds[1];
        
        if (x < minbound)
            return minbound;
        if (maxbound < x)
            return maxbound;
        return x;
    }
    
    /**
     * If the vector x is outside the feasible region, move it to
     * the boundary.
     * @param x the vector that might be outside the feasible region
     * @param bounds The feasible region. The size of the array must
     *        be [x.length][2], where each row has the lower and
     *        upper limits for each dimension.
     * @return a new vector which is x moved to the feasible region
     */
    public static double[] saturateAbsorbingWalls(double[] x,
            double[][] bounds) {
        
        int D = x.length;
        double[] xnew = new double[D];
        
        for (int i = 0; i < D; i++)
            xnew[i] = saturateAbsorbingWalls(x[i], bounds[i]);
        
        return xnew;
    }

    /**
     * Move the number x back to the feasible interval in a toroidal
     * periodic boundary conditions manner.
     * @param x the number that might be outside the feasible
     *        interval 
     * @param bounds the feasible interval [ bounds[0], bounds[1] ] 
     * @return x moved to the feasible interval
     * @example
     * <pre name="test">
     *  double[] bounds = {-1.0, 1.0};
     *  // in the feasible interval
     *  saturateToro(0.0, bounds) ~~~ 0.0;
     *  // in the feasible interval
     *  saturateToro(-1.0, bounds) ~~~ -1.0;
     *  // in the feasible interval
     *  saturateToro(1.0, bounds) ~~~ 1.0;
     *  // not in the feasible interval
     *  saturateToro(1.1, bounds) ~~~ -0.9;
     *  
     *  bounds[1] = 2.0;
     *  saturateToro(-1.5, bounds) ~~~ 1.5;
     *  saturateToro(1.5, bounds) ~~~ 1.5;
     *  saturateToro(2.5, bounds) ~~~ -0.5;
     *  saturateToro(-8.0, bounds) ~~~ 1.0;
     *  saturateToro(7.0, bounds) ~~~ 1.0;
     * </pre>
     */
    public static double saturateToro(double x, double[] bounds) {

        double minbound = bounds[0];
        double maxbound = bounds[1];
        if (x < minbound) {
            return maxbound - (minbound - x) % (maxbound - minbound);
        }
        if (maxbound < x) {
            return minbound + (x - maxbound) % (maxbound - minbound);
        }
        return x;
    }
    
    /**
     * Move the vector x back to the feasible region in a toroidal
     * periodic boundary conditions manner.
     * @param x the vector that might be outside the feasible region
     * @param bounds The feasible region. The size of the array must
     *        be [x.length][2], where each row has the lower and
     *        upper limits for each dimension.
     * @return a new vector which is x moved to the feasible region
     * @example
     * <pre name="test">
     *  double[][] bounds = new double[][]{
     *      {-1.0, 1.1},
     *      {-2.1, 2.3},
     *      {-3.3, 3.4}
     *  };
     *  double[] x;
     *  // every dimension in the feasible region
     *  x = saturateToro(new double[]{0.0, 0.0, 0.0}, bounds);
     *  x[0] ~~~ 0.0; x[1] ~~~ 0.0; x[2] ~~~ 0.0;
     *  // 2nd and 3rd dimensions in the feasible region, 1st
     *  // dimension not in the feasible region
     *  x = saturateToro(new double[]{-1.2, 1.0, 2.0}, bounds);
     *  x[0] ~~~ 0.9; x[1] ~~~ 1.0; x[2] ~~~ 2.0;
     *  x = saturateToro(new double[]{1.5, 1.0, 2.0}, bounds);
     *  x[0] ~~~ -0.6; x[1] ~~~ 1.0; x[2] ~~~ 2.0;
     *  // every dimension out of the feasible region
     *  x = saturateToro(new double[]{3.1, -2.2, 4.0}, bounds);
     *  x[0] ~~~ 1.0; x[1] ~~~ 2.2; x[2] ~~~ -2.7;
     * </pre>
     */
    public static double[] saturateToro(double[] x, double[][] bounds) {
        
        int D = x.length;
        double[] xnew = new double[D];

        for (int i = 0; i < D; i++)
            xnew[i] = saturateToro(x[i], bounds[i]);
        
        return xnew;
    }
    
    /**
     * Generate 3 mutually distinct random integers in the interval
     * [0, n[ such that they are also distinct from i.
     * distinct from i.
     * @param i see above
     * @param n see avove
     * @return 3 mutually distinct random integers in the interval
     * [0, n[ such that they are also distinct from i
     */
    public static int[] generate3distinctRandomInts(int i, int n) {
        
        if (n < 4) {
            String msg = "n must be at least 4.";
            throw new IllegalArgumentException(msg);
        }
        
        int rand0;
        int rand1;
        int rand2;
        int[] rands = new int[3];
        
        do
            rand0 = RandUtils.randomInt(n);
        while (rand0 == i);
        rands[0] = rand0;

        do
            rand1 = RandUtils.randomInt(n);
        while (rand1 == rand0 || rand1 == i);
        rands[1] = rand1;
        
        do
            rand2 = RandUtils.randomInt(n);
        while (rand2 == rand1 || rand2 == rand0 || rand2 == i);
        rands[2] = rand2;
        
        return rands;
    }
    
    /**
     * Make a DE-style mutation to the i'th individual of the given
     * population. Returns the mutated vector, doesn't affect the
     * population itself.
     * @param i the index of the individual
     * @param population population where the individual to be
     *        mutated resides
     * @param F the parameter that controls the amplification of the
     *        difference vector (usually F is in the range [0, 2])
     * @param mutateExtraComponents whether to mutate the possible
     *        extra components also (if not, the resulting vector
     *        does not include the extra components)
     * @return the mutated vector
     */
    public static double[] getDErand1mutation(final int i,
            final Population population, final double F,
            final boolean mutateExtraComponents) {
        
        int D;
        if (mutateExtraComponents)
            D = population.getIndividualDimension();
        else
            D = population.getProblemDimension();
        
        int popSize = population.getPopulationSize();
        int[] rands = generate3distinctRandomInts(i, popSize);

        int r1 = rands[0];
        int r2 = rands[1];
        int r3 = rands[2];
        
        double[] v = new double[D];
        
        for (int j = 0; j < D; j++)
            v[j] = population.getIndividual(r1)[j] +
                   F * (population.getIndividual(r2)[j] -
                           population.getIndividual(r3)[j]);
        
        return v;
    }
    
    /**
     * Make a DE-style mutation to the i'th individual of the given
     * population. Returns the mutated vector, doesn't affect the
     * population itself.
     * @param i the index of the individual
     * @param population population where the individual to be
     *        mutated resides
     * @param F the parameter that controls the amplification of the
     *        difference vector (usually F is in the range [0, 2])
     * @return the mutated vector
     */
    public static double[] getDErand1mutation(final int i,
            final Population population, final double F) {
        return getDErand1mutation(i, population, F, false);
    }
    
    /**
     * Make a DE-style binomial crossover between the target and
     * the donor vectors.
     * @param x the target vector
     * @param v the donor vector
     * @param CR the crossover rate
     * @return the trial vector formed by crossing over the target
     *         and the donor vectors
     */
    public static double[] crossOverBin(final double[] x,
            final double[] v, final double CR) {
        
        int dim = v.length;
        double[] u = new double[dim];
        double randb;
        int rnbr = RandUtils.randomInt(dim);

        for (int j = 0; j < dim; j++) {
            randb = RandUtils.random();
            if (randb <= CR || j == rnbr)
                u[j] = v[j];
            else
                u[j] = x[j];
        }
        return u;
    }

    /**
     * @param args not used
     */
    public static void main(String[] args) {
        generate3distinctRandomInts(3, 4);
    }
}
