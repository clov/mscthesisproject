package algorithms;

import java.io.FileNotFoundException;
import java.util.Vector;

import problems.interfaces.Problem;
import utils.FileUtils;
import utils.random.RandUtils;

import algorithms.interfaces.Algorithm;
import algorithms.interfaces.ExtendedPopulation;
import algorithms.interfaces.Logger;
import algorithms.utils.AlgorithmUtils;
import algorithms.utils.Best;

/**
 * Self-adaptive differential evolution. [1] The name jDE was later
 * given by Brest et al. [2].
 * 
 * [1] Brest, J. et al., Self-Adapting Control Parameters in
 * Differential Evolution: A Comparative Study on Numerical Benchmark
 * Problems, IEEE Transactions on Evolutionary Computation, 2006, 10,
 * 646-657 
 * 
 * [2] Brest, J. et al., Performance comparison of self-adaptive and
 * adaptive differential evolution algorithms, Soft Computing,
 * Springer-Verlag, 2007, 11, 617-629

 * @author Teemu Peltonen
 * @version Jan 13, 2015
 */
public class JDE extends Algorithm {
    
    private final int Fidx = 0;
    private final int CRidx = 1;
        
	private ExtendedPopulation extendedPopulation;
	private double Fi; // current Fi
	private double CRi; // current CRi
	
    // "parameters" that need not be adjusted according to the
    // authors of jDE
    private final double Fl = 0.1;
    private final double Fu = 0.9;
    private final double tau1 = 0.1;
    private final double tau2 = 0.1;
        
    /**
     * @param parameters parameters for the algorithm such that
     *        parameters[0] is the population size
     */
    public JDE(Vector<Number> parameters) {
        super(parameters);
    }
    
    /**
     * @param algorithm the algorithm to copy
     */
    public JDE(Algorithm algorithm) {
        super(algorithm);
    }
    
    /**
     * Generate extended bounds which include both the bounds for
     * the actual solution vectors and for the Fi's and CRi's
     * @param bounds the original bounds for the actual solution
     *        vectors
     * @return the extended bounds
     * @example
     * <pre name="test">
     *  #import java.util.Arrays;
     *  #import java.util.Vector;
     *  double[][] bounds = new double[][]{{-1.0, 1.0},
     *                                     {-2.0, 2.0},
     *                                     {-3.0, 4.0}};
     *  JDE a = new JDE(new Vector<Number>());
     *  double[][] extendedBounds = a.generateExtendedBounds(bounds);
     *  Arrays.toString(extendedBounds[0]) === "[-1.0, 1.0]";
     *  Arrays.toString(extendedBounds[1]) === "[-2.0, 2.0]";
     *  Arrays.toString(extendedBounds[2]) === "[-3.0, 4.0]";
     *  Arrays.toString(extendedBounds[3]) === "[0.1, 1.0]";
     *  Arrays.toString(extendedBounds[4]) === "[0.0, 1.0]";
     *  extendedBounds.length === 5;
     * </pre>
     */
    public double[][] generateExtendedBounds(double[][] bounds) {
        
        final int problemDimension = bounds.length;
        double[][] extendedBounds = new double[problemDimension+2][2];
        
        for (int i = 0; i < problemDimension; i++)
            extendedBounds[i] = bounds[i];
        
        extendedBounds[problemDimension] =
                new double[]{this.Fl, this.Fl+this.Fu};
        extendedBounds[problemDimension+1] = new double[]{0.0, 1.0};

        return extendedBounds;
    }
    
    /**
     * Initialize the extra components.
     * @param F initial value for all the F's
     * @param CR initial value for all the CR's
     */
    private void initializeExtraComponents(double F, double CR) {
        
        int populationSize = extendedPopulation.getPopulationSize();
        
        for (int i = 0; i < populationSize; i++) {
            extendedPopulation.setExtraComponent(i, this.Fidx, F);
            extendedPopulation.setExtraComponent(i, this.CRidx, CR);
        }
        
    }
    
    @Override
    public Vector<Best> execute(Problem problem, int maxEvaluations,
            String folder, int repetitionNr) throws Exception {
        
        this.repNr = repetitionNr;
        
        final int populationSize = this.getParameter(0).intValue();
        final double initF = this.getParameter(1).doubleValue();
        final double initCR = this.getParameter(2).doubleValue();
        
        final int problemDimension = problem.getDimension();
        final double[][] bounds = problem.getBounds();
        
        this.extendedPopulation =
                new ExtendedPopulation(populationSize,
                        problemDimension, 2);

        this.evaluations = 0;
        
        Vector<Best> bests = new Vector<Best>();
        
        extendedPopulation.generateRandomPopulationWoExtraComponents(bounds);
        this.initializeExtraComponents(initF, initCR);
        extendedPopulation.calcFitnessesAndUpdateBest(problem);
        this.evaluations += populationSize;
        
        bests.add(new Best(this.evaluations,
                extendedPopulation.getBestFitness()));
        
        // temp variables
        double rand1;
        double rand2;
        double rand3;
        double rand4;
        double[] x;
        double[] v;
        double[] u;
        boolean[] fitnessImproved;
        
        // iterate until the stopping criterion is met
        while (this.evaluations < maxEvaluations) {
            
            // for each individual
            for (int i = 0; i < populationSize; i++) {
                
                this.Fi = extendedPopulation.getExtraComponent(i,
                        Fidx);
                this.CRi = extendedPopulation.getExtraComponent(i,
                        CRidx);
                
                // Fi update
                rand2 = RandUtils.random();
                if (rand2 < this.tau1) {
                    rand1 = RandUtils.random();
                    this.Fi = this.Fl + rand1 * this.Fu;
                }

                // CRi update
                rand4 = RandUtils.random();
                if (rand4 < this.tau2) {
                    rand3 = RandUtils.random();
                    this.CRi = rand3;
                }
                
                // mutation
                v = AlgorithmUtils.getDErand1mutation(i,
                        extendedPopulation, this.Fi, false);
                
                // saturation
                //v = AlgorithmUtils.saturateAbsorbingWalls(v,
                //        bounds);
                v = AlgorithmUtils.saturateToro(v, bounds);
                
                // crossover
                x = extendedPopulation.getSolution(i);
                u = AlgorithmUtils.crossOverBin(x, v, this.CRi);
                
                // selection and best update (to this end, the
                // fitness of x hasn't been changed)
                fitnessImproved =
                    extendedPopulation.replaceIfBetterAndUpdateBest(i,
                            u, problem);
                this.bestFitnessImproved = fitnessImproved[1];

                if (fitnessImproved[0]) {
                    extendedPopulation.setExtraComponent(i,
                            this.Fidx, this.Fi);
                    extendedPopulation.setExtraComponent(i,
                            this.CRidx, this.CRi);
                }
                // only the u fitness evaluation is done above
                this.evaluations++;
                
                this.callIndividualLoggers();
            }
            this.generation++;
            
            bests.add(new Best(this.evaluations,
                    extendedPopulation.getBestFitness()));
            this.callGenerationLoggers();
        }
        this.setFinalBest(extendedPopulation.getBestSolution());
        this.setFinalFbest(extendedPopulation.getBestFitness());
        return bests;
    }

    @Override
    public String getParametersAsFilenameString() {

        final int populationSize = this.getParameter(0).intValue();
        return "NP"+populationSize;
    }

    @Override
    public String getParametersAsSaveableString() {

        final int populationSize = this.getParameter(0).intValue();
        return "NP "+populationSize;
    }

    @Override
    public String getParametersAsPrintableString() {

        final int populationSize = this.getParameter(0).intValue();
        return "NP "+populationSize;
    }

    @Override
    public Algorithm copy() {
        return new JDE(this);
    }

	@Override
	public Vector<Number> getLoggableVariables() {

		Vector<Number> vars = this.getBasicLoggableVariables();
		vars.add(new Double(this.extendedPopulation.getBestFitness()));
		vars.add(new Double(this.Fi));
		vars.add(new Double(this.CRi));
		
		return vars;
	}

    /**
     * A logger class for logging the F and CR.
     * @author Teemu Peltonen
     * @version Dec 5, 2014
     */
    public class SaveFCR extends Logger {
        
        /**
         * @param path path of the folder for the log file
         */
        public SaveFCR(String path) {
            super(path, "FCR");
        }

        @Override
        public void call(Algorithm algorithm)
                throws FileNotFoundException {
            
            Vector<Number> loggableVariables =
                    algorithm.getLoggableVariables();
            
            int gen = loggableVariables.get(0).intValue();
            double F = loggableVariables.get(3).doubleValue();
            double CR = loggableVariables.get(4).doubleValue();
            
            String filename = this.getPath() + this.getName() +
                    "_rep" + algorithm.getRepNr() + ".txt";
            FileUtils.save(gen, F, CR, filename, true);
        }
    }

}
